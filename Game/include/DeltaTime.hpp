#ifndef __DELTATIME_HPP__
#define __DELTATIME_HPP__

#include <Magnum/GL/TimeQuery.h>

constexpr float maxDeltaTime = 0.000001f;

class  DeltaTime : public Magnum::GL::TimeQuery
{
	private:
		Magnum::UnsignedLong 	oldTime_;
		Magnum::UnsignedLong 	newTime_;
		float					deltaTime_;
	public:
		

		 DeltaTime	(Magnum::NoCreateT):
			Magnum::GL::TimeQuery	(Magnum::NoCreate),
			oldTime_ 				{0},
			newTime_				{0},
			deltaTime_				{0.0f}
		 {
		 }

		 DeltaTime	():
			Magnum::GL::TimeQuery 	{Magnum::GL::TimeQuery::Target::Timestamp},
			oldTime_ 				{0},
			newTime_				{0},
			deltaTime_				{0.0f}
		{
			deltaTime_ 	= 0.0f;
		}

		DeltaTime	(DeltaTime&& movedDeltaTime):
			Magnum::GL::TimeQuery	(std::move(movedDeltaTime)),
			oldTime_				{movedDeltaTime.oldTime_},
			newTime_				{movedDeltaTime.newTime_},
			deltaTime_				{movedDeltaTime.deltaTime_}

		{
		}

		~DeltaTime	(){}

		inline 	void 		update		()
		{
				timestamp();
				oldTime_ = newTime_;
				newTime_ = result<Magnum::UnsignedLong>();
				
				Magnum::UnsignedLong dTime 	= newTime_ - oldTime_;

				deltaTime_	= dTime != 0.0 ? static_cast<float>(dTime * 0.000000001f) : 0.0f;
		}

		inline 	float& 		get 		()
		{
			return 	deltaTime_;	
		}

		inline	float&		operator*	()
		{
			return deltaTime_;
		}

		inline	DeltaTime& 	operator=	(DeltaTime&& movedDeltaTime)
		{
			Magnum::GL::TimeQuery::operator=(std::move(movedDeltaTime));

			return *this;
		}
};




#endif /* __DELTATIME_HPP__ */
