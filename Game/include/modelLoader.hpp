//Project : workspace.json
//Editing by Six Jonathan
//Date : 05/02/2020 - 17 h 48

#ifndef _MODEL_LOADER_H
#define _MODEL_LOADER_H

/*Magnum dependence*/
#include <Magnum/Trade/TextureData.h>
#include <Magnum/Trade/SceneData.h>
#include <Corrade/Containers/Array.h>
#include <Corrade/Containers/Optional.h>
#include <Magnum/Resource.h>

/*STD dependence*/
#include <memory>
#include <string>

/*Project file dependence*/
#include "type.hpp"

/**
 * @brief Singletone class to load model
 * 
 */
class ModelLoader
{
    public:

        ModelLoader ()							= delete;
        ModelLoader (const ModelLoader& other)	= delete;
        ModelLoader (ModelLoader&& other)		= delete;
        virtual ~ModelLoader ()				    = delete;

        /**
         * @brief Load model and return unique pointor to the boject create
         * 
         * @param pathModel                 :   The relative path of the .Obj file. Start to the Game folder
         *                                      i.e : Game/resources/meshes/[...].obj
         * @param resources                 :   The resource manager of the game to avoid to load multiple time any asset
         * @param group                     :   The group of that this model will to belong. Each scen must be associate with a group to be drawable by the camera.
         * @param parent                    :   Pointor to the parent of the models loaded. If nullptr, the model doesn't have parent.
         *                                      Remberber that SceneGraph is an Object3D. So if you want add this model to the world add the pointor of the scene.
         *                                      Else use function setParent() to assign a new parent.
         * @return std::unique_ptr<Object3D>:   pointor of the model create. Use std::move to move this unique pointor to another 
         *                                      i.e : std::unique_ptr pt = std::move(loadModel([...]))
         */
        static
        std::unique_ptr<Object3D> loadModel(const std::string&                      pathModel,
                                            GameResourceManager&                    resources,
                                            Magnum::SceneGraph::DrawableGroup3D&    group,
                                            Object3D*                               parent);

    protected:

    /**
     * @brief Init model loader
     * 
     * @param manager 
     * @param loaderId 
     * @return Importer 
     */
    static
    Importer initLoaderAndGetImporter(ImporterPlugin& manager, const std::string& loaderId = "AssimpImporter");

    /**
     * @brief Exporte date of obj file (with mtl and texture) toward impoter
     * 
     * @param pathModel 
     * @param Importer 
     * @return flase if file not found
     */
    static
    bool importeDataFromfile(const std::string& pathModel, Importer& importer);


    /**
     * @brief load and configurate the texture found with mtl. If Not found, NullOpt set
     * 
     * @param resources 
     * @param importer 
     * @return Containers::Array<Resource<Containers::Optional<GL::Texture2D>>> 
     */
    static
    Textures loadTextures(GameResourceManager& resources, Importer& importer);

    /**
     * @brief Load all meshes. Meshes that fail to load will be NullOpt.
     * 
     * @param resources 
     * @param importer 
     * @return Meshes 
     */
    static
    Meshes loadMeshes(GameResourceManager& resources, Importer& importer);

    /**
     * @brief   Load all materials. Materials that fail to load will be NullOpt. 
     *          The data will be stored directly in objects later, so save them only temporarily.
     * 
     * @param resources 
     * @param importer 
     * @return Materials 
     */
    static
    Materials loadMaterials(GameResourceManager& resources, Importer& importer);


    /**
     * @brief Load scene with each component load in resources and return object3D
     * 
     * @param resources 
     * @param textures 
     * @param meshes 
     * @param materials 
     * @param importer 
     * @param group 
     * @param parent 
     * @return std::unique_ptr<Object3D>:   pointor of the model create. Use std::move to move this unique pointor to another 
     *                                      i.e : std::unique_ptr pt = std::move(loadModel([...]))
     */
    static
    std::unique_ptr<Object3D> loadScene(GameResourceManager&                    resources,
                                        Textures&                               textures,
                                        Meshes&                                 meshes,
                                        Materials&                              materials,
                                        Importer&                               importer,
                                        Magnum::SceneGraph::DrawableGroup3D&    group, 
                                        Object3D*                               parent);

    /**
     * @brief recusive function to create sub object in model
     * 
     * @param ressourcesManager 
     * @param importer 
     * @param textures 
     * @param meshes 
     * @param materials 
     * @param parent 
     * @param idObject
     */
    static
    void addObject( GameResourceManager& ressourcesManager,
                    Importer& importer,
                    Textures& textures,
                    Meshes& meshes,
                    Materials& materials,
                    Magnum::SceneGraph::DrawableGroup3D&    group,
                    Object3D& parent,
                    Magnum::UnsignedInt idObject);
                    
    private:

};

#endif // _MODEL_LOADER_H