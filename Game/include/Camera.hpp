#ifndef _CAMERA_HPP_
#define _CAMERA_HPP_

#include <Magnum/Math/Color.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Math/FunctionsBatch.h>
#include <Magnum/Platform/Sdl2Application.h>

#include "type.hpp"

class Camera
{
    private:
        
        Magnum::Vector3 cameraRot;
        int xpos,ypos;
        float ForwardVelocity = 0.f;
        float StrafeVelocity = 0.f;
        bool freefly_[6] = {false};

    public:

        float yaw = 0;
        float pitch = 0;
        Magnum::Vector3 cameraPos{0,8,10};
        

        Camera(/* args */);
        ~Camera();


        void mouse_camera(Magnum::Platform::Sdl2Application::MouseMoveEvent& event);
        void camera_pressinput(Magnum::Platform::Sdl2Application::KeyEvent& event);
        void camera_realeaseinput(Magnum::Platform::Sdl2Application::KeyEvent& event);

        void camerafreefly();
        void position_camerafreefly();

        void camerafps();
        void position_camerafps();

};
    
#endif