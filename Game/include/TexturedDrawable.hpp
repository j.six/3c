//Project : 3c
//Editing by Six Jonathan
//Date : 06/02/2020 - 12 h 57

#ifndef _TEXTURED_DRAWABLE_H
#define _TEXTURED_DRAWABLE_H

/*Magnum dependence*/
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Shaders/Phong.h>
#include <Magnum/GL/Mesh.h>

/*STD dependence*/
/*Project file dependence*/
#include "../include/type.hpp"


class TexturedDrawable
    : public Magnum::SceneGraph::Drawable3D
{
    public:
        explicit TexturedDrawable(Object3D& object, Magnum::Shaders::Phong& shader, Magnum::GL::Mesh& mesh, Magnum::GL::Texture2D& texture, Magnum::SceneGraph::DrawableGroup3D& group)
            :   Magnum::SceneGraph::Drawable3D{object, &group},
                shader_(shader),
                mesh_(mesh),
                texture_(texture)
        {}

    private:
        void draw(const Magnum::Matrix4& transformationMatrix, Magnum::SceneGraph::Camera3D& camera) override
        {
            shader_
                .setLightPosition(camera.cameraMatrix().transformPoint({-3.0f, 10.0f, 10.0f}))
                .setTransformationMatrix(transformationMatrix)
                .setNormalMatrix(transformationMatrix.normalMatrix())
                .setProjectionMatrix(camera.projectionMatrix())
                .bindDiffuseTexture(texture_);

            mesh_.draw(shader_);
        }

        Magnum::Shaders::Phong& shader_;
        Magnum::GL::Mesh&       mesh_;
        Magnum::GL::Texture2D&  texture_;
};

#endif // _TEXTURED_DRAWABLE_H


