//Project : workspace.json
//Editing by Six Jonathan
//Date : 05/02/2020 - 17 h 51

#ifndef _TYPE_H
#define _TYPE_H

/*Magnum dependence*/
#include <Magnum/SceneGraph/MatrixTransformation3D.h>
#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/Trade/AbstractImporter.h>
#include <Magnum/Trade/PhongMaterialData.h>
#include <Corrade/Containers/Optional.h>
#include <Corrade/Containers/Pointer.h>
#include <Corrade/Containers/Array.h>
#include <Magnum/ResourceManager.h>
#include <Magnum/GL/Texture.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/GL/AbstractShaderProgram.h>
#include <Magnum/Trade/AbstractImporter.h>


/*STD dependence*/
/*Project file dependence*/

#define CORRADE_POINTER_STL_COMPATIBILITY

/*Alias*/
using Object3D 				= Magnum::SceneGraph::Object<Magnum::SceneGraph::MatrixTransformation3D>;    
using Scene3D 				= Magnum::SceneGraph::Scene<Magnum::SceneGraph::MatrixTransformation3D>;
using Texture				= Magnum::Containers::Optional<Magnum::GL::Texture2D>;
using GameResourceManager 	= Magnum::ResourceManager<Magnum::GL::Mesh, Texture , Magnum::Containers::Pointer<Magnum::GL::AbstractShaderProgram>>;
using Textures              = Magnum::Containers::Array<Magnum::Resource<Texture>>;
using Materials             = Magnum::Containers::Array<Magnum::Containers::Optional<Magnum::Trade::PhongMaterialData>>;
using Meshes                = Magnum::Containers::Array<Magnum::Resource<Magnum::GL::Mesh>>;
using Importer              = Magnum::Containers::Pointer<Magnum::Trade::AbstractImporter>;
using ImporterPlugin        = Magnum::PluginManager::Manager<Magnum::Trade::AbstractImporter>;

#endif // _TYPE_H
