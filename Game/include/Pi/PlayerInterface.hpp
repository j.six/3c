#ifndef __PLAYER_INTERFACE_HPP__
#define __PLAYER_INTERFACE_HPP__

#include <Magnum/Platform/Sdl2Application.h>
#include <Magnum/ImGuiIntegration/Context.hpp>
#include <Magnum/ImGuiIntegration/Widgets.h>
#include <Magnum/Math/Color.h>
#include <Magnum/GL/TimeQuery.h>

/* all the widgets possible*/
#include "Widgets.hpp"
#include "TextButton.hpp"
#include "LifeBar.hpp"
#include "TextBox.hpp"
#include "ManaBar.hpp"
#include "TimeBar.hpp"


/* Pi is the User's interface.
 * it uses ImGui and Put in class*/
namespace Pi
{
	using namespace Magnum;

	class PlayerInterface : public ImGuiIntegration::Context , public Widgets	
	{
		private:
			Vector2 size_;
			ImGuiIO	io_;
			ImFont*	font_;
		public:

			//========== CONSTRUCTORS/DESTRUCTOR =============//
			 PlayerInterface(NoCreateT);
			 PlayerInterface(const PlayerInterface& copyPlayerInterface);
			 PlayerInterface(const Vector2i& windowSize = Vector2i(0,0), 
					 		 const Vector2i& frameBufferSize = Vector2i(0,0), 
							 const Vector2& dpiScaling = Vector2(1,1));
			 
			 PlayerInterface(PlayerInterface&& movedPlayerInterface);

			~PlayerInterface();

			//========= WIDGETS METHODS =============//

			/**
			 * @brief override Widgets func to not do anything
			 */
	inline 	void			 close		()override{}

			/**
			 * @brief draw all what's contained in the children_ with the ImGuiintegration::Context of Magnum
			 * 
			 * @param appli: 	 Platform::Application&, the application that draws in Magnum.
			 * @param deltaTime: const float&.	
			 */
			void 			 draw		(Platform::Application& appli, const float& deltaTime = 0.0f);

			//===========	OPERATORS	================//
	
			/**
			 * @brief 	assign by move what's in the PlayerInterface&&.
			 * 
			 * @param	movedPlayerInterface, PlayerInterface&&.
			 * @warning The movedPlayerInterface should 
			 * 			not be used after assignment.
			 * 
			 * @return 	PlayerInterface&, *this.
			 */
			PlayerInterface& operator=	(PlayerInterface&& movedPlayerInterface);

			//========= CREATION METHODS ===============//

			/**
			 * @brief 			create a position fixed TextBox. 
			 * 
			 * @param rs 		GameResourceManager*, 			 a pointer on the ResourceManager.
			 * @param name 		const std::string&,				 the name_ of the Widgets.
			 * @param message 	const std::vector<std::string>&, a vector of messages.
			 */
			void	PnjTextBox		(GameResourceManager* rs, const std::string& name , const std::vector<std::string>& message);
			
			/**
			 * @brief 			create a LifeBar and a ManaBar at the position chosen
			 * 
			 * @param rs 		GameResourceManager*, 		a pointer on the ResourceManager.
			 * @param name 		const std::string&,			the name_ of the Widgets.
			 * @param life 		float&,						the life of the entity.		
			 * @param mana 		float&,						the mana of the entity.
			 * @param pos 		ImVec2,						the pos of the entity info 
			 * 												between {0.0f,0.0f} (top-left) 
			 * 												and {1.0f,1.0f} (bottom-right) of the window			
			 */
			void	EntityInfo		(GameResourceManager* rs, const std::string& name , float& life, float& mana, ImVec2 pos);

			/**
			 * @brief 			create a TimeBar at the position chosen.
			 * 
			 * @param rs 		GameResourceManager*, 		a pointer on the ResourceManager.
			 * @param name 		const std::string&,			the name_ of the Widgets.
			 * @param time 		float&,						the time we want to display.
			 * @param pos 		ImVec2,						the pos of the entity info 
			 * 												between {0.0f,0.0f} (top-left) 
			 * 												and {1.0f,1.0f} (bottom-right) of the window
			 */
			void	TimeDisp		(GameResourceManager* rs, const std::string& name , float& time, ImVec2 pos);

			/**
			 * @brief 			Create the Start Menu
			 * 
			 * @param rs 		GameResourceManager*, 		a pointer on the ResourceManager.
			 * @param name 		const std::string&,			the name_ of the Widgets.
			 */
			void	StartMenu		(GameResourceManager* rs, const std::string& name);
	};

}/*Pi*/


#endif //__PLAYER_INTERFACE_HPP__
