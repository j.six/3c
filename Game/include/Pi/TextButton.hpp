#ifndef __TEXTBUTTON_HPP__
#define __TEXTBUTTON_HPP__

#include <Magnum/GL/Texture.h>
#include <Magnum/GL/TextureFormat.h>

#include "Widgets.hpp"

namespace ImGui 
{
	/**
	 * @brief 	function creating a button with a text in the middle.
	 * 			the internal function of ImGui has been used for it
	 * 
	 * @param texId: 		 GL::Texture2D, 	the background texture.
	 * @param label: 		 const char*, 	the name_ of the Widgets.
	 * @param imageSize:	 const ImVec2&, 	the size of the Widgets. 
	 * @param uv0:			 const ImVec2&,	the top-left of the part to be taken on texId.
	 * @param uv1:			 const ImVec2&,	the bottom-right of the part to be taken on texId.
	 * @param frame_padding: float,			the distance between element of the image. 
	 * @param bg_col:		 const ImVec4&,	the color of the background.
	 * @param tint_col:		 const ImVec4&,	the color with wich the texture will be displayed.
	 * 
	 */
	IMGUI_API bool ImageButtonWithText(Magnum::GL::Texture2D& texId,const char* label,
	const ImVec2& imageSize = ImVec2(0,0), const ImVec2& uv0 = ImVec2(0,0),
	const ImVec2& uv1 = ImVec2(1,1), float frame_padding = -1.0f, 
	const ImVec4& bg_col = ImVec4(0,0,0,0), const ImVec4& tint_col = ImVec4(1,1,1,1));

}

namespace Pi
{

	class TextButton : public Widgets
	{
		private:
			std::string message_;
		    ImVec4		textColor_;	
		public:
			bool pressed_ = false;


			TextButton(const WidgetsArgs& Args, const ImVec4& textColor = {0,0,0,0});
			TextButton(const std::string& 	message = "" , const WidgetsArgs& Args = WidgetsArgs(), const ImVec4& textColor = {0,0,0,0});
			TextButton(Widgets&& movedWidgets, const std::string& message, const ImVec4& textColor = {0,0,0,0});
			TextButton(TextButton&& movedTextButton);
			
			virtual ~TextButton();

			/**
			 * @brief return the state of the button
			 * 
			 * @return true if pressed, false if not.
			 */
			inline virtual bool active (){return pressed_;}

			/**
			 * @brief display the Widgets.
			 * 
			 * @param parentPos : const ImVec2&.
			 */
			void	draw	(const ImVec2& parentPos = ImVec2(0,0))override;
	
	};

}/*Pi*/

#endif //__TEXTBUTTON_HPP__
