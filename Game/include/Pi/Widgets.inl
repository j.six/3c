#ifndef __WIDGETS_INL__
#define __WIDGETS_INL__

#include "Widgets.hpp"

//================	UPDATE METHODS	================//

 inline	void Widgets::clear	()
{
	context_ 	= Context::OVER;
	children_.clear();
}

 inline	void Widgets::stop	(const std::string& key)
{
	Widgets& checkWidgets = get(key);

	if (checkWidgets != *this)
		checkWidgets.context_ = Context::OVER;	
}

 inline	void Widgets::close 	()
{
	switch (context_)
	{
		case Context::ANIM:
			context_ = Context::CLOSING;
			break;
		case Context::CLOSING:	
			break;
		default:
			context_ = Context::CLOSED;
			break;
	}
}

 inline	void Widgets::next 	()
{
	for (std::unique_ptr<Widgets>& i : children_)
	{
		i->next();
	}
}

//=================	OPERATORS ==================//

inline bool					Widgets::operator==	(const Widgets& value)
{
	return (name_ == value.name_);
}

inline bool					Widgets::operator!=	(const Widgets& value)
{
	return (name_ != value.name_);
}

//==================	BOOLEAN METHODS 	====================//
 inline	bool Widgets::has	(const std::string& key)
{
	for (std::unique_ptr<Widgets>& i : children_)
	{
		if (i->name_ == key)
			return true;

		const Widgets checkWidgets = *i;

		if (i->get(key) != checkWidgets)
			return true;
	}

	return false;
}

 inline	bool Widgets::closed	(const std::string& key)
{
	Widgets& checkWidgets = get(key);

	if (checkWidgets != *this)
		return (checkWidgets.context_ == Context::CLOSED);
	else
		return false;	
}

//=============		DATA MANAGE METHODS		===================//

template<typename T = Widgets>
inline void							Widgets::add	(T&& value)
{
	for (std::unique_ptr<Widgets>& i : children_)
	{
		if (i->name_ == value.name_)
			return;

		const Widgets checkWidgets = static_cast<Widgets>(value);

		if (i->get(value.name_) == checkWidgets)	
			return;

	}

	children_.push_back(std::make_unique<T>(std::move(value)));
}



inline	 Widgets&	Widgets::get (const std::string& key)
{
	for (std::unique_ptr<Widgets>& i : children_)
	{
		if (i->name_ == key)
			return *i;

		const Widgets checkWidgets = *i;

		if (i->get(key) != checkWidgets)
			return i->get(key);
	}

	return *this;
}

#endif /*__WIDGETS_INL__*/
