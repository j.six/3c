#ifndef __TIMEBAR_HPP__
#define __TIMEBAR_HPP__

#include <Magnum/GL/Texture.h>
#include <Magnum/GL/TextureFormat.h>

#include "Widgets.hpp"


namespace ImGui 
{



  	/**
  	 * @brief  function creating a texture with a text on the left.
   	 * it makes appear the two other texture on it depending on timeCoeff 
   	 * the internal function of ImGui has been used for it 
  	 * 
  	 * @param texId:  	 	GL::Texture2D, 	the background texture.
  	 * @param timeId: 	 	GL::Texture2D, 	the Time's Texture.
  	 * @param label:	 	const char*, 	the name_ of the Widgets.		
  	 * @param timeCoeff: 	const float&, 	the coeff between the time left and the time given.
  	 * @param time:			const float&,	the time left, to be print on top of the time's texture.
  	 * @param imageSize:	const ImVec2&, 	the size of the Widgets.
  	 * @param uv0:			const ImVec2&,	the top-left of the part to be taken on texId.
  	 * @param uv1:			const ImVec2&,	the bottom-right of the part to be taken on texId.
  	 * @param frame_padding float,			the distance between element of the image.	
  	 * @param bg_col 		const ImVec4&,	the color of the background.
  	 * @param tint_col 		const ImVec4&,	the color with wich the texture will be displayed.
  	 */
	IMGUI_API void timeBar(
	Magnum::GL::Texture2D& texId, Magnum::GL::Texture2D& timeId,
	const char* 	label, 
	const float& 	timeCoeff	= 1.0f,
	const float&	time		= 0.0f,
	const ImVec2& 	imageSize 	= ImVec2(0,0), 
	const ImVec2& 	uv0 		= ImVec2(0,0), const ImVec2& uv1 = ImVec2(1,1), 
	float frame_padding 		= -1.0f, 
	const ImVec4& bg_col 		= ImVec4(0,0,0,0), 
	const ImVec4& tint_col 		= ImVec4(1,1,1,1));

}

namespace Pi
{

	class TimeBar : public Widgets
	{
		private:
			float&			time_;
			float			maxTime_;
			GL::Texture2D*	timeTex_;
			ImVec4			textColor_;		
		public:

			TimeBar(float& 	time, const WidgetsArgs& Args = WidgetsArgs(), const ImVec4& textColor = {0,0,0,0});
			TimeBar(Widgets&& movedWidgets, float& time, GameResourceManager* rs, const ImVec4& textColor = {0,0,0,0});
			TimeBar(TimeBar&& movedTimeBar);
			~TimeBar();

			/**
			 * @brief 	put the anim_ to 1.0f, if it is equal to 0.0f.
			 * 
			 * @return 	true if anim_ is to 1.0f, false if not.
			 */
			inline bool reset	()override
			{
				if (anim_ == 0.0f)
					anim_ = 1.0f;
				return (anim_ == 1.0f);
			}

			/**
			 * @brief update the anim_ coeff with delta time and animSpeed.
			 * 
			 * @param deltaTime : const float&.
			 */
			void		update	(const float& deltaTime)override;

			/**
			 * @brief display the Widgets.
			 * 
			 * @param parentPos : const ImVec2&.
			 */
			void		draw	(const ImVec2& parentPos = ImVec2(0,0))override;
	
	};

}/*Pi*/

#endif //__TIMEBAR_HPP__


