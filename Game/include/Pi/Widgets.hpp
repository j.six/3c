#ifndef __WIDGETS_HPP__
#define __WIDGETS_HPP__

#include <Magnum/Platform/Sdl2Application.h>
#include <Magnum/ImGuiIntegration/Context.hpp>
#include <Magnum/ImGuiIntegration/Widgets.h>
#include <Magnum/Math/Color.h>

#include <Magnum/ResourceManager.h>

#include "../type.hpp"
#include <list>
#include <memory>
#include <string>
#include <iostream>

namespace Pi
{
	using namespace Magnum;


	struct WidgetsArgs;


	class Widgets
	{
		protected:
			std::string 	name_;
  			GL::Texture2D* 	texture_;
			float			animSpeed_, anim_;

		public:
			ImVec2 		pos_;
			ImVec2 		size_;
			ImVec2		uv0_;
			ImVec2		uv1_;
			ImVec4		color_;

			enum class Context
			{
				ANIM 	= 0,
				DISP 	= 1,
				CLOSING = 2,
				CLOSED	= 3,
				OVER 	= 4
			};

			Context		context_;
			std::list<std::unique_ptr<Widgets>> children_;

			//==================	CONSTRUCTORS/DESTRUCTORS  ======================//
			Widgets();
			Widgets(const WidgetsArgs& Args);
			Widgets(const Widgets& copyWidgets);
			Widgets(Widgets&& movedWidgets);
			
			virtual ~Widgets();

			//=================		OPERATORS	==================//

			/**
			 * @brief 	Assign the values of a widget to another.
			 * @param	movedWidgets: Widgets&&.
			 * @warning The widgets got in parameters should not 
			 * 			be used anymore.
			 * @return 	The assigned Widgets: *this.
			 */
			Widgets&	operator=	(Widgets&& movedWidgets);

			/**
			 * @brief 	Verify that the Widgets got in parameter
			 *			is the same as *this.
			 *
			 *			Two Widgets cannot have the same name so,
			 *			it just compares the name.
			 *
			 *@param	value: const Widgets&.
			 *@return	true if it is, false if it isn't
			 */
			inline bool	operator==	(const Widgets& value);
			
			/**
			 *@brief 	Verify that the Widgets got in parameter
			 *			is not the same as *this.
			 *
			 *			Two Widgets cannot have the same name so,
			 *			it just compares the name.
			 *
			 *@param	value: const Widgets&.
			 *@return	true if it isn't, false if it is.
			 */
			inline bool	operator!=	(const Widgets& value);

			//================	UPDATE METHODS	================//

			/**
			 * @brief 	update the animation counter 
			 * 			anim_ of each children_
			 * 
			 * @param deltaTime 
			 */
			virtual 		void update	(const float& deltaTime);
			
			/**
			 * @brief reset the anim_ to 1.0f
			 * 
			 * @return true  
			 */
			virtual inline  bool reset	(){anim_ = 1.0f;return true;}
			
			/**
			 * @brief clear the list of children_
			 */
			virtual inline	void clear	();	
			
			/**
			 * @brief set context_ to Context::OVER the children with the following key
			 * 
			 * @param key : const std::string&
			 */
			virtual inline	void stop	(const std::string& key);
			
			/**
			 * @brief set context_ to Context::CLOSING or Context::CLOSED
			 */
			virtual inline	void close 	();

			/**
			 * @brief set to the next step of animation.
			 */
			virtual inline	void next 	();

			//================	BOOLEAN METHODS ===============//

			/**
			 * @brief 	see if the following Widget is a Button
			 *  		pressed.
			 *
			 * @return 	false.
			 */
			virtual inline 	bool active	(){return false;}

			/**
			 * @brief 	see if the context_ of the Widgets
			 * 			with the following key is Context::Closed.
			 * 
			 * @param key : const std::string& .
			 * 
			 * @return false if the context_ is not Context::CLOSED,
			 * @return true	 if it is.
			 */
			virtual inline	bool closed	(const std::string& key);

			/**
			 * @brief 	check if one of the children_
			 * 			has the same name_ has the key.
			 * 
			 * @param key : const std::string&.
			 * @return true		if the Widgets got or one of 
			 * 					its children_ has such Widgets, 
			 * @return false 	if it has none.
			 */
			virtual inline	bool has	(const std::string& key);


			//================ DISPLAY METHODS ===============//
			/**
			 * @brief display the widget on screen.
			 * 
			 * @param parentPos 
			 */
			virtual 		void draw 	(const ImVec2& parentPos = ImVec2(0,0));


			//===============	DATA METHODS	===================//
			/**
			 * @brief 	add a Widgets or a subclass of Widgets 
			 * 			in the list of children_ of this Widgets.
			 * 
			 * @tparam 	Widgets or a subclass of Widgets .
			 * @param 	value : T&& (a Widgets).
			 */
			template<typename T = Widgets>
			inline void					add	(T&& value);	

			/**
			 * @brief 	get the Widgets with the following key
			 * 			if it has it, or returns himself.
			 * 
			 * @param key : std::string&.
			 * @return Widgets&  : *this or a Widgets with the name_ of the key.
			 */
			inline	virtual Widgets&	get (const std::string& key);

			/**
			 * @brief load a texture with Magnum's function
			 * 
			 * @param texPath : std::string&
			 * @return GL::Texture2D : a Magnum's texture info class 
			 */
			GL::Texture2D	loadTexture	(const std::string& texPath);

	};

	struct WidgetsArgs
	{
		GameResourceManager*		rs;	
		const std::string&			texPath;
		const std::string& 			name;
		const ImVec2& 				pos;		
		const Vector2i& 			windowSize;
		const ImVec2& 				uv0, uv1;
		const ImVec4&				color;
		const Widgets::Context		flags;
		const float					animSpeed;


		WidgetsArgs(GameResourceManager*		_rs			= nullptr, 
					const std::string&			_texPath 	= "",
					const std::string& 			_name 		= "Main", 
					const ImVec2& 				_pos 		= ImVec2(0,0),
					const Vector2i& 			_windowSize = Vector2i(0,0),
					const ImVec4&				_color		= {0,0,0,0},
					const ImVec2& 				_uv0 		= {0,0},
					const ImVec2& 				_uv1 		= {1,1},
					const Widgets::Context&		_flags		= Widgets::Context::DISP,
					const float					_animSpeed	= 1):							
			rs 			{_rs},
			texPath 	{_texPath},
			name		{_name},
			pos			{_pos},
			windowSize 	{_windowSize},
			uv0			{_uv0},
			uv1			{_uv1},
			color		{_color},
			flags		{_flags},
			animSpeed	{_animSpeed}
		{}
											
		~WidgetsArgs(){}
	};

#include "Widgets.inl"

}/*Pi*/


#endif //__WIDGETS_HPP__
