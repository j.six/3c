#ifndef __LIFEBAR_HPP__
#define __LIFEBAR_HPP__

#include <Magnum/GL/Texture.h>
#include <Magnum/GL/TextureFormat.h>

#include "Widgets.hpp"


namespace ImGui 
{

  /* function creating a texture with a text on the left.
   * it makes appear the two other texture on it depending on lifeCoeff 
   * the internal function of ImGui has been used for it */

  	/**
  	 * @brief 	function creating a texture with a text on the left.
     * 			it makes appear the two other texture on it depending on lifeCoeff 
     * 			the internal function of ImGui has been used for it
  	 * 
  	 * @param texId:  	 	 GL::Texture2D, 	the background texture.
  	 * @param lifeId:		 GL::Texture2D, 	the Life's Texture.
  	 * @param damageId: 	 GL::Texture2D, 	the Damage's Texture.
  	 * @param label: 		 const char*, 		the name_ of the Widgets.		
  	 * @param lifeCoeff: 	 const float&, 		the coeff between the life left and the maxLifes.
  	 * @param imageSize: 	 const ImVec2&, 	the size of the Widgets.
  	 * @param uv0: 			 const ImVec2&,		the top-left of the part to be taken on texId.
  	 * @param uv1: 			 const ImVec2&,		the bottom-right of the part to be taken on texId.
  	 * @param frame_padding: float,				the distance between element of the image.
  	 * @param bg_col: 		 const ImVec4&,		the color of the background.	
  	 * @param tint_col: 	 const ImVec4&,		the color with wich the texture will be displayed.
  	 */
	IMGUI_API void lifeBar(
	Magnum::GL::Texture2D& texId, Magnum::GL::Texture2D& lifeId, Magnum::GL::Texture2D& damageId,
	const char* 	label, 
	const float& 	lifeCoeff	= 1.0f,
	const ImVec2& 	imageSize 	= ImVec2(0,0), 
	const ImVec2& 	uv0 		= ImVec2(0,0), const ImVec2& uv1 = ImVec2(1,1), 
	float frame_padding 		= -1.0f, 
	const ImVec4& bg_col 		= ImVec4(0,0,0,0), 
	const ImVec4& tint_col 		= ImVec4(1,1,1,1));

}


namespace Pi
{

	class LifeBar : public Widgets
	{
		private:
			float&			life_;
			float			maxLife_;
			GL::Texture2D*	lifeTex_;
			GL::Texture2D*	damageTex_;
			ImVec4			textColor_;	
		public:

			LifeBar( 	float& 					life,
						const float&			maxLife,
						const WidgetsArgs&		Args 		= WidgetsArgs(),
						const ImVec4&			textColor	= {0,0,0,0});

			LifeBar(Widgets&& movedWidgets, float& life, 			 const float& 	maxLife, 
											GameResourceManager* rs, const ImVec4&	textColor = {0,0,0,0});
			LifeBar(LifeBar&& movedLifeBar);

			
			~LifeBar();

			/**
			 * @brief 	put the anim_ to 1.0f, if it is equal to 0.0f.
			 * 
			 * @return 	true if anim_ is to 1.0f, false if not.
			 */
			inline bool reset	()override
			{
				if (anim_ == 0.0f)
					anim_ = 1.0f;
				return (anim_ == 1.0f);
			}

			/**
			 * @brief update the anim_ coeff with delta time and animSpeed.
			 * 
			 * @param deltaTime : const float&.
			 */
			void		update	(const float& deltaTime)override;

			/**
			 * @brief display the Widgets.
			 * 
			 * @param parentPos : const ImVec2&.
			 */
			void		draw	(const ImVec2& parentPos = ImVec2(0,0))override;
	
	};

}/*Pi*/

#endif //__LIFEBAR_HPP__


