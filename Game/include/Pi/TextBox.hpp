#ifndef __TEXTBOX_HPP__
#define __TEXTBOX_HPP__

#include <Magnum/GL/Texture.h>
#include <Magnum/GL/TextureFormat.h>

#include "Widgets.hpp"


namespace ImGui 
{
  	/**
  	 * @brief 	function creating a texture with a text starting from the left.
	 * 			the internal function of ImGui has been used for it
  	 * 
  	 * @param texId:			GL::Texture2D, 	the background texture.
  	 * @param owner: 			const char*, 	the name_ of the Widgets.
  	 * @param label: 			const char*, 	the message to be displayed Widgets.
  	 * @param imageSize: 		const ImVec2&, 	the size of the Widgets. 
  	 * @param uv0: 				const ImVec2&,	the top-left of the part to be taken on texId.
  	 * @param uv1: 				const ImVec2&,	the bottom-right of the part to be taken on texId.
  	 * @param frame_padding: 	float,			the distance between element of the image. 
  	 * @param bg_col: 			const ImVec4&,	the color of the background.
  	 * @param tint_col: 		const ImVec4&,	the color with wich the texture will be displayed.
  	 */
	IMGUI_API void textBox(
	Magnum::GL::Texture2D& texId,
	const char*		owner,
	const char* 	label, 
	const ImVec2& 	imageSize 	= ImVec2(0,0), 
	const ImVec2& 	uv0 		= ImVec2(0,0), const ImVec2& uv1 = ImVec2(1,1), 
	float frame_padding 		= -1.0f, 
	const ImVec4& bg_col 		= ImVec4(0,0,0,0), 
	const ImVec4& tint_col 		= ImVec4(1,1,1,1));

}


namespace Pi
{

	class TextBox : public Widgets
	{
		private:
			unsigned int 				phrasePart_ = 0;
			std::vector<std::string> 	message_;
			ImVec4						textCol_;
		public:

			TextBox(const WidgetsArgs& Args 	= WidgetsArgs(), 	const ImVec4& textCol = {1,1,1,1});
			TextBox(const std::vector<std::string>& message, const ImVec4& textCol,	const WidgetsArgs& Args = WidgetsArgs());
			TextBox(Widgets&& movedWidgets, 	const std::vector<std::string>& message, const ImVec4& textCol);
			TextBox(TextBox&& movedTextBox);

			/**
			 * @brief go to the next step of animation.
			 */
			inline void next	()override
			{

				/* call next for children_ */
				for (std::unique_ptr<Widgets>& i : children_)
				{
					i->next();
				}

				/* Guards : return if not animated */
				if (context_ ==  Context::OVER || context_ == Context::DISP || context_ == Context::CLOSED)
					return;

				/* if the animation did not displayed fullys: return*/
				if (!reset())
					return;

				/* get the next message */
				phrasePart_++;

				/* close the window if next is called and no message is left*/
				if (phrasePart_ >= message_.size() && context_ == Context::ANIM)
				{
					context_ 	= Context::CLOSING;
					reset();
					phrasePart_--;
					return;
				}

				/* making it closed if next is called on closing */
				if (context_ == Context::CLOSING)
				{
					context_ 	= Context::CLOSED;
					return;
				}
			}

			/**
			 * @brief close the window or put the context on closing.
			 */
			inline void close	()override
			{
				if (context_ != Context::CLOSING)
					anim_ = (1.0f - anim_);	
				Widgets::close();
			}	

			/**
			 * @brief get the anim_ coeff to 1.0f if it is at 0.0f.
			 * 
			 * @return true if anim_ is at 1.0f, else return false.
			 */
			inline bool reset	()override
			{
				if (anim_ == 0.0f)
					anim_ = 1.0f;
				return (anim_ == 1.0f);
			}

			/**
			 * @brief update the anim_ coeff by the deltaTime coeff.
			 * 
			 * @param deltaTime : const float&.  
			 */
			void		update	(const float& deltaTime)override;
			~TextBox();

			/**
			 * @brief display the Widgets
			 * 
			 * @param parentPos : const ImVec2&.
			 */
			void	draw	(const ImVec2& parentPos = ImVec2(0,0))override;
	
	};

}/*Pi*/

#endif //__TEXTBOX_HPP__


