//Project : 3c
//Editing by Six Jonathan
//Date : 13/02/2020 - 10 h 40

#ifndef _PNJ_H
#define _PNJ_H

/*Magnum dependence*/
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>

/*STL dependence*/
#include <vector>
#include <string>

/*Project file dependence*/
#include "../include/type.hpp"
#include "../include/modelLoader.hpp"

class Pnj
{
    public:

        explicit Pnj ()						= delete;
        explicit Pnj (const Pnj& other)		= default;
        explicit Pnj (Pnj&& other)			= default;
        virtual ~Pnj ()				        = default;


        /**
         * @brief Construct a new Pnj object
         * 
         * @param pos           :   transformation position
         * @param rot           :   transformation rotation
         * @param scale         :   transformation scale        
         * @param pathModel     :   The relative path of the .Obj file. Start to the Game folder
         *                          i.e : Game/resources/meshes/[...].obj
         * @param resources     :   The resource manager of the game to avoid to load multiple time any asset
         * @param parent        :   Pointor to the parent of the models loaded. If nullptr, the model doesn't have parent.
         *                          Remberber that SceneGraph is an Object3D. So if you want add this model to the world add the pointor of the scene.
         *                          Else use function setParent() to assign a new parent.
         */
        explicit Pnj (  const std::string&      name,
                        const Magnum::Vector3&  pos,
                        const Magnum::Vector3&  rot,
                        const Magnum::Vector3&  scale,
                        const std::string&      pathModel,
                        GameResourceManager&    resources,
                        Object3D*               parent);

        void addModel (const std::string&      pathModel,
                        GameResourceManager&   resources,
                        Object3D*              parent)
        {
            drawables_.emplace_back();
            models_.emplace_back(std::move(ModelLoader::loadModel(pathModel, resources, drawables_.back(), parent)));

            models_.back()->scale(scale_)
                    .rotateZ(Magnum::Rad(rotation_.z()))
                    .rotateX(Magnum::Rad(rotation_.x()))
                    .rotateY(Magnum::Rad(rotation_.y()))
                    .translate(position_);
        }

        /**
         * @brief Add dialogue to the Pnj, can be display with dialogue box
         * 
         * @param text 
         */
        void addDialog (const std::string& text)
        { 
            if(!text.empty())
            {
                dialogues_.emplace_back(text);
            }
        }

        void changSetAnimation (unsigned int index) { animationStep_ = index;}

        /**
         * @brief Get the Dialogues object
         * 
         * @return const std::vector<std::string>& 
         */
        const std::vector<std::string>& getDialogues () const { return dialogues_; }

        /**
         * @brief Get the Name object
         * 
         * @return const std::string& 
         */
        const std::string& getName () const { return name_;}

        /**
         * @brief Get the Position object
         * 
         * @return const Magnum::Vector3& 
         */
        const Magnum::Vector3& getPosition () const { return position_; }

        /**
         * @brief Draw the Pnj 
         * 
         * @param camera 
         */
        void draw(Magnum::SceneGraph::Camera3D& camera) { camera.draw(drawables_[animationStep_]);}
        
        void orientTowardPoint(const Magnum::Vector3& pos)
        {
            Magnum::Matrix4 lookAt = Magnum::Matrix4::lookAt(pos, position_, Magnum::Vector3{0.f, 1.f, 0.f});
            lookAt[0][3] = 0.f;
            lookAt[1][3] = 0.f;
            lookAt[2][3] = 0.f;
            lookAt[3][3] = 1.f;

            lookAt[3][0] = 0.f;
            lookAt[3][1] = 0.f;
            lookAt[3][2] = 0.f;

            for (auto &&model : models_)
            {
                model->setTransformation(lookAt);
                model->scale(scale_);
                model->translate(position_);
            }
        }

        /**
         * @brief return true if player collided with pnj. Collider box is a sphere of radius 1
         * 
         * @param playerPos 
         * @return true 
         * @return false 
         */
        bool playerHitPnj(const Magnum::Vector3& playerPos)
        {
            Magnum::Vector3 distenWithPnj(position_ - playerPos);
            return hitSphereRadius_ >= distenWithPnj.length();
        }

    protected:

        Magnum::Vector3             position_,
                                    rotation_,
                                    scale_;
        std::vector<std::string>    dialogues_;
        std::string                 name_;

        std::vector<std::unique_ptr<Object3D>>              models_;
        std::vector<Magnum::SceneGraph::DrawableGroup3D>    drawables_;
        unsigned int                animationStep_ = 0;

        float                               hitSphereRadius_ = 2.5f;

    private:

};

#endif // _PNJ_H