#ifndef _CHEST_HPP_
#define _CHEST_HPP_

#include "Player.hpp"
#include "type.hpp"
#include "Pi/PlayerInterface.hpp"

class Chest
{
    private:

        float rayon = 1;

    public:

        float moneyin = 100 ;
        Magnum::Vector3 position{-23.7f,2.5f,-8.5f}; // -23.7 / 2.5f / -8.5f
        std::unique_ptr<Object3D> modelchest_;

        void player_in_zone(Player& player,bool openit, GameResourceManager& ressources,Pi::PlayerInterface& textbox);
};

#endif 
