#ifndef __GAME_HPP__
#define __GAME_HPP__

/*Magnum dependence*/
#include <Magnum/Platform/Sdl2Application.h>
#include <Magnum/ImGuiIntegration/Widgets.h>
#include <Magnum/ImGuiIntegration/Context.hpp>
#include <Magnum/Image.h>
#include <Magnum/GL/Buffer.h>
#include <Magnum/GL/DefaultFramebuffer.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/GL/PixelFormat.h>
#include <Magnum/GL/Renderer.h>
#include <Magnum/Math/Color.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Math/FunctionsBatch.h>
#include <Magnum/MeshTools/Interleave.h>
#include <Magnum/MeshTools/CompressIndices.h>
#include <Magnum/MeshTools/Compile.h>
#include <Magnum/Primitives/Grid.h>
#include <Magnum/Primitives/Cube.h>
#include <Magnum/Primitives/Plane.h>
#include <Magnum/Primitives/UVSphere.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/MatrixTransformation3D.h>
#include <Magnum/SceneGraph/Object.h>
#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/Shaders/Flat.h>
#include <Magnum/Shaders/Phong.h>
#include <Magnum/Shaders/VertexColor.h>
#include <Magnum/Trade/MeshData3D.h>
#include <Magnum/ResourceManager.h>

/*STL dependence*/
#include "Player.hpp"
#include "Enemy.hpp"
#include "Camera.hpp"
#include "World/World.hpp"
#include "time.h"

/*Project file dependence*/
#include "DeltaTime.hpp"
#include "Pi/PlayerInterface.hpp"
#include "World/City.hpp"
#include "type.hpp"
#include "Pi/PlayerInterface.hpp"

namespace Magnum {

class Donata : public Platform::Application 
{
	enum class Context
	{
		START 	= 0,
		RUNNING = 1,
		ANIM	= 2,
		OVER	= 6

	};

	private:
		Pi::PlayerInterface			PI_;
		GameResourceManager 		resourceManager_;
		DeltaTime					deltaTime_;

		std::unique_ptr<World>		currentWorld_;

		Shaders::Phong _vertexColorShader{NoCreate};
		Shaders::Flat3D _flatShader{NoCreate};
		GL::Mesh 	_mesh{NoCreate},
					_grid{NoCreate},
					_cube{NoCreate}, 
					gameplane{NoCreate},
					_sphere{NoCreate};

		Scene3D 					_scene;
		SceneGraph::DrawableGroup3D _drawables;
		Object3D* 					_cameraObject;
		SceneGraph::Camera3D* 		_camera;
		Object3D* 					plane[3];

		Player 		player;

		//Arena* arena;

		bool mouse[4] = {false};
		bool zone[4] = {false};
		Context context_ = Context::START;

		// =============== Camera =============== // 

		bool camerafree_ = false;
		bool camerafps_ = false;
		bool camera3p_ = false;

		Camera cam;


	public :

		void drawEvent() override;
		void check_zone();
		void camerafreefly();
		void freeflyinput(KeyEvent& event);

		explicit Donata(const Arguments& arguments);
			~Donata();

		/* Input Methods */
		void viewportEvent		(ViewportEvent& event) override;

		void keyPressEvent		(KeyEvent& event) override;
		void keyReleaseEvent	(KeyEvent& event) override;
		void resetKey 			(); 

		void mousePressEvent	(MouseEvent& event) override;
		void mouseReleaseEvent	(MouseEvent& event) override;
		void mouseMoveEvent		(MouseMoveEvent& event) override;
		void mouseScrollEvent	(MouseScrollEvent& event) override;

		void textInputEvent		(TextInputEvent& event) override;

		/* ticke event */

		void tickEvent() override;	

		/* Draw Methods */
		void setToDraw		();
		void drawStart		();	
													
};


} //!namespace Magnum 

#endif //__GAME_HPP__
