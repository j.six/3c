#ifndef _PLAYER_HPP_
#define _PLAYER_HPP_

#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/MatrixTransformation3D.h>
#include <Magnum/SceneGraph/Object.h>
#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/Math/Color.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Math/FunctionsBatch.h>

#include <memory>
#include <vector>

#include "../include/type.hpp"
#include "../include/Fighter.hpp"

class DrawObject;

struct S_PlayerControle
{
    bool moveForward        = false;
    bool moveBackward       = false;
    bool moveLeft           = false;
    bool moveRight          = false;
    bool turnLeft           = false;
    bool turnRight          = false;
    bool useCapa1           = false;
    bool useCapa2           = false;
    bool useCapa3           = false;
    bool useCapa4           = false;
};

class Player
{
    protected:

        Magnum::Vector3 direction_  {0.f ,0.f ,1.f}; //unit vector

        float           rot_        = 0.f;     //in rad
        float           moveSpeed_  = 4.f;     //in m/sec
        float           turnSpeed_  = M_PI;    // in rad/sec
        float           walkAnim_   = 0.f;
        float           walkTime_   = 0.03f;


    public:
        Magnum::Vector3 scale_      {0.9f, 0.9f, 0.9f};
        unsigned int    stepAnim_   = 0;
        float           money       = 0.f;
        float           life        = 100.f;
        float           mana        = 100.f;
        Magnum::Vector3 position    {0.6f ,2.3f ,18.5f};

       /* Player ()						        = delete;
        explicit Player (const Player& other)	= default;
        explicit Player (Player&& other)		= default;
        virtual ~Player ()				        = default;*/

        /**
         * @brief rotate player arround his Y axis
         * 
         * @param rad 
         */
        void rotateY (float rad);
        void resetOrientation () {direction_ = {0.f ,0.f ,1.f}; rot_ = 0.f;}

        const Magnum::Vector3& getDirection() const { return direction_;}
        float getRotRad() const { return rot_;}
        float getRotDeg() const { return rot_ * 180.f / static_cast<float>(M_PI);}

        void setParent (Object3D& parent)
        {
            for (auto &&model : models)
            {
                model->setParent(&parent);                
            }            

            for (auto &&hero : heros_)
            {
                hero->modelfighter_->setParent(&parent);
            }  
        }

        std::vector<std::unique_ptr<Object3D>>              models;
        std::vector<Magnum::SceneGraph::DrawableGroup3D>    drawables;

        S_PlayerControle                            action;
        bool                                        fight  = false;
        std::vector<std::unique_ptr<Fighter>>       heros_ ;

        /**
         * @brief Chreck action that the player can do and execut it
         * 
         */
        void update (float deltaTime);

        /**
         * @brief Add hero into player. Hero is the fighter
         * 
         * @param life 
         * @param attspeed 
         * @param damage 
         * @param mana 
         * @param pathname
         */
        void addHero(E_FighterType type, float life, float attspeed, float damage, float mana,const std::string& pathname, const std::string& name, GameResourceManager& ressources, Object3D* parent);

        /**
         * @brief Few action that the player can do
         * 
         */
        void moveForward    (float deltaTime);
        void moveBackward   (float deltaTime);
        void moveLeft       (float deltaTime);
        void moveRight      (float deltaTime);
        void turnLeft       (float deltaTime);
        void turnRight      (float deltaTime);
        void resetAction    ();

};

#endif
