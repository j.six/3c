#ifndef _ENEMY_H_
#define _ENEMY_H_

#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/MatrixTransformation3D.h>
#include <Magnum/SceneGraph/Object.h>
#include <Magnum/SceneGraph/Scene.h>

#include "Player.hpp"
#include "Fighter.hpp"

#include <memory>

using Object3D = Magnum::SceneGraph::Object<Magnum::SceneGraph::MatrixTransformation3D>;

class Enemy
{
    private:

        // Data
        float life = 30;
        float rayon = 2;

    public: 

        std::vector<std::unique_ptr<Fighter>>   monsters_;
        Magnum::Vector3                         position_;
        std::unique_ptr<Object3D>               model; //TODO: in protected

        bool inFight_ = false;
        bool isDead_ = false;

        // Constructor
        Enemy(const Magnum::Vector3& pos);
        ~Enemy() = default;


        // Methods
        void addMonster(E_FighterType type, float life,float attspeed,float damage,float mana,const std::string& pathname, const std::string& name, GameResourceManager& ressources, Object3D* parent);
        void engage_fight(Player* p1);

        void setParent (Object3D& parent)
        {
            model->setParent(&parent);

            for (auto &&monster : monsters_)
            {
                monster->modelfighter_->setParent(&parent);
            }  
        }


};

#endif 
