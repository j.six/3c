#ifndef _FIGHTER_HPP_
#define _FIGHTER_HPP_


#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/MatrixTransformation3D.h>
#include <Magnum/SceneGraph/Object.h>
#include <Magnum/SceneGraph/Scene.h>
#include <Magnum/Math/Color.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Math/FunctionsBatch.h>
#include <Magnum/DebugTools/DebugTools.h>

#include <string>
#include <vector>

#include "type.hpp"
#include "modelLoader.hpp"

enum class E_FighterType
{
    Mage,
    Warrior,
    Assassin
};


struct S_Capacity
{
    std::string name;
    float timeDelay;
    float range;
    float manaCost;
};


class Fighter
{
    protected:
        float moveSpeed_;
        float attSpeed_;
        float damage_;
        std::vector<S_Capacity> capacities_;
        int capacityUse = -1; //-1 == Don't use capacity
        float delayAttack_ = 0.f;
        Magnum::Vector3 direction_                  {0.f ,0.f ,1.f}; //unit vector
        float                                       rot_  = 0.f; //in rad
        Magnum::SceneGraph::DrawableGroup3D         drawable_;

    public:

		std::string					name_;
        Magnum::Vector3             position_;
        std::unique_ptr<Object3D>   modelfighter_;
        std::string                 pathname_;
        float                       life_;
        float                       rotatey;

        float   mana_;
        float   initiative_;
        int     order = 1;

        // Constructor
        Fighter() = default;
        virtual ~Fighter() {};
        Fighter(const Fighter&) = default;

        Fighter(float life, float attSpeed, float damage,float mana, const std::string& pathname, const std::string& name, GameResourceManager& ressources, Object3D* parent);

        const S_Capacity& getCapacity(unsigned int index)
        { 
            CORRADE_ASSERT((index > capacities_.size() - 1), "Capacity unknow", capacities_[0]);
            return capacities_[index];   
        }

        bool isDead() { return life_ <= 0.f; }

        /**
         * @brief   Move and considerate the move speed of fighter.
         *          Orient fighter toward this point
         * 
         */
        void moveToward(const Magnum::Vector3& target, float deltaTime)
        {
            position_ += (target - position_).normalized() * moveSpeed_ * deltaTime;
            Magnum::Matrix4 lookAt = Magnum::Matrix4::lookAt(target, position_, Magnum::Vector3{0.f, 1.f, 0.f});
            lookAt[0][3] = 0.f;
            lookAt[1][3] = 0.f;
            lookAt[2][3] = 0.f;
            lookAt[3][3] = 1.f;

            lookAt[3][0] = 0.f;
            lookAt[3][1] = 0.f;
            lookAt[3][2] = 0.f;
            
            modelfighter_->setTransformation(lookAt)
                    .scale      ({0.7,0.7,0.7})
                    .translate  (position_);
        }

        Magnum::SceneGraph::DrawableGroup3D& getDrawable () { return drawable_;}

        float getRotRad() const { return rot_;}

        /**
         * @brief Move whitout considerate the move speed of fighter
         * 
         */
        void translate (const Magnum::Vector3& translation)
        {
            position_ += translation;
            modelfighter_->resetTransformation()
                        .scale({0.7,0.7,0.7})
                        .rotateY(static_cast<Magnum::Rad>(rotatey))
                        .translate(position_);
        }

        virtual void attack1(Fighter& fighter){(void)fighter;}
        virtual void attack2(Fighter& fighter){(void)fighter;}
        virtual void attack3(Fighter& fighter){(void)fighter;}
        virtual void attack4(Fighter& fighter){(void)fighter;}

        bool isUsingCapacity() { return capacityUse != -1; }

        virtual void execut(float deltaTime)
        {
            if(isUsingCapacity())
            {
                delayAttack_ -= deltaTime;

                if(delayAttack_ <= 0)
                {
                    delayAttack_ = 0.f;
                    capacityUse = -1;
                }
            }
        }

        /**
         * @brief Rotate 
         * 
         * @param rad 
         */
        void rotateY (float rad)
        {
            float exDirX = direction_.x();
            float cosR = cosf(rad);
            float sinR = sinf(rad);

            direction_.x() = exDirX * cosR - direction_.z() * sinR;
            direction_.z() = exDirX * sinR + direction_.z() * cosR;

            rot_ -= rad;

            if (rot_ > static_cast<float>(M_PI) * 2.f)
            {
                rot_ -= static_cast<float>(M_PI) * 2.f;
            }
            else if (rot_ < 0.f)
            {
                rot_ += static_cast<float>(M_PI) * 2.f;
            }
        }

        /**
         * @brief Few action that the fighter can do
         * 
         */
        void moveForward    (float detlaTime);
        void moveBackward   (float detlaTime);
        void moveLeft       (float detlaTime);
        void moveRight      (float detlaTime);
        void turnLeft       (float detlaTime);
        void turnRight      (float detlaTime);

        // Methods
        //void loadFighter(GameResourceManager& ressources, Scene3D& scene_);
        void attack_fighter(Fighter& fighter);
        void rotate_Y(float rad);
};


class FighterMage : public Fighter
{
    private:
    public:

        virtual ~FighterMage() = default;

        FighterMage(float life, float attSpeed, float damage,float mana, const std::string& pathname, const std::string& name, GameResourceManager& ressources, Object3D* parent);

        void attack1(Fighter& fighter) override; // basic attack
        void attack2(Fighter& fighter) override; // give mana
        void attack3(Fighter& fighter) override; // give heal
        void attack4(Fighter& fighter) override; // spend all mana to do dmg
};

class FighterWarrior : public Fighter
{
    private:
    public:
        virtual ~FighterWarrior() = default;

        FighterWarrior(float life, float attSpeed, float damage,float mana, const std::string& pathname, const std::string& name, GameResourceManager& ressources, Object3D* parent);
        
        void attack1(Fighter& fighter) override; // basic attack
        void attack2(Fighter& fighter) override; // do more dmg spend more mana 
        void attack3(Fighter& fighter) override; // push enemy and do little dmg
        void attack4(Fighter& fighter) override; // spend all mana to do dmg 
};

class FighterAssassin : public Fighter
{
    private:
    public:
        virtual ~FighterAssassin() = default;

        FighterAssassin(float life, float attSpeed, float damage,float mana, const std::string& pathname, const std::string& name, GameResourceManager& ressources, Object3D* parent);

        void attack1(Fighter& fighter) override; // basic attack 
        void attack2(Fighter& fighter) override; // give multiple attack
        void attack3(Fighter& fighter) override; // dash on front of u 
        void attack4(Fighter& fighter) override; // spend all mana to do dmg 
};


#endif 
