#ifndef _ARENA_HPP_
#define _ARENA_HPP_

#include <Magnum/Platform/Sdl2Application.h>

/*STD dependence*/
#include <vector>
#include <memory>

/*Project file dependence*/

#include "../Enemy.hpp"
#include "../type.hpp"
#include "Zone.hpp"
#include "World.hpp"

enum class E_ArenaContext
{
    arriveCinematic,
    zoomCinematicOnFirstPlayer,
    battle,
    playerTurn,
    enemyTurn
};

struct S_Fighter
{
    Fighter*    fighter;
    bool        isEnemy;
};

class Arena : public World
{
    public:

        Arena ()						= delete;
        Arena (const Arena& other)		= default;
        Arena (Arena&& other)			= default;
        virtual ~Arena () = default;

        Arena (GameResourceManager& ressources, Player& player, Object3D& cameraObject, Magnum::SceneGraph::Camera3D& camera, 
				Pi::PlayerInterface& playerInterface);

        void execut             (float deltaTime) final;
        void draw               () final;
        void keyPressEvent		(Magnum::Platform::Application::KeyEvent& event) final;
        void keyReleaseEvent	(Magnum::Platform::Application::KeyEvent& event) final;
        void mousePressEvent	(Magnum::Platform::Application::MouseEvent& event) final;
        void mouseReleaseEvent	(Magnum::Platform::Application::MouseEvent& event) final;

        void entranceInto       (Enemy& enemy);

        /**
         * @brief True if all player or all monster is dead. If battle is End, reset arena
         * 
         * @return true 
         * @return false 
         */
        bool battleIsEnd(bool& playerWin);

        /**
         * @brief Return enemy
         * 
         * @return Enemy* 
         */
        Enemy* getEnemy () const { return enemy_;}
        void resetEnemy () { enemy_ = nullptr;}

    protected:

        void initializeMap(GameResourceManager& ressources);
        void turnEnemy(S_Fighter& monsterEngaged, float deltaTime);
        void turnPlayer(S_Fighter& heroEngaged, float deltaTime);

        std::unique_ptr<Object3D>   pMap_;
        Zone                        figthZone_;
        Enemy*                      enemy_;
        E_ArenaContext              arenaContext_;
        float                       timeToattack = 10.f;

        float                       rotationCameraCinematicArrive_;
        Magnum::Vector3             posCameraCinematicArrive_;

        float                       lerpStepForZoomAnimation_;

        GameResourceManager&        ressources_;
        std::vector<S_Fighter>      fighterOrder_; /*The order of fighter depending of there initiative*/
        
        Fighter*                    fighterFocusByPlayer_;
        unsigned int                stepFighterFocusByPlayer_;

        int countTurn;

        /**
         * @brief   Do learp with each value of vector.
         *          Computes a+t*(b−a),
         *          i.e. the linear interpolation between a and b for the parameter t (or extrapolation, when t is outside the range [0,1])
         * 
         * @param a 
         * @param b 
         * @param t : is outside the range [0,1]
         * @return Magnum::Vector3<float> 
         */
        Magnum::Vector3 lerp (const Magnum::Vector3& a, const Magnum::Vector3& b, float t)
        {
            Magnum::Vector3 rst;
            
            rst.x() = Magnum::Math::lerp<float>(a.x(), b.x(), t);
            rst.y() = Magnum::Math::lerp<float>(a.y(), b.y(), t);
            rst.z() = Magnum::Math::lerp<float>(a.z(), b.z(), t);

            return rst;
        }

        /**
         * @brief Return the fighter adverse most near than other
         * 
         * @param fighter 
         * @return Fighter* 
         */
        Fighter* foundEnemyMostNear (S_Fighter& fighter)
        {
            Fighter* rst = nullptr;
            float distMem = FLT_MAX;
            for (auto&& otherFighter : fighterOrder_)
            {
                if(otherFighter.isEnemy != fighter.isEnemy)
                {
                    float dist = foundDistWithOtherFighter(fighter, otherFighter);
                    if(dist < distMem)
                    {
                        distMem = dist;
                        rst = otherFighter.fighter;
                    }
                }
            }
            return rst;
        }

        /**
         * @brief Found the distance with other fighter
         * 
         * @param fighter1 
         * @param fighter2 
         * @return float 
         */
        float foundDistWithOtherFighter (S_Fighter& fighter1, S_Fighter& fighter2)
        {
            Magnum::Vector2 dist(   fighter1.fighter->position_.x() - fighter2.fighter->position_.x(),
                                    fighter1.fighter->position_.z() - fighter2.fighter->position_.z());
            
            return dist.length();
        }

    private:
};

#endif 
