//Project : 3c
//Editing by Six Jonathan
//Date : 10/02/2020 - 13 h 33

#ifndef _CITY_H
#define _CITY_H

/*Magnum dependence*/
#include <Magnum/Platform/Sdl2Application.h>

/*STD dependence*/
#include <vector>
#include <list>
#include <memory>

/*Project file dependence*/
#include "../Enemy.hpp"
#include "../Pnj.hpp"
#include "../type.hpp"
#include "../Chest.hpp"
#include "Zone.hpp"
#include "World.hpp"
#include "House.hpp"
#include "Arena.hpp"


struct S_Entrance
{
    Zone    zone;
    std::unique_ptr<World>  world;
};

enum class PlayerContextInCity
{
    InCity,
    InHouse1,
    InArena1
};

class City
    : public World
{
    public:

        City ()							= delete;
        City (const City& other)		= default;
        City (City&& other)				= default;
        virtual ~City ()				= default;

        City (GameResourceManager& ressources, Player& player, Object3D& cameraObject, Magnum::SceneGraph::Camera3D& camera, Pi::PlayerInterface& playerInterface);

        void execut             (float deltaTime) final;
        void draw               () final;
        void keyPressEvent		(Magnum::Platform::Application::KeyEvent& event) final;
        void keyReleaseEvent	(Magnum::Platform::Application::KeyEvent& event) final;
        void mouseMoveEvent     (Magnum::Platform::Application::MouseMoveEvent& event) final;
        void mousePressEvent	(Magnum::Platform::Application::MouseEvent& event) final;
        void mouseReleaseEvent	(Magnum::Platform::Application::MouseEvent& event) final;

    protected:

        void settingPlayer();
        void initializeMap(GameResourceManager& ressources);
        void addRandomEnemyInZone(const ZoneCamera& zone, GameResourceManager& ressources, float min = 2.f, float max = 10.f);
        void initializePNJ(GameResourceManager& ressources);
        void initializeCamerasAndZonesWithEnemy(GameResourceManager& ressources);
        void initialiseEntanceTowardWorld(GameResourceManager& ressources);
        void initializeObject(GameResourceManager& ressources);


        std::unique_ptr<Object3D>                   pMap_;

        std::vector<Zone>                           entrancesTowardWorld_;
        ZoneCamera                                  camZone_;
        std::list<Enemy>                          enemy_;
        std::vector<Pnj>                            pnjs_;

        PlayerContextInCity                         context_; //Where the player is inside the city
        S_Entrance                                  houseEntrance_; //internal world like houses, arena...
        std::unique_ptr<Arena>                      arena_;
		Pi::PlayerInterface&						playerInterface_;	
		GameResourceManager& 						rs_;
        bool interact;
        Chest chest;

    private:

};

#endif // _CITY_H
