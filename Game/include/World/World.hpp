//Project : Game
//Editing by Six Jonathan
//Date : 05/02/2020 - 11 h 34

#ifndef _WORLD_H
#define _WORLD_H

/*Magnum dependence*/
#include <Magnum/Platform/Sdl2Application.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/SceneGraph/Drawable.h>

/*STD dependence*/
#include <vector>
#include <string>
#include <iostream>

/*Project file dependence*/
#include "../Pi/PlayerInterface.hpp"
#include "../Player.hpp"
#include "../type.hpp"

class World
{
    public:

        World ()						= delete;
        World (const World& other)		= default;
        World (World&& other)			= default;
        virtual ~World ()				= default;

        World (Player& player, Object3D& cameraObject, Magnum::SceneGraph::Camera3D& camera, 
				Pi::PlayerInterface& playerInterface, GameResourceManager& rs)
            :   player_         {player},
                cameraObject_   {cameraObject},
                camera_         {camera},
                scene_          {},
                world_          {},
                drawables_      {},
				playerInterface_{playerInterface},
				rs_				{rs}
        {
            entranceInto();
        }

        virtual void execut (float deltaTime) = 0;

        virtual void draw()
        {
            camera_.draw(player_.drawables[player_.stepAnim_]);
            camera_.draw(drawables_);
        }

        /**
         * @brief Use third person camera
         * 
         * @param characterPos  : player that use the thrid person
         * @param pointToSee    : point that the player is focus
         * @param angle         : Angle in degres that define the camera position on plan XZ
         * @param camDepth      : depth of the camera. More is hight, more user can see the back of the player
         */
        void useThirdPersonnCameraToSeePoint(const Magnum::Vector3& characterPos, const Magnum::Vector3& pointToSee, float angle = 170.f, float camDepth = 2.5f, float camHeigth = 0.f)
        {
            Magnum::Vector3 vectorFromPlayerToPnj(pointToSee - characterPos);
            vectorFromPlayerToPnj.y() = camHeigth;
            float exDirX = vectorFromPlayerToPnj.x();
            float camAngleRad = angle * static_cast<float>(M_PI) / 180.f;
            float cosR = cosf(camAngleRad);
            float sinR = sinf(camAngleRad);

            vectorFromPlayerToPnj.x() = exDirX * cosR - vectorFromPlayerToPnj.z() * sinR;
            vectorFromPlayerToPnj.z() = exDirX * sinR + vectorFromPlayerToPnj.z() * cosR;

            Magnum::Vector3 vectorFromPlayerToCam ((vectorFromPlayerToPnj + vectorFromPlayerToPnj.normalized() * camDepth) + pointToSee);
            vectorFromPlayerToCam.y() += 2.5f; //TODO: add player heigth
            cameraObject_.setTransformation(Magnum::Matrix4::lookAt(vectorFromPlayerToCam, pointToSee + Magnum::Vector3::yAxis(1.f), {0.f, 1.f, 0.f}));
        }

        /**
         * @brief settup camera and player to entrance inside this world
         * 
         */
        virtual void entranceInto       () 
        {   
            cameraObject_.setParent(&scene_);
            player_.setParent(scene_);
            player_.resetAction();
        }

        virtual void keyPressEvent		(Magnum::Platform::Application::KeyEvent& event) { (void)event; };
        virtual void keyReleaseEvent	(Magnum::Platform::Application::KeyEvent& event) {(void)event;};
        virtual void mousePressEvent	(Magnum::Platform::Application::MouseEvent& event) {(void)event;};
        virtual void mouseReleaseEvent	(Magnum::Platform::Application::MouseEvent& event) {(void)event;};
        virtual void mouseMoveEvent		(Magnum::Platform::Application::MouseMoveEvent& event) {(void)event;};
        virtual void mouseScrollEvent	(Magnum::Platform::Application::MouseScrollEvent& event) {(void)event;};

    protected:

        Player&                             player_;
        Object3D&                           cameraObject_;
        Magnum::SceneGraph::Camera3D&       camera_;
		Scene3D						        scene_;
		Object3D                            world_;
		Magnum::SceneGraph::DrawableGroup3D drawables_;
		Pi::PlayerInterface&				playerInterface_;	
		GameResourceManager& 				rs_;

    private:

};

#endif // _WORLD_H
