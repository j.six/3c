//Project : 3c
//Editing by Six Jonathan
//Date : 10/02/2020 - 13 h 33

#ifndef _HOUSE_H
#define _HOUSE_H

/*Magnum dependence*/
#include <Magnum/Platform/Sdl2Application.h>

/*STD dependence*/
#include <vector>
#include <memory>

/*Project file dependence*/
#include "../Enemy.hpp"
#include "../Pnj.hpp"
#include "../type.hpp"
#include "../Camera.hpp"
#include "Zone.hpp"
#include "World.hpp"

class House
    : public World
{
    public:

        House ()						= delete;
        House (const House& other)		= default;
        House (House&& other)			= default;
        virtual ~House ()				= default;

        House (GameResourceManager& ressources, Player& player, Object3D& cameraObject, Magnum::SceneGraph::Camera3D& camera, 
				Pi::PlayerInterface& playerInterface);

        void entranceInto       () final; 
        void execut             (float deltaTime) final;
        void draw               () final;
        void keyPressEvent		(Magnum::Platform::Application::KeyEvent& event) final;
        void keyReleaseEvent	(Magnum::Platform::Application::KeyEvent& event) final;
        void mouseMoveEvent     (Magnum::Platform::Application::MouseMoveEvent& event) final;

        /**
         * @brief True if player walk front of the door to exit
         * 
         * @return true 
         * @return false 
         */
        bool playerExit() { return exitZone_.pointIsInsideZone(player_.position);}


    protected:

        void settingPlayer();
        void initializeMap(GameResourceManager& ressources);
        void initializePNJ(GameResourceManager& ressources);

        std::unique_ptr<Object3D>                   pMap_;
        Zone                                        exitZone_;
        std::vector<Pnj>                            pnjs_;
        Camera                                      cam_;
        Zone                                        houseLimit_;

    private:

};

#endif // _HOUSE_H
