//Project : 3c
//Editing by Six Jonathan
//Date : 11/02/2020 - 08 h 54

#ifndef _ZONE_H
#define _ZONE_H

/*Magnum dependence*/
#include <Magnum/Math/Vector3.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Math/Functions.h>

/*STL dependence*/
#include <list>

/*Project file dependence*/
#include "../type.hpp"

/**
 * @brief Zone create limite on map. It's 3D box. If point is insid the the, something can happend. 
 *          i.e : Camera chang toward other camera zone
 * 
 */
class Zone
{
    public:

        Zone ()							= delete;
        Zone (const Zone& other)		= default;
        Zone (Zone&& other)				= default;
        virtual ~Zone ()				= default;

        Zone (const Magnum::Math::Vector3<float>& pt1, const Magnum::Math::Vector3<float>& size)
            :   pt1_    {pt1},
                size_   {size}
        {}


        /**
         * @brief Check if point is inside the zone
         * 
         * @param pt 
         * @return true 
         * @return false 
         */
        bool pointIsInsideZone(const Magnum::Math::Vector3<float>& pt) const
        {
            return  isBetween(pt.x(), pt1_.x(), pt1_.x() + size_.x()) &&
                    isBetween(pt.y(), pt1_.y(), pt1_.y() + size_.y()) &&
                    isBetween(pt.z(), pt1_.z(), pt1_.z() + size_.z());
        }

        void keepPointInsideZone(Magnum::Math::Vector3<float>& pt) const
        {
            float pt1X = pt1_.x();
            float pt2X = pt1_.x() + size_.x();
            float pt1Y = pt1_.y();
            float pt2Y = pt1_.y() + size_.y();
            float pt1Z = pt1_.z();
            float pt2Z = pt1_.z() + size_.z();

            if(pt1X > pt2X)
                std::swap(pt1X, pt2X);
            if(pt1Y > pt2Y)
                std::swap(pt1Y, pt2Y);
            if(pt1Z > pt2Z)
                std::swap(pt1Z, pt2Z);

            if      (pt.x() < pt1X) pt.x() = pt1X; 
            else if (pt.x() > pt2X) pt.x() = pt2X;
            if      (pt.y() < pt1Y) pt.y() = pt1Y; 
            else if (pt.y() > pt2Y) pt.y() = pt2Y;
            if      (pt.z() < pt1Z) pt.z() = pt1Z; 
            else if (pt.z() > pt2Z) pt.z() = pt2Z;
        }

        const Magnum::Math::Vector3<float>& getPt1  () const { return pt1_;}
        const Magnum::Math::Vector3<float>& getSize () const { return size_;}
        Magnum::Math::Vector3<float>        getPt2  () const { return pt1_ + size_;}


    protected:

        Magnum::Math::Vector3<float> pt1_;      //Point on border (the lower Left bottom point)
        Magnum::Math::Vector3<float> size_;     //The vector from pt1_ to the opposite point

        bool isBetween(float val, float min, float max) const
        {
            if(min > max)
                std::swap(min, max);
            
            return val >= min && val <= max; 
        }

    private:

};

class ZoneCamera
    : public Zone
{
    public:

        ZoneCamera ()							= delete;
        ZoneCamera (const ZoneCamera& other)	= default;
        ZoneCamera (ZoneCamera&& other)			= default;
        virtual ~ZoneCamera ()				    = default;

        ZoneCamera (const Magnum::Math::Vector3<float>& pt1,
                    const Magnum::Math::Vector3<float>& size,
                    const Magnum::Math::Vector3<float>& camEye,
                    const Magnum::Math::Vector3<float>& camTarget,
                    const Magnum::Math::Vector3<float>& camUp,
                    float level, float interpolationSpeed,
                    Magnum::Math::Vector3<float>* playerPos = nullptr)
            :   Zone                    (pt1, size),
                camEye_                 {camEye},
                camTarget_              {camTarget},
                camUp_                  {camUp},
                playerPos_              {playerPos},
                interpolationSpeed_     {interpolationSpeed},
                level_                  {level}            
        {}

        /**
         * @brief return reference to the current camera zone inside the player is
         * 
         * @param playerPos 
         * @return ZoneCamera& 
         */
        ZoneCamera& checkInWitchZonePlayerIs(const Magnum::Math::Vector3<float>& playerPos)
        {
            for (auto &&child : child_)
            {
                if (child.pointIsInsideZone(playerPos))
                {
                    return child.checkInWitchZonePlayerIs(playerPos);
                }
            }
            return *this;
        }

        /**
         * @brief Add child zone
         * 
         * @param pt1 
         * @param size 
         * @param camEye 
         * @param camTarget 
         * @param camUp 
         * @param interpolationSpeed 
         * @param playerPos 
         * @return ZoneCamera& 
         */
        ZoneCamera& addZone(const Magnum::Math::Vector3<float>& pt1,
                            const Magnum::Math::Vector3<float>& size,
                            const Magnum::Math::Vector3<float>& camEye,
                            const Magnum::Math::Vector3<float>& camTarget,
                            const Magnum::Math::Vector3<float>& camUp,
                            float level, float interpolationSpeed,
                            Magnum::Math::Vector3<float>* playerPos = nullptr)
        {
            child_.emplace_back(pt1, size, camEye, camTarget, camUp, level, interpolationSpeed, playerPos);
            return child_.back();
        }

        float getInterpolationSpeed() const { return interpolationSpeed_;}

        /**
         * @brief Get the Look At Matrix object of this zone
         * 
         * @return Magnum::Math::Matrix4<float> 
         */
        Magnum::Math::Matrix4<float> getLookAtMatrix () const
        { 
            return Magnum::Math::Matrix4<float>::lookAt(camEye_, playerPos_ == nullptr ? camTarget_ : (*playerPos_ +  Magnum::Math::Vector3<float>::yAxis(2.0f)), camUp_);
        }

        const std::list<ZoneCamera>& getChild () { return child_;}
        float getLevel() const { return level_;}

        void setTargetPosition (const Magnum::Math::Vector3<float>& camTarget) { camTarget_ = camTarget;}

    protected:

        /**
         * @brief   Do learp with each value of matrix.
         *          Computes a+t*(b−a),
         *          i.e. the linear interpolation between a and b for the parameter t (or extrapolation, when t is outside the range [0,1])
         * 
         * @param a 
         * @param b 
         * @param t : is outside the range [0,1]
         * @return Magnum::Math::Matrix4<float> 
         */
        Magnum::Math::Matrix4<float> lerp (const Magnum::Math::Matrix4<float>& a, const Magnum::Math::Matrix4<float>& b, float t)
        {
            Magnum::Math::Matrix4<float> rst;

            for (size_t i = 0; i < 4; i++)
            {
                for (size_t j = 0; j < 4; j++)
                {
                    rst[i][j] = Magnum::Math::lerp<float>(a[i][j], b[i][j], t);
                }
            }
            return rst;
        }

        /*Attribut*/
        Magnum::Math::Vector3<float>    camEye_,
                                        camTarget_,
                                        camUp_;

        Magnum::Math::Vector3<float>*   playerPos_; //if nullptr, cam don't follow the player 

        float                           interpolationSpeed_;
        float                           level_; //height of the plan in zone
        std::list<ZoneCamera>           child_;

    private:

};

#endif // _ZONE_H