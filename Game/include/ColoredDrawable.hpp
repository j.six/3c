//Project : 3c
//Editing by Six Jonathan
//Date : 06/02/2020 - 12 h 57

#ifndef _COLORED_DRAWABLE_H
#define _COLORED_DRAWABLE_H

/*Magnum dependence*/
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/Camera.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Shaders/Phong.h>
#include <Magnum/GL/Mesh.h>
#include <Corrade/Utility/Debug.h>

/*STD dependence*/
/*Project file dependence*/
#include "../include/type.hpp"


class ColoredDrawable
    : public Magnum::SceneGraph::Drawable3D
{
    public:
        explicit ColoredDrawable(Object3D& object, Magnum::Shaders::Phong& shader, Magnum::GL::Mesh& mesh, const Magnum::Color4& color, Magnum::SceneGraph::DrawableGroup3D& group)
            :   Magnum::SceneGraph::Drawable3D{object, &group},
                shader_(shader),
                mesh_(mesh),
                color_{color}
        {}

    private:
        void draw(const Magnum::Matrix4& transformationMatrix, Magnum::SceneGraph::Camera3D& camera) override
        {
            shader_
                .setDiffuseColor(color_)
                .setLightPosition(camera.cameraMatrix().transformPoint({-3.0f, 10.0f, 10.0f}))
                .setTransformationMatrix(transformationMatrix)
                .setNormalMatrix(transformationMatrix.normalMatrix())
                .setProjectionMatrix(camera.projectionMatrix());

            mesh_.draw(shader_);
        }

        Magnum::Shaders::Phong& shader_;
        Magnum::GL::Mesh&       mesh_;
        Magnum::Color4          color_;
};

#endif // _COLORED_DRAWABLE_H