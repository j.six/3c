#ifndef _TOOLS_H
#define _TOOLS_H

#include <math.h>
#include <iomanip>
#include <sstream>
#include <string>

int ranValueLimite(int min, int max);
bool ranPercentProba(int percent);

template<typename T = float, T Min, T Max>
auto isBetween(T value);

std::string to_strPrecision (const float & numb);

#endif //_TOOLS_H
