#include "../include/World/City.hpp"
#include "../include/modelLoader.hpp"
#include <Magnum/GL/Renderer.h>
#include <Magnum/Math/Angle.h>
#include "../include/tools.hpp"
#include "../include/Fighter.hpp"

using namespace Magnum;
using namespace Math::Literals;
using namespace Magnum::Platform;

City::City (GameResourceManager& ressources, Player& player, Object3D& cameraObject, Magnum::SceneGraph::Camera3D& camera, 
			Pi::PlayerInterface& playerInterface)
    :   World           (player, cameraObject, camera, playerInterface , ressources),
        pMap_           {nullptr},
		camZone_		{	{-100, -100, -100},
							{200, 200, 200}, 
							{0.f, 70.f, 50.f},
							{1.f, 1.f, 1.f},
							{0.f, 1.f, 0.f}, 0.f, 1.f},
		enemy_          {},
		pnjs_			{},
		context_		{PlayerContextInCity::InCity},
		houseEntrance_	{Zone{{0, 0, -2}, {3, 10, -4}}, std::make_unique<House>(ressources, player, cameraObject, camera,playerInterface)},
		arena_			{std::make_unique<Arena>(ressources, player, cameraObject, camera,playerInterface)},
		playerInterface_{playerInterface},
		rs_				{ressources}
{
    settingPlayer();
    initializeMap(ressources);
	initializeCamerasAndZonesWithEnemy(ressources);
	initializePNJ(ressources);
	initializeObject(ressources);

	entranceInto();
}

void City::settingPlayer()
{
    player_.position = {1.f, 2.3f, 18.5f};

	for (auto &&model : player_.models)
	{
		model-> resetTransformation()
						.rotateY     (180.0_degf)
						.scale       (player_.scale_)
						.translate   (player_.position);
	}

	player_.resetOrientation();
    player_.rotateY(M_PI);
}

void City::initializeMap(GameResourceManager& ressources)
{
    /*Initialize map model*/
    pMap_ = std::move(ModelLoader::loadModel("Game/resources/meshes/cityLevel.obj", ressources, drawables_, &scene_));
    pMap_   ->rotateX(180.0_degf)
            .scale({0.01f,0.01f,0.01f})
            .translate({0.f, 0.f, 0.f});


    /*Initialize map light*/

    /*Set up map camera*/
}

void City::addRandomEnemyInZone(const ZoneCamera& zone, GameResourceManager& ressources, float min, float max)
{
	/*get random iteration*/
	unsigned int numberTest = ranValueLimite(min, max);
	for (unsigned int j = 0; j < numberTest; j++)
	{
		/*check what kind of monster will spawn*/
		int enemyType = ranValueLimite(0, 1); 
		
		if(enemyType == 0)
		{
			enemy_.emplace_back(zone.getPt1() + Vector3{ranValueLimite(0, zone.getSize().x() * 10) / -10.f, zone.getLevel() + 1.f, ranValueLimite(0, zone.getSize().z() * 10) / 10.f});
			enemy_.back().model = std::move(ModelLoader::loadModel("Game/resources/meshes/shadow.obj", ressources, drawables_, &scene_));
			enemy_.back().model
						->rotateY(Magnum::Rad(ranValueLimite(0, 314) / 100.f))
						.scale({0.6f,0.6f,0.6f})
						.translate(enemy_.back().position_);

			enemy_.back().addMonster(E_FighterType::Assassin, 30,20,10,15,"Game/resources/meshes/riku.obj", "Sbire1", ressources, &scene_);	
			enemy_.back().addMonster(E_FighterType::Mage, 30,20,10,10,"Game/resources/meshes/shadow.obj", "Sbire2", ressources, &scene_);
			enemy_.back().addMonster(E_FighterType::Warrior, 30,20,10,4,"Game/resources/meshes/shadow.obj","Dark-Sora", ressources, &scene_);
		}
		else if(enemyType == 1)
		{
			enemy_.emplace_back(zone.getPt1() + Vector3{ranValueLimite(0, zone.getSize().x() * 10) / -10.f, zone.getLevel() + 1.f, ranValueLimite(0, zone.getSize().z() * 10) / 10.f});
			enemy_.back().model = std::move(ModelLoader::loadModel("Game/resources/meshes/riku.obj", ressources, drawables_, &scene_));
			enemy_.back().model
						->rotateY(Magnum::Rad(ranValueLimite(0, 314) / 100.f))
						.scale({0.7f,0.7f,0.7f})
						.translate(enemy_.back().position_);

			enemy_.back().addMonster(E_FighterType::Assassin, 30,20,10,6,"Game/resources/meshes/riku.obj","Sbire1", ressources, &scene_);
			enemy_.back().addMonster(E_FighterType::Mage, 30,20,10,20,"Game/resources/meshes/riku.obj","Sbire2", ressources, &scene_);
			enemy_.back().addMonster(E_FighterType::Warrior, 30,20,10,5,"Game/resources/meshes/shadow.obj","Dark-Sora", ressources, &scene_);
		}
	}

}
void City::initializeObject(GameResourceManager& ressources)
{
	chest.modelchest_ = std::move(ModelLoader::loadModel("Game/resources/meshes/chest.obj", ressources, drawables_, &scene_));
	chest.modelchest_->rotateY(static_cast<Rad>(M_PI_2))
					.scale({0.5f,0.5f,0.5f})
					.translate(chest.position);
}

void City::initializePNJ(GameResourceManager& ressources)
{
	/*Grand ma in taverne*/
	pnjs_.emplace_back(	"Kairis",
						Vector3{-19.f, 0.f, 19.f},
						Vector3{0.f, 90.f, 0.f},
						Vector3{0.4f,0.4f,0.4f},
						"Game/resources/meshes/waitgrandma.obj", ressources, &scene_);
						
	pnjs_.back().addDialog("Hey traveler, do you want a glass of beer ?");
	pnjs_.back().addDialog("No ?! My bear is so bad ? Can you help me ?\nLots of poeple don't like my beer and I loose my customer...");
	pnjs_.back().addDialog("Can you go see Leon, my beer artisan to know what is the problem ?\nHe live in house with pannel accessory in front of it.");
	pnjs_.back().addDialog("Thank you traveler, I hope to see you soon !");
}

void City::execut             (float deltaTime)
{
	if(context_ == PlayerContextInCity::InCity)
	{
		ZoneCamera& camPrevious = camZone_.checkInWitchZonePlayerIs(player_.position);
    	player_.update(deltaTime);
		ZoneCamera& cam = camZone_.checkInWitchZonePlayerIs(player_.position);

		if(&cam == &camZone_)
		{
			camPrevious.keepPointInsideZone(player_.position);
			cameraObject_.setTransformation(camPrevious.getLookAtMatrix());
			player_.position.y() = camPrevious.getLevel();	
		}
		else
		{
			cameraObject_.setTransformation(cam.getLookAtMatrix());
			player_.position.y() = cam.getLevel();			
		}

		//check chest
		chest.player_in_zone(player_,interact,rs_,playerInterface_);
		interact = false;
		
		for (auto &&model : player_.models)
		{
			model-> resetTransformation()
				.rotateY     (Magnum::Rad(player_.getRotRad()))
				.scale       (player_.scale_)
				.translate   (player_.position);
		}

		for (auto &&pnj : pnjs_)
		{
			if (pnj.playerHitPnj(player_.position))
			{
				if (!playerInterface_.has(pnj.getName()))
				{
					playerInterface_.PnjTextBox(&rs_,pnj.getName(),pnj.getDialogues());
				}

				pnj.orientTowardPoint(player_.position);
				/*third person camera*/
				Magnum::Vector3 vectorFromPlayerToPnj(pnj.getPosition() - player_.position);
				vectorFromPlayerToPnj.y() = 0.f;
				float exDirX = vectorFromPlayerToPnj.x();
				static float angle = 170.f;
				float camAngleRad = angle * static_cast<float>(M_PI) / 180.f;
				float cosR = cosf(camAngleRad);
				float sinR = sinf(camAngleRad);

				vectorFromPlayerToPnj.x() = exDirX * cosR - vectorFromPlayerToPnj.z() * sinR;
				vectorFromPlayerToPnj.z() = exDirX * sinR + vectorFromPlayerToPnj.z() * cosR;

				Magnum::Vector3 vectorFromPlayerToCam ((vectorFromPlayerToPnj.normalized() * 3.f) + pnj.getPosition());
				vectorFromPlayerToCam.y() += 3.f;
				cameraObject_.setTransformation(Magnum::Matrix4::lookAt(vectorFromPlayerToCam, pnj.getPosition() + Magnum::Vector3::yAxis(1.f), {0.f, 1.f, 0.f}));

				break; /*player can only interract with one pnj at time*/
			}
			if (playerInterface_.has(pnj.getName()))
			{
				playerInterface_.get(pnj.getName()).close();
				if (playerInterface_.closed(pnj.getName()))
					playerInterface_.stop(pnj.getName());
			}
		}

		for( auto&& enem : enemy_) 
   		{
        	if(enem.inFight_ == false)
        	{
            	enem.engage_fight(&player_);
        
            	if( enem.inFight_ == true)
            	{
					context_ = PlayerContextInCity::InArena1;
					arena_->entranceInto(enem);
				}
			}
		}
		
		if (houseEntrance_.zone.pointIsInsideZone(player_.position))
		{
			context_ = PlayerContextInCity::InHouse1;
			houseEntrance_.world->entranceInto();
		}
	}
	else if (context_ == PlayerContextInCity::InHouse1)
	{
		houseEntrance_.world->execut(deltaTime);

		if(static_cast<House*>(houseEntrance_.world.get())->playerExit())
		{
			context_ = PlayerContextInCity::InCity;
			entranceInto();

			player_.position = {0.f, 3.f, 0.f};	
			for (auto &&model : player_.models)
			{
				model-> resetTransformation()
				.rotateY     (0.0_degf)
				.scale       (player_.scale_) 
				.translate   (player_.position);
			}
		}
	}
	else if (context_ == PlayerContextInCity::InArena1)
	{
		arena_->execut(deltaTime);

		bool playerWin;
		if(arena_->battleIsEnd(playerWin))
		{
			context_ = PlayerContextInCity::InCity;
			entranceInto();

			/*if player win destroy enemy*/
			if(playerWin)
			{
				for(auto it = enemy_.begin(); it != enemy_.end(); it++)
				{
					if(&(*it) == arena_->getEnemy())
					{
						enemy_.erase(it);
						break;
					}
				}
			}
			else /*spawn player toward home and continu to use enemy*/
			{
				arena_->getEnemy()->setParent(scene_);

				player_.position = {0.f, 3.f, 0.f};	
				for (auto &&model : player_.models)
				{
					model-> resetTransformation()
					.rotateY     (0.0_degf)
					.scale       (player_.scale_)
					.translate   (player_.position);
				}
			}
			arena_->resetEnemy();
		}
	}
}

void City::draw()
{
	switch (context_)
	{
	case PlayerContextInCity::InCity :
		for (auto &&pnj : pnjs_)
		{
			pnj.draw(camera_);
		}

		World::draw();
		break;

	case PlayerContextInCity::InHouse1 :
		houseEntrance_.world->draw();
		break;

	case PlayerContextInCity::InArena1 :
		GL::Renderer::disable(GL::Renderer::Feature::FaceCulling);
		arena_->draw();
		break;
	
	default:
		break;
	}
}

void City::keyPressEvent		(Magnum::Platform::Application::KeyEvent& event)
{
	switch (context_)
	{
	case PlayerContextInCity::InCity :
		switch (event.key())
		{
			case Application::KeyEvent::Key::E :
				interact = true;
				break;
			case Application::KeyEvent::Key::W :
				player_.action.moveForward  = true;
				cameraObject_.translate(Vector3::zAxis(0.1f));
				break;
			case Application::KeyEvent::Key::S :
				player_.action.moveBackward = true;
				cameraObject_.translate(Vector3::zAxis(-0.1f));
				break;
			case Application::KeyEvent::Key::A :
				player_.action.turnLeft     = true;
				break;
			case Application::KeyEvent::Key::D :
				player_.action.turnRight    = true;
			break;
			default:
			break;
		}
		break;

	case PlayerContextInCity::InHouse1 :
		houseEntrance_.world->keyPressEvent(event);
		break;

	case PlayerContextInCity::InArena1 :
		arena_->keyPressEvent(event);
		break;
	
	default:
		break;
	}
}

void City::keyReleaseEvent	(Magnum::Platform::Application::KeyEvent& event)
{
	switch (context_)
	{
	case PlayerContextInCity::InCity :
		switch (event.key())
		{
			case Application::KeyEvent::Key::W :
				player_.action.moveForward  = false;
			break;
			case Application::KeyEvent::Key::S :
				player_.action.moveBackward = false;
			break;
			case Application::KeyEvent::Key::A :
				player_.action.turnLeft     = false;
			break;
			case Application::KeyEvent::Key::D :
				player_.action.turnRight    = false;
			break;
			default:
			break;
		}
		break;

	case PlayerContextInCity::InHouse1 :
		houseEntrance_.world->keyReleaseEvent(event);
		break;

	case PlayerContextInCity::InArena1 :
		arena_->keyReleaseEvent(event);
		break;
	
	default:
		break;
	}
}

void City::mousePressEvent	(Magnum::Platform::Application::MouseEvent& event)
{
	switch (context_)
	{
	case PlayerContextInCity::InArena1 :
		arena_->mousePressEvent(event);
		break;
	
	default:
		break;
	}
}

void City::mouseReleaseEvent	(Magnum::Platform::Application::MouseEvent& event)
{
	switch (context_)
	{
	case PlayerContextInCity::InArena1 :
		arena_->mouseReleaseEvent(event);
		break;

	default:
		break;
	}
}


void City::mouseMoveEvent     (Magnum::Platform::Application::MouseMoveEvent& event)
{
	switch (context_)
	{
	case PlayerContextInCity::InCity :
		break;

	case PlayerContextInCity::InHouse1 :
		houseEntrance_.world->mouseMoveEvent(event);

	case PlayerContextInCity::InArena1 :
		arena_->mouseMoveEvent(event);
		break;
	
	default:
		break;
	}
}

void City::initializeCamerasAndZonesWithEnemy(GameResourceManager& ressources)
{
	/*Place front of the accessory shoop*/
	camZone_.addZone(	{-6.5f, -1.f, 4.f}, 
						{21.5f, 10.f, -6.f}, 
						{0.5f, 5.f, 6.5f},
						{0.f, 0.f, 0.f},
						{0.f, 1.f, 0.f}, 2.5f, 1.f, &player_.position);

		/*allay left to the accessory shop*/
		camZone_.addZone(	{-6.5f, -1.f, -2.f}, 
							{4.f, 10.f, -10.5f}, 
							{-5.f, 10.f, -11.f},
							{0.f, 0.f, 0.f},
							{0.f, 1.f, 0.f}, 2.5f, 1.f, &player_.position);

			/*corner left of this allay*/
			ZoneCamera& darkAlley = camZone_.addZone(	{-6.5f, -1.f, -12.5f}, 
														{-18, 10.f, 6}, 
														{-5.f, 20.f, -10.f},
														{0.f, 0.f, 0.f},
														{0.f, 1.f, 0.f}, 2.5f, 1.f, &player_.position);
			addRandomEnemyInZone(darkAlley, ressources);

	/*main place of the city*/
	camZone_.addZone(	{14.5f, -1.f, 9.f}, 
						{-26, 10.f, 15},
						{14.5f, 10.f, 4.7f},
						{0.f, 0.f, 0.f},
						{0.f, 1.f, 0.f}, 0.f, 1.f, &player_.position);

	/*staircase layout 1*/
	camZone_.addZone(	{-2.3f, -1.f, 9.f},
						{5.f, 10.f, -2.6},
						{0.f, 30.f, 0.f},
						{0.f, 0.f, 0.f},
						{0.f, 1.f, 0.f}, 0.7f, 1.f, &player_.position);

	/*staircase layout 2*/
	camZone_.addZone(	{-2.3f, -1.f, 7.5f}, 
						{5.f, 10.f, -3.5},
						{0.f, 30.f, 0.f},
						{0.f, 0.f, 0.f},
						{0.f, 1.f, 0.f}, 1.4f, 1.f, &player_.position);

	/*Tavern*/
	camZone_.addZone(	{-22.4f, -1.f, 20.f}, 
						{11, 10.f, -10},
						{-1.8f, 4.f, 9.f},
						{-14.f, 1.2f, 15.f},
						{0.f, 1.f, 0.f}, 0.f, 1.f);
}