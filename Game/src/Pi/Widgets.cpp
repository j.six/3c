#include <Magnum/Trade/AbstractImporter.h>
#include <Magnum/Trade/ImageData.h>
#include <Magnum/ImageView.h>
#include <Magnum/GL/Texture.h>
#include <Magnum/GL/TextureFormat.h>
#include <Corrade/Containers/ArrayView.h>
#include <Corrade/Containers/Optional.h>
#include <Corrade/Containers/Reference.h>
#include <Corrade/PluginManager/Manager.h>
#include <Magnum/GL/Renderer.h>

#include <iostream>

#include <imgui.h>
#include <imgui_internal.h>

#include "../../include/Pi/Widgets.hpp"


using namespace Pi;

#define ImGuiWindowFlags_No ImGuiWindowFlags_NoBackground|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoScrollbar|ImGuiWindowFlags_NoScrollWithMouse|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoSavedSettings|ImGuiWindowFlags_NoMove

//========================= CONSTRUCTORS ===================================
Widgets::Widgets():
		name_		{"Main"},
		texture_	{nullptr},	
		animSpeed_	{1.0f},
		anim_		{1.0f},
		pos_		{ImVec2(0,0)},
		size_		{ImVec2(0,0)},
		uv0_		{ImVec2(0,0)},
		uv1_		{ImVec2(1,1)},
		color_		{ImVec4(0,0,0,0)},
		context_	{Widgets::Context::DISP},
		children_	{}
{}


Widgets::Widgets	(const WidgetsArgs& Args):
	name_		{Args.name},
	texture_	{nullptr},	
	animSpeed_	{Args.animSpeed},
	anim_		{1.0f},
	pos_		{Args.pos},
	size_		{static_cast<float>(Args.windowSize.x()),static_cast<float>(Args.windowSize.y())},
	uv0_		{Args.uv0},
	uv1_		{Args.uv1},
	color_		{Args.color},
	context_	{Args.flags},
	children_	{}
{

	/* no texture is needed */
	if (!(Args.texPath == "") && Args.rs != nullptr )
	{
		/* taking the ressource in the RessourceManager */
		Resource<Texture> tex{Args.rs->get<Texture>(Args.texPath)};
		
		/* if there's none loading into the RessourceManager*/
		if (!tex)
		{
			GL::Texture2D data 		= std::move(loadTexture(Args.texPath));	
			Args.rs->set<Texture>	(tex.key(),std::move(data));
			texture_ 				=  &(*(*Args.rs->get<Texture>(tex.key())));
		}

		else if (tex)
		{
			/* else taking it in*/
			texture_ = &(*(*tex));

		}
	}
}

Widgets::Widgets	(const Widgets& copyWidgets):
	name_		{copyWidgets.name_},
	texture_	{copyWidgets.texture_},
	animSpeed_	{copyWidgets.animSpeed_},
	anim_		{1.0f},
	pos_		{copyWidgets.pos_},
	size_		{copyWidgets.size_},
	uv0_		{copyWidgets.uv0_},
	uv1_		{copyWidgets.uv1_},
	color_		{copyWidgets.color_},
	context_	{copyWidgets.context_},
	children_	{}
{
}

Widgets::Widgets	(Widgets&& movedWidgets):
	name_		{std::move(movedWidgets.name_)},
	texture_	{movedWidgets.texture_},
	animSpeed_	{movedWidgets.animSpeed_},
	anim_		{1.0f},
	pos_		{std::move(movedWidgets.pos_)},
	size_		{std::move(movedWidgets.size_)},
	uv0_		{std::move(movedWidgets.uv0_)},
	uv1_		{std::move(movedWidgets.uv1_)},
	color_		{std::move(movedWidgets.color_)},
	context_	{movedWidgets.context_},
	children_	{std::move(movedWidgets.children_)}
{
}


Widgets::~Widgets	()
{
}

//========================= 	OPERATORS 		==============================

Widgets&	Widgets::operator=	(Widgets&& movedWidgets)
{
	name_		= std::move(movedWidgets.name_);
	texture_	= movedWidgets.texture_;
	animSpeed_	= movedWidgets.animSpeed_;
	anim_		= 1.0f;
	pos_		= std::move(movedWidgets.pos_);
	size_		= std::move(movedWidgets.size_);
	uv0_		= std::move(movedWidgets.uv0_);
	uv1_		= std::move(movedWidgets.uv1_);
	color_		= std::move(movedWidgets.color_);
	context_	= movedWidgets.context_;
	children_ 	= std::move(movedWidgets.children_);

	return *this;
}


//========================= UPDATE	METHODS ===================================


void	Widgets::update	(const float& deltaTime)
{
	/* type : list<unique_ptr<Widgets>>::iterator */
	for (auto&& i = children_.begin(); i != children_.end(); i++)
	{
		/* calling for update */
		(*i)->update(deltaTime);

		/* if the context_ is OVER the Widgets should be erased*/
		if ((*i)->context_ == Context::OVER)
			i = children_.erase(i);
	}
}

//========================= DISPLAY METHODS ===================================

void 	Widgets::draw	(const ImVec2& parentPos)
{
		/* Guards */
		if (texture_ != nullptr)
		{

			/* Set Widgets position considering parent dependency */
			ImGui::SetWindowPos(name_.c_str(),{parentPos.x + pos_.x, parentPos.y + pos_.y});
			ImGui::Begin(name_.c_str(), nullptr, ImGuiWindowFlags_No);

			/* display chosen by context_ */
			context_ == Context::ANIM || context_ == Context::CLOSING ?
			ImGuiIntegration::image(*texture_,{size_.x,(size_.y * anim_)},{{uv0_.x,uv0_.y},{uv1_.x,uv1_.y}}):
  			ImGuiIntegration::image(*texture_,{size_.x,size_.y},{{uv0_.x,uv0_.y},{uv1_.x,uv1_.y}});

		   	ImGui::End();
		}
		
		/* displaying the children */
		for (std::unique_ptr<Widgets>& i : children_)
		{
			i->draw({parentPos.x + pos_.x, parentPos.y + pos_.y});
		}
}

GL::Texture2D		Widgets::loadTexture		(const std::string& texPath)
{
	/* manage loading of any kind */
	PluginManager::Manager<Trade::AbstractImporter> manager;
					
	/* loading the right importer through the manager*/
	Containers::Pointer<Trade::AbstractImporter> importer = manager.loadAndInstantiate("AnyImageImporter");
						
	/* verify the load of importer */
	if(!importer) 
		std::exit(1);
						
	/* verify hte file is here and have been opened */
	if(!importer->openFile(texPath))
		std::exit(2);
			
	/* load an image2D */
	Containers::Optional<Trade::ImageData2D> image = importer->image2D(0);
	CORRADE_INTERNAL_ASSERT(image);
		
	GL::Texture2D data;

	/*put the image in a Texture */
	data.setWrapping(GL::SamplerWrapping::ClampToEdge)
	.setMagnificationFilter(GL::SamplerFilter::Linear)
	.setMinificationFilter(GL::SamplerFilter::Linear)
	.setStorage(1, GL::textureFormat(image->format()), image->size())
	.setSubImage(0, {}, *image);
			
	/* give the texture away*/
	return data;
}
