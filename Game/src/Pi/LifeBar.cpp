#include <Magnum/Trade/AbstractImporter.h>
#include <Magnum/Trade/ImageData.h>
#include <Magnum/ImageView.h>
#include <Magnum/GL/Texture.h>
#include <Magnum/GL/TextureFormat.h>
#include <Corrade/Containers/ArrayView.h>
#include <Corrade/Containers/Optional.h>
#include <Corrade/Containers/Reference.h>
#include <Corrade/PluginManager/Manager.h>
#include <Magnum/GL/Renderer.h>
#include <cmath>

#include <iostream>


#include <imgui.h>

#define IMGUI_DEFINE_MATH_OPERATORS

#include <imgui_internal.h>


#include "../../include/Pi/LifeBar.hpp"

constexpr const char* lifePath 		= "Game/resources/textures/Life.png";
constexpr const char* damagePath 	= "Game/resources/textures/Damage.png";

namespace Pi
{
	#define ImGuiWindowFlags_No ImGuiWindowFlags_NoBackground|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoScrollbar|ImGuiWindowFlags_NoScrollWithMouse|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoSavedSettings|ImGuiWindowFlags_NoMove
	
	//========================= CONSTRUCTORS ===================================

	LifeBar::LifeBar	(float& life, const float& maxLife, const WidgetsArgs& Args, const ImVec4& textColor):
		Widgets		(Args),
		life_		{life},
		maxLife_	{maxLife},
		textColor_	{textColor}
	{
		/* no texture is needed */
		if (Args.rs != nullptr )
		{
			/* taking the ressource in the RessourceManager */
			Resource<Texture> lifeTex		{Args.rs->get<Texture>(std::string(lifePath))};
			Resource<Texture> damTex		{Args.rs->get<Texture>(std::string(damagePath))};

			/* if there's none loading into the RessourceManager*/
			if (!lifeTex && !damTex)
			{
				GL::Texture2D lifeData 		= std::move(loadTexture(lifePath));	
				GL::Texture2D damageData 	= std::move(loadTexture(damagePath));	
			
				Args.rs->set<Texture>		(lifeTex.key(),std::move(lifeData));
				Args.rs->set<Texture>		(damTex.key(),std::move(damageData));
			
				lifeTex_ 					=  &(*(*Args.rs->get<Texture>(lifeTex.key())));
				damageTex_ 					=  &(*(*Args.rs->get<Texture>(damTex.key())));
			}
			else if (lifeTex && !damTex)
			{
				GL::Texture2D damageData 	= std::move(loadTexture(damagePath));

				Args.rs->set<Texture>		(damTex.key(),std::move(damageData));

				damageTex_ 					=  &(*(*Args.rs->get<Texture>(damTex.key())));
				lifeTex_ 					=  &(*(*lifeTex));
			}
			else if (!lifeTex && damTex)
			{
				GL::Texture2D lifeData 		= std::move(loadTexture(lifePath));	

				Args.rs->set<Texture>		(lifeTex.key(),std::move(lifeData));

				lifeTex_ 					=  &(*(*Args.rs->get<Texture>(lifeTex.key())));
				damageTex_ 					=  &(*(*damTex));
			}
			else
			{
				/* else taking it in*/
				lifeTex_ 	= &(*(*lifeTex));
				damageTex_ 	= &(*(*damTex));
			}
		}
	}

	LifeBar::LifeBar(Widgets&& movedWidgets, float& life, const float& maxLife, GameResourceManager* rs, const ImVec4& textColor):
		Widgets		(std::move(movedWidgets)),
		life_		{life},
		maxLife_	{maxLife},
		textColor_	{textColor}
	{
		/* no texture is needed */
		if (rs != nullptr )
		{
			/* taking the ressource in the RessourceManager */
			Resource<Texture> lifeTex	{rs->get<Texture>(std::string(lifePath))};
			Resource<Texture> damTex	{rs->get<Texture>(std::string(damagePath))};

			/* if there's none loading into the RessourceManager*/
			if (!lifeTex && !damTex)
			{
				GL::Texture2D lifeData 		= std::move(loadTexture(lifePath));	
				GL::Texture2D damageData 	= std::move(loadTexture(damagePath));	
			
				rs->set<Texture>			(lifeTex.key(),std::move(lifeData));
				rs->set<Texture>			(damTex.key(),std::move(damageData));
			
				lifeTex_ 					=  &(*(*rs->get<Texture>(lifeTex.key())));
				damageTex_ 					=  &(*(*rs->get<Texture>(damTex.key())));
			}
			else if (lifeTex && !damTex)
			{
				GL::Texture2D damageData 	= std::move(loadTexture(damagePath));

				rs->set<Texture>			(damTex.key(),std::move(damageData));

				damageTex_ 					= &(*(*rs->get<Texture>(damTex.key())));
				lifeTex_ 					= &(*(*lifeTex));
			}
			else if (!lifeTex && damTex)
			{
				GL::Texture2D lifeData 		= std::move(loadTexture(lifePath));	

				rs->set<Texture>			(lifeTex.key(),std::move(lifeData));

				lifeTex_ 					=  &(*(*rs->get<Texture>(lifeTex.key())));
				damageTex_ 					= &(*(*damTex));
			}
			else
			{
				/* else taking it in*/
				lifeTex_ 	= &(*(*lifeTex));
				damageTex_ 	= &(*(*damTex));
			}
		}

	}

	LifeBar::LifeBar	(LifeBar&& movedLifeBar):
		Widgets		(std::move(movedLifeBar)),
		life_		{movedLifeBar.life_},
		maxLife_	{movedLifeBar.maxLife_},
		lifeTex_	{movedLifeBar.lifeTex_},
		damageTex_	{movedLifeBar.damageTex_},
		textColor_	{std::move(movedLifeBar.textColor_)}
	{
	}
	
	LifeBar::~LifeBar	()
	{
	}
		
//========================= UPDATE	METHODS ===================================
//
	void	LifeBar::update	(const float& deltaTime)
	{
		switch (context_)
		{
			case Context::ANIM:

				/* no need to update if anim_ is finished */
				if (anim_ == 0)
					break;
				
				/* making anim_ continue */
				anim_ -= deltaTime * animSpeed_;
				
				/* puting anim_ to end */
				if (0 >= anim_)
				{	
					anim_ = 0;
				}
				
				break;
			default:
				break;
		}

		/* update the children_'s Widgets */
		for (std::unique_ptr<Widgets>& i : children_)
		{
			i->update(deltaTime);
		}
	}

	//========================= DISPLAY METHODS ===================================



	void 	LifeBar::draw	(const ImVec2& parentPos)
	{

		static float oldLife = life_;
		static float newLife = life_;
		static float lifeDisplay = oldLife;
	
		/* Guards */
		if (life_ < 0)
			life_ = 0;

		switch (context_ )
		{
			case Context::ANIM:

				/* life_ has changed */
				if (life_ != oldLife)
				{
					/* the life_ is different from the one 
					we were going for*/
					if (newLife != life_)
					{
						/* the interpolation is between the new time
						and the time that we had*/
						newLife = life_;
						oldLife = lifeDisplay;
						anim_ = 1;
					}

					/* Interpolation */
					lifeDisplay = Math::Implementation::lerp(life_,oldLife,anim_);
					
					/* Stopping animation */
					if (lifeDisplay == life_)
					{
						oldLife 	= life_;
					}

					/* Get the anim_ ready for if the life_ change again*/
					reset();	
				}
			break;
			default:
				/* Just displaying the time */
				lifeDisplay = life_;
			break;
		}

		ImGui::SetWindowPos(name_.c_str(),{parentPos.x + pos_.x, parentPos.y + pos_.y});
		ImGui::Begin(name_.c_str(), nullptr, ImGuiWindowFlags_No);
		ImGui::SetWindowSize(size_);
	
		ImGui::PushStyleColor(ImGuiCol_Text,textColor_);
		ImGui::PushStyleColor(ImGuiCol_WindowBg,color_);

		/*choose display between context_*/
		context_ == Context::ANIM ?
		ImGui::lifeBar(*texture_,*lifeTex_,*damageTex_,name_.c_str(),lifeDisplay/maxLife_,
						size_,uv0_,uv1_):
		ImGui::lifeBar(*texture_,*lifeTex_,*damageTex_,name_.c_str(),life_/maxLife_,
						size_,uv0_,uv1_);
	
		
		ImGui::PopStyleColor(2);
	  	ImGui::End();

		/* display children */
		for (std::unique_ptr<Widgets>& i : children_)
		{
			i->draw({parentPos.x + pos_.x, parentPos.y + pos_.y});
		}
	}
}

//========================== INTERNAL FUNCTION ====================================

IMGUI_API void ImGui::lifeBar(
Magnum::GL::Texture2D& texId, Magnum::GL::Texture2D& lifeId, Magnum::GL::Texture2D& damageId,
const char* 	label, 
const float& 	lifeCoeff,
const ImVec2& 	imageSize, 
const ImVec2& 	uv0, const ImVec2& uv1, 
float frame_padding,
const ImVec4& bg_col, 
const ImVec4& tint_col) 
{
	/* set the widgets display on the window*/
	ImGuiWindow* window = ImGui::GetCurrentWindow();
	
	/* pass if not wanted*/
	if (window->SkipItems)
		return;
	
	/*making sure we, at least, have the place to pint the text*/
	ImVec2 size = imageSize;
	if (size.x <= 0 && size.y <= 0) 
	{	
		size.x = size.y = ImGui::GetTextLineHeightWithSpacing();
	}
	else 
	{
		if (size.x <= 0)          
			size.x = size.y;
		else if (size.y <= 0)     
			size.y = size.x;
		
		size *= window->FontWindowScale * ImGui::GetIO().FontGlobalScale;
	}
	
	/* get the context */
	ImGuiContext& 	  g 		= *GImGui;
	const ImGuiStyle& style 	= g.Style;
	/* get the text info */
	const ImGuiID id 			= window->GetID(label);
	const ImVec2  textSize 		= ImGui::CalcTextSize(label,nullptr,true);
	const bool 	  hasText 		= textSize.x > 0;

	/* get the space between element*/
	const float innerSpacing 			= hasText ? ((frame_padding >= 0) 	? frame_padding : (style.ItemInnerSpacing.x)) : 0.f;
	const ImVec2 padding 				= (frame_padding >= 0) 				? ImVec2(frame_padding, frame_padding) : style.FramePadding;

	/* get the size of the whole */
	const ImVec2 totalSizeWithoutPadding(size.x + innerSpacing,size.y > textSize.y ? size.y : textSize.y);

	/* get the bound-box of the whole*/
	const ImRect bb(window->DC.CursorPos, window->DC.CursorPos + totalSizeWithoutPadding + padding * 2);
	
	/* get the pos of the text */
	ImVec2 start(0,0);
	start = window->DC.CursorPos + padding;
	
	/* get the pos of the text depending on coeff */
	if (size.y < textSize.y) 
		start.y += (textSize.y - size.y) * 0.1f;
	
	const ImRect image_bb(start, start + size);
	start 	= window->DC.CursorPos + padding;
	start.x += innerSpacing;

	if (size.y > textSize.y) 
		start.y += (size.y - textSize.y) * 0.1f;
	
	/* add the text to the thing to display*/
	ImGui::ItemSize(bb);
	if (!ImGui::ItemAdd(bb, id))
		return;
	
	/* Begin Render */
	const ImU32 col = ImGui::GetColorU32(ImGuiCol_WindowBg);
	ImGui::RenderFrame(bb.Min, bb.Max, col, true, ImClamp(ImMin(padding.x, padding.y), 0.0f, style.FrameRounding));

	/* get the bounding box of the life and damage */
	ImRect barBb		= bb;

	/* get the bouding box of life */
	barBb.Min.x 	+= 2 * padding.x;
   	barBb.Min.y		+= 2 * padding.y + textSize.y + (size.y - textSize.y) * 0.1f;	
	barBb.Max.x 	-= (2 * padding.x) + innerSpacing;
   	barBb.Max.y		-= (2 * padding.y);	

	ImRect lifeBb 		= barBb;
	lifeBb.Max.x		= (lifeBb.Max.x - lifeBb.Min.x) * lifeCoeff + lifeBb.Min.x;

	/* get bounding box of damage */
	ImRect damageBb 	= barBb;
	damageBb.Min.x		= lifeBb.Max.x;

	/* adding things to render into list */
	if (bg_col.w > 0.0f)
	{
		window->DrawList->AddRectFilled(image_bb.Min, image_bb.Max, ImGui::GetColorU32(bg_col));
		window->DrawList->AddRectFilled(lifeBb.Min, lifeBb.Max, ImGui::GetColorU32(bg_col));
		window->DrawList->AddRectFilled(damageBb.Min, damageBb.Max, ImGui::GetColorU32(bg_col));
	}

	/* Render texture */
	window->DrawList->AddImage(static_cast<ImTextureID>(&texId), image_bb.Min, image_bb.Max, uv0, uv1, ImGui::GetColorU32(tint_col));
	window->DrawList->AddImage(static_cast<ImTextureID>(&lifeId), lifeBb.Min, lifeBb.Max, uv0, uv1, ImGui::GetColorU32(tint_col));
	window->DrawList->AddImage(static_cast<ImTextureID>(&damageId), damageBb.Min, damageBb.Max, uv0, uv1, ImGui::GetColorU32(tint_col));

	/* Render Text*/
	if (textSize.x > 0) 
		ImGui::RenderText(start,label);
	
}
