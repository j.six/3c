#include <imgui.h>

#define IMGUI_DEFINE_MATH_OPERATORS

#include <imgui_internal.h>
#include "../../include/Pi/TextButton.hpp"

#include <iostream>

//================================================== TEXTBUTTON ================================================

namespace Pi
{
	#define ImGuiWindowFlags_No ImGuiWindowFlags_NoBackground|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoScrollbar|ImGuiWindowFlags_NoScrollWithMouse|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoSavedSettings|ImGuiWindowFlags_NoMove
	
	//========================= CONSTRUCTORS ===================================
	
	TextButton::TextButton	(const WidgetsArgs& Args, const ImVec4& textColor):
		Widgets		(Args),
		message_	{Args.name},
		textColor_	{textColor}
	{
	}

	TextButton::TextButton	(const std::string& message, const WidgetsArgs& Args, const ImVec4& textColor):
		Widgets		(Args),
		message_	{message},
		textColor_	{textColor}
	{
	}

	TextButton::TextButton	(Widgets&& movedWidgets, const std::string& message, const ImVec4& textColor):
		Widgets		(std::move(movedWidgets)),
		message_	{std::move(message)},
		textColor_	{textColor}
	{
	}

	TextButton::TextButton	(TextButton&& movedTextButton):
		Widgets		(std::move(movedTextButton)),
		message_	{std::move(movedTextButton.message_)},
		textColor_	{std::move(movedTextButton.textColor_)}
	{
	}
	
	TextButton::~TextButton	()
	{
	}
	
	//========================= DISPLAY METHODS ===================================
	
	void 	TextButton::draw	(const ImVec2& parentPos)
	{
				/* set the Widgets option */
				ImGui::SetNextWindowFocus();
				ImGui::SetWindowPos(name_.c_str(),{parentPos.x + pos_.x, parentPos.y + pos_.y});
				ImGui::Begin(name_.c_str(), nullptr, ImGuiWindowFlags_No);
				ImGui::SetWindowSize(size_);
			
				ImGui::PushStyleColor(ImGuiCol_Text,textColor_);
				ImGui::PushStyleColor(ImGuiCol_Button,{0,0,0,0});
				ImGui::PushStyleColor(ImGuiCol_ButtonActive,{0,0,0,0});	
				ImGui::PushStyleColor(ImGuiCol_ButtonHovered,color_);

				/* display the Widgets and get the Button behavior*/
				pressed_ = ImGui::ImageButtonWithText(*texture_,message_.c_str(),{size_.x,size_.y},uv0_,uv1_);
		
				ImGui::PopStyleColor(4);
			   	ImGui::End();

			/* display the children */
			for (std::unique_ptr<Widgets>& i : children_)
			{
				i->draw({parentPos.x + pos_.x, parentPos.y + pos_.y});
			}
	}
}

//===================	BUTTON WITH TEXT FUNCTION =================================

IMGUI_API bool ImGui::ImageButtonWithText(
Magnum::GL::Texture2D& texId,
const char* label,
const ImVec2& imageSize,
const ImVec2& uv0, const ImVec2& uv1,
float frame_padding, 
const ImVec4& bg_col,
const ImVec4& tint_col)
{
	/* set the widgets display on the window*/
	ImGuiWindow* window = ImGui::GetCurrentWindow();
	
	/* pass if not wanted*/
	if (window->SkipItems)
		return false;
	
	/*making sure we, at least, have the place to pint the text*/
	ImVec2 size = imageSize;
	if (size.x <= 0 && size.y <= 0) 
	{	
		size.x = size.y = ImGui::GetTextLineHeightWithSpacing();
	}
	else 
	{
		if (size.x <= 0)          
			size.x = size.y;
		else if (size.y <= 0)     
			size.y = size.x;
		
		size *= window->FontWindowScale * ImGui::GetIO().FontGlobalScale;
	}
	
	/* get the context */
	ImGuiContext& 	  g 		= *GImGui;
	const ImGuiStyle& style 	= g.Style;
	/* get the text info */
	const ImGuiID id 			= window->GetID(label);
	const ImVec2  textSize 		= ImGui::CalcTextSize(label,nullptr,true);
	const bool 	  hasText 		= textSize.x > 0;


	/* get the space between element*/
	const float innerSpacing 			= hasText ? ((frame_padding >= 0) 	? frame_padding : (style.ItemInnerSpacing.x)) : 0.f;
	const ImVec2 padding 				= (frame_padding >= 0) 				? ImVec2(frame_padding, frame_padding) : style.FramePadding;

	/* get the size of the whole */
	const ImVec2 totalSizeWithoutPadding(size.x + innerSpacing,size.y > textSize.y ? size.y : textSize.y);

	/* get the bound-box of the whole*/
	const ImRect bb(window->DC.CursorPos, window->DC.CursorPos + totalSizeWithoutPadding + padding * 2);
	
	/* get the pos of the text */
	ImVec2 start(0,0);
	start = window->DC.CursorPos + padding;
	
	/* get the pos of the text depending on coeff */
	if (size.y < textSize.y) 
		start.y += (textSize.y - size.y) * 0.5f;
	
	const ImRect image_bb(start, start + size);
	start 	= window->DC.CursorPos + padding;
	start.x += innerSpacing;

	if (size.y > textSize.y) 
		start.y += (size.y - textSize.y) * 0.5f;
	
	/* add the text to the thing to display*/
	ImGui::ItemSize(bb);
	if (!ImGui::ItemAdd(bb, id))
		return false;
	
	/* Get Button Behavior */
	bool hovered = false, held = false;
	bool pressed = ImGui::ButtonBehavior(bb, id, &hovered, &held);
	
	/* Begin Render */
	const ImU32 col = ImGui::GetColorU32((hovered && held) ? ImGuiCol_ButtonActive : hovered ? ImGuiCol_ButtonHovered : ImGuiCol_Button);
	ImGui::RenderFrame(bb.Min, bb.Max, col, true, ImClamp(ImMin(padding.x, padding.y), 0.0f, style.FrameRounding));
	
	/* Render texture */
	if (bg_col.w > 0.0f)
		window->DrawList->AddRectFilled(image_bb.Min, image_bb.Max, ImGui::GetColorU32(bg_col));
	
	window->DrawList->AddImage(static_cast<ImTextureID>(&texId), image_bb.Min, image_bb.Max, uv0, uv1, ImGui::GetColorU32(tint_col));
	
	/* Render Text*/
	if (textSize.x > 0) 
		ImGui::RenderText(start,label);
	
	return pressed;
}

