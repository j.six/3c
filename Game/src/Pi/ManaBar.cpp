#include <Magnum/Trade/AbstractImporter.h>
#include <Magnum/Trade/ImageData.h>
#include <Magnum/ImageView.h>
#include <Magnum/GL/Texture.h>
#include <Magnum/GL/TextureFormat.h>
#include <Corrade/Containers/ArrayView.h>
#include <Corrade/Containers/Optional.h>
#include <Corrade/Containers/Reference.h>
#include <Corrade/PluginManager/Manager.h>
#include <Magnum/GL/Renderer.h>
#include <cmath>

#include <iostream>


#include <imgui.h>

#define IMGUI_DEFINE_MATH_OPERATORS

#include <imgui_internal.h>


#include "../../include/Pi/ManaBar.hpp"

constexpr const char* ManaPath 		= "Game/resources/textures/Mana.png";

namespace Pi
{
	#define ImGuiWindowFlags_No ImGuiWindowFlags_NoBackground|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoScrollbar|ImGuiWindowFlags_NoScrollWithMouse|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoSavedSettings|ImGuiWindowFlags_NoMove
	
	//========================= CONSTRUCTORS ===================================

	ManaBar::ManaBar	(float& mana, const WidgetsArgs& Args, const ImVec4& textColor):
		Widgets		(Args),
		mana_		{mana},
		maxMana_	{mana},
		textColor_	{textColor}
	{
		/* no texture is needed */
		if (Args.rs != nullptr )
		{
			/* taking the ressource in the RessourceManager */
			Resource<Texture> manaTex{Args.rs->get<Texture>(std::string(ManaPath))};
			
			if (!manaTex)
			{
				/* if there's none loading into the RessourceManager*/
				GL::Texture2D manaData 		= std::move(loadTexture(ManaPath));	
				Args.rs->set<Texture>		(manaTex.key(),std::move(manaData));
				manaTex_ 					=  &(*(*Args.rs->get<Texture>(manaTex.key())));
			}
			else
			{
				/* else taking it in*/
				manaTex_ 	= &(*(*manaTex));	
			}
		}
	}

	ManaBar::ManaBar(Widgets&& movedWidgets, float& mana, GameResourceManager* rs, const ImVec4& textColor):
		Widgets		(std::move(movedWidgets)),
		mana_		{mana},
		maxMana_	{mana},
		textColor_	{textColor}
	{
		/* no texture is needed */
		if (rs != nullptr )
		{
			/* taking the ressource in the RessourceManager */
			Resource<Texture> manaTex{rs->get<Texture>(std::string(ManaPath))};

			
			if (!manaTex)
			{
				/* if there's none loading into the RessourceManager*/
				GL::Texture2D manaData 		= std::move(loadTexture(ManaPath));	
				rs->set<Texture>			(manaTex.key(),std::move(manaData));
				manaTex_ 					=  &(*(*rs->get<Texture>(manaTex.key())));
			
			}
			else
			{
				/* else taking it in*/
				manaTex_ 	= &(*(*manaTex));
			}
		}

	}

	ManaBar::ManaBar	(ManaBar&& movedManaBar):
		Widgets		(std::move(movedManaBar)),
		mana_		{movedManaBar.mana_},
		maxMana_	{movedManaBar.maxMana_},
		manaTex_	{movedManaBar.manaTex_},
		textColor_	{std::move(movedManaBar.textColor_)} 
	{
	}
	
	ManaBar::~ManaBar	()
	{
	}
		
//========================= UPDATE	METHODS ===================================

	void	ManaBar::update	(const float& deltaTime)
	{
		switch (context_)
		{
			case Context::ANIM:
				/* no need to update if anim_ is finished */
				if (anim_ == 0)
					break;

				/* making anim_ continue */
				anim_ -= deltaTime * animSpeed_;
				
				/* puting anim_ to end */
				if (0 >= anim_)
				{	
					anim_ = 0;
				}
				break;
			default:
				break;
		}		

		/* update the children_'s Widgets */
		for (std::unique_ptr<Widgets>& i : children_)
		{
			i->update(deltaTime);
		}
	}

	//========================= DISPLAY METHODS ===================================



	void 	ManaBar::draw	(const ImVec2& parentPos)
	{
		static float oldMana = mana_;
		static float newMana = mana_;
		static float manaDisplay = oldMana;
	
		/* Guards */
		if (mana_ < 0)
			mana_ = 0;
		
		switch (context_ )
		{
			case Context::ANIM:
			/* mana_ has changed */
			if (mana_ != oldMana)
			{

				/* the mana_ is different from the one 
				we were going for the end*/
				if (newMana != mana_)
				{
					/* the interpolation is between the new mana
					and the mana that we had*/
					newMana = mana_;
					oldMana = manaDisplay;
					anim_ = 1;

				}

				/* Interpolation */
				manaDisplay = Math::Implementation::lerp(mana_,oldMana,anim_);
				
				/* Stopping animation */
				if (manaDisplay == mana_)
				{
					oldMana 	= mana_;
				}
				
				/* Get the anim_ ready for if the time change again*/
				reset();	
			}
			break;
			default:
			/* Just displaying the time */
				manaDisplay = mana_;
			break;
		}

		ImGui::SetWindowPos(name_.c_str(),{parentPos.x + pos_.x, parentPos.y + pos_.y});
		ImGui::Begin(name_.c_str(), nullptr, ImGuiWindowFlags_No);
		ImGui::SetWindowSize(size_);
		ImGui::PushStyleColor(ImGuiCol_Text,textColor_);
		ImGui::PushStyleColor(ImGuiCol_WindowBg,color_);


		/*choose display between anim_*/
		context_ == Context::ANIM ?
		ImGui::manaBar(*texture_,*manaTex_,name_.c_str(),manaDisplay/maxMana_,
						size_,uv0_,uv1_):
		ImGui::manaBar(*texture_,*manaTex_,name_.c_str(),mana_/maxMana_,
						size_,uv0_,uv1_);
		
		
		ImGui::PopStyleColor(2);
	  	ImGui::End();

		/* display children */
		for (std::unique_ptr<Widgets>& i : children_)
		{
			i->draw({parentPos.x + pos_.x, parentPos.y + pos_.y});
		}
	}
}

//========================== INTERNAL FUNCTION ====================================

IMGUI_API void ImGui::manaBar(
Magnum::GL::Texture2D& texId, Magnum::GL::Texture2D& manaId,
const char* 	label, 
const float& 	manaCoeff,
const ImVec2& 	imageSize, 
const ImVec2& 	uv0, const ImVec2& uv1, 
float frame_padding,
const ImVec4& bg_col, 
const ImVec4& tint_col) 
{
	/* set the widgets display on the window*/
	ImGuiWindow* window = ImGui::GetCurrentWindow();
	
	/* pass if not wanted*/
	if (window->SkipItems)
		return;
	
	/*making sure we, at least, have the place to pint the text*/
	ImVec2 size = imageSize;
	if (size.x <= 0 && size.y <= 0) 
	{	
		size.x = size.y = ImGui::GetTextLineHeightWithSpacing();
	}
	else 
	{
		if (size.x <= 0)          
			size.x = size.y;
		else if (size.y <= 0)     
			size.y = size.x;
		
		size *= window->FontWindowScale * ImGui::GetIO().FontGlobalScale;
	}
	
	/* get the context */
	ImGuiContext& 	  g 		= *GImGui;
	const ImGuiStyle& style 	= g.Style;
	/* get the text info */
	const ImGuiID id 			= window->GetID(label);
	const ImVec2  textSize 		= ImGui::CalcTextSize(label,nullptr,true);
	const bool 	  hasText 		= textSize.x > 0;

	/* get the space between element*/
	const float innerSpacing 			= hasText ? ((frame_padding >= 0) 	? frame_padding : (style.ItemInnerSpacing.x)) : 0.f;
	const ImVec2 padding 				= (frame_padding >= 0) 				? ImVec2(frame_padding, frame_padding) : style.FramePadding;

	/* get the size of the whole */
	const ImVec2 totalSizeWithoutPadding(size.x + innerSpacing,size.y > textSize.y ? size.y : textSize.y);

	/* get the bound-box of the whole*/
	const ImRect bb(window->DC.CursorPos, window->DC.CursorPos + totalSizeWithoutPadding + padding * 2);
	
	/* get the pos of the text */
	ImVec2 start(0,0);
	start = window->DC.CursorPos + padding;
	
	/* get the pos of the text depending on coeff */
	if (size.y < textSize.y) 
		start.y += (textSize.y - size.y) * 0.1f;
	
	const ImRect image_bb(start, start + size);
	start 	= window->DC.CursorPos + padding;
	start.x += innerSpacing;

	if (size.y > textSize.y) 
		start.y += (size.y - textSize.y) * 0.1f;
	
	/* add the text to the thing to display*/
	ImGui::ItemSize(bb);
	if (!ImGui::ItemAdd(bb, id))
		return;
	
	/* Begin Render */
	const ImU32 col = ImGui::GetColorU32(ImGuiCol_WindowBg);
	ImGui::RenderFrame(bb.Min, bb.Max, col, true, ImClamp(ImMin(padding.x, padding.y), 0.0f, style.FrameRounding));


	/* get the bounding box of the mana */
	ImRect barBb		= bb;

	barBb.Min.x 	+= 2 * padding.x;
   	barBb.Min.y		+= 2 * padding.y + textSize.y + (size.y - textSize.y) * 0.1f;	
	barBb.Max.x 	-= 2 * padding.x + innerSpacing;
   	barBb.Max.y		-= 2 * padding.y;	

	ImRect manaBb 		= barBb;
	manaBb.Max.x		= (manaBb.Max.x - manaBb.Min.x) * manaCoeff + manaBb.Min.x;

	/* adding things to render into list */
	if (bg_col.w > 0.0f)
	{
		window->DrawList->AddRectFilled(image_bb.Min, image_bb.Max, ImGui::GetColorU32(bg_col));
		window->DrawList->AddRectFilled(manaBb.Min, manaBb.Max, ImGui::GetColorU32(bg_col));
	}

	/* Render texture */
	window->DrawList->AddImage(static_cast<ImTextureID>(&texId), image_bb.Min, image_bb.Max, uv0, uv1, ImGui::GetColorU32(tint_col));
	window->DrawList->AddImage(static_cast<ImTextureID>(&manaId), manaBb.Min, manaBb.Max, uv0, uv1, ImGui::GetColorU32(tint_col));

	/* Render Text*/
	if (textSize.x > 0) 
		ImGui::RenderText(start,label);
	
}
