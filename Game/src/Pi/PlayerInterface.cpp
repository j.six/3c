#include <Magnum/Trade/AbstractImporter.h>
#include <Magnum/Trade/ImageData.h>
#include <Magnum/ImageView.h>
#include <Magnum/GL/Texture.h>
#include <Magnum/GL/TextureFormat.h>
#include <Corrade/Containers/ArrayView.h>
#include <Corrade/Containers/Optional.h>
#include <Corrade/Containers/Reference.h>
#include <Corrade/PluginManager/Manager.h>
#include <Magnum/GL/Renderer.h>

#include <iostream>

#include "../../include/Pi/PlayerInterface.hpp"
#include "../../include/Pi/TextButton.hpp"

using namespace Pi;

//================================= GAME UI's Value =============================//

constexpr float			PnjTextBoxPos	= 4.0f;
constexpr const char* 	PnjTextBoxPath 	= "Game/resources/textures/dialogboxalpha.png";
constexpr float		 	PnjTextBoxAnim 	= 0.50f;

constexpr float			EntityInfoScaleX = 4.0f;
constexpr float			EntityInfoScaleY = 10.0f;
constexpr const char* 	EntityInfoPath 	 = "Game/resources/textures/wood.jpg";

constexpr float			TimeScaleX 		= 2.0f;
constexpr float			TimeScaleY 		= 12.0f;
constexpr const char* 	TimePath 	 	= "Game/resources/textures/wood.jpg";

#define ImGuiWindowFlags_No ImGuiWindowFlags_NoBackground|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoScrollbar|ImGuiWindowFlags_NoScrollWithMouse|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoSavedSettings|ImGuiWindowFlags_NoMove

//========================= CONSTRUCTORS ===================================

PlayerInterface::PlayerInterface	(const Vector2i& windowSize, const Vector2i& frameBufferSize, const Vector2& dpiScaling):
	    ImGuiIntegration::Context 	{Vector2{windowSize}/dpiScaling, windowSize, frameBufferSize},
		Widgets						{{nullptr,"","Pi",{0,0},windowSize}},
		size_						{static_cast<float>(windowSize.x()),static_cast<float>(windowSize.y())},
		io_							{ImGui::GetIO()},
		font_						{nullptr}
{
		/* Put the ImGui window on the foreground */
		GL::Renderer::setBlendEquation(GL::Renderer::BlendEquation::Add,
				GL::Renderer::BlendEquation::Add);
		GL::Renderer::setBlendFunction(GL::Renderer::BlendFunction::SourceAlpha,
				GL::Renderer::BlendFunction::OneMinusSourceAlpha);
	
		/* load font and put it as the default in the ImGuiIntegration::Context */
		font_ = io_.Fonts->AddFontFromFileTTF("Game/resources/font/GreatVibes-Regular.otf", 40.0f);
		relayout(Vector2i(size_));

}

PlayerInterface::PlayerInterface(const PlayerInterface& copyPlayerInterface):
	ImGuiIntegration::Context	(NoCreate),
	Widgets						{copyPlayerInterface},
	size_						{copyPlayerInterface.size_},
	io_							{copyPlayerInterface.io_},
	font_						{copyPlayerInterface.font_}
{
}

PlayerInterface::PlayerInterface	(NoCreateT):
	ImGuiIntegration::Context 	(NoCreate),
	Widgets						(),
	size_						{0,0},
	io_							{},
	font_						{nullptr}
{
}


PlayerInterface::PlayerInterface	(PlayerInterface&& movedPlayerInterface):
	ImGuiIntegration::Context 	(std::move(movedPlayerInterface)),
	Widgets						(std::move(movedPlayerInterface)),
	size_						{std::move(movedPlayerInterface.size_)},
	io_							{std::move(movedPlayerInterface.io_)},
	font_						{movedPlayerInterface.font_}
{
}


PlayerInterface::~PlayerInterface	()
{
}

//========================= 	OPERATOR 	===================================

PlayerInterface& 	PlayerInterface::operator=	(PlayerInterface&& movedPlayerInterface)
{
	ImGuiIntegration::Context::	operator=	(std::move(movedPlayerInterface));
	Widgets::					operator=	(std::move(movedPlayerInterface));
	size_						=			(std::move(movedPlayerInterface.size_));
	io_							=			(std::move(movedPlayerInterface.io_));

	/* load font and put it as the default in the ImGuiIntegration::Context */
	font_ = io_.Fonts->AddFontFromFileTTF("Game/resources/font/GreatVibes-Regular.otf", 30.0f);
	relayout(Vector2i(size_));

	return  *this;
}

//========================= DISPLAY METHODS ===================================//

void 	PlayerInterface::draw	(Platform::Application& appli, const float& deltaTime)
{

	if (deltaTime > 0.0f)
		update(deltaTime);

	/* creation of Window */
	newFrame();

	/* ImGui widget calls here ... */
	updateApplicationCursor(appli);

	/* choose font */
	if (font_ != nullptr)
	{
		ImGui::PushFont(font_);
	}
	
	/* draw each Widgets in the PlayerInterface */
	for (std::unique_ptr<Widgets>& i : children_)
	{
		i->draw({pos_.x,pos_.y});
	}

	if (font_ != nullptr)
		ImGui::PopFont();

	/* enable window to render */	
	GL::Renderer::enable(GL::Renderer::Feature::Blending);
	GL::Renderer::enable(GL::Renderer::Feature::ScissorTest);
	GL::Renderer::disable(GL::Renderer::Feature::DepthTest);
	GL::Renderer::disable(GL::Renderer::Feature::FaceCulling);

	/* Display */
	drawFrame();

	/* put back the thing to draw like we would want to*/	
	GL::Renderer::disable(GL::Renderer::Feature::Blending);	
	GL::Renderer::disable(GL::Renderer::Feature::ScissorTest);
	GL::Renderer::enable(GL::Renderer::Feature::DepthTest);
	GL::Renderer::enable(GL::Renderer::Feature::FaceCulling);
}

//========================== Game UI's METHODS ===============================

void 	PlayerInterface::PnjTextBox		(GameResourceManager* rs, const std::string& name , const std::vector<std::string>& message)
{

	/* Guards (A Widgets with the same name is found)*/
	if (get(name) != *this)
		return;

	add(std::move(TextBox(	{message,
							 {1,1,0.7,1},
								{rs,
								 PnjTextBoxPath,
								 name,
								 {0.0f,size_.y() - (size_.y()/PnjTextBoxPos)},
								 {static_cast<int>(size_.x()),
								 static_cast<int>(size_.y()/ PnjTextBoxPos) - static_cast<int>(size_.y()/ (PnjTextBoxPos * PnjTextBoxPos))},
								 {0,0,0,0},
								 {0,0},
								 {1,1},
								 Pi::Widgets::Context::ANIM,
								 PnjTextBoxAnim}}
								 
								 )));

}


void	PlayerInterface::EntityInfo		(GameResourceManager* rs, const std::string& name, float& life, float& mana, ImVec2 pos)
{
	Widgets containerWidgets = WidgetsArgs(nullptr,"",name.c_str(),{pos.x * size_.x(), pos.y * size_.y()});


	Vector2i size = {static_cast<int>(size_.x()/ (EntityInfoScaleX)), static_cast<int>(size_.y()/ EntityInfoScaleY)};
	containerWidgets.add(std::move(LifeBar(life,life,
								{rs,
								 EntityInfoPath,
								 name + "'s Life: ",
								 {0.0f,0.0f},
								 size},
								 {1,1,1,1}
								 )));

	containerWidgets.add(std::move(ManaBar(mana,
								{rs,
								 EntityInfoPath,
								 name + "'s Mana: ",
								 {0.0f,(size_.y()/EntityInfoScaleY)},
								 size},
								 {1,1,1,1}
								 )));

	add(std::move(containerWidgets));
}

void	PlayerInterface::TimeDisp		(GameResourceManager* rs, const std::string& name , float& time, ImVec2 pos)
{
	Vector2i size = {static_cast<int>(size_.x()/ (TimeScaleX)), static_cast<int>(size_.y()/ TimeScaleY)};
	add(std::move(TimeBar(time,
								{rs,
								 EntityInfoPath,
								 name,
								 {pos.x * size_.x(), pos.y * size_.y()},
								 size},
								 {1,0,0,1}
								 )));
}

void	PlayerInterface::StartMenu		(GameResourceManager* rs, const std::string& name)
{
	Widgets containerWidgets = WidgetsArgs(nullptr,"",name.c_str(),{0,0});

	containerWidgets.add(std::move(Pi::Widgets(
					{rs,
					"Game/resources/donata.jpg",
					"BackGround",
					{0,0},
					{static_cast<int>(size_.x()),static_cast<int>(size_.y())}})));

	containerWidgets.add(std::move(Pi::TextButton(
					{rs,
					"Game/resources/play.png",
					"PlayButton",
					{100.0f,300.0f},
					{200,70},
					{0.5,0.5,0.5,0.3}})));
	
	containerWidgets.add(std::move(Pi::TextButton(
				{rs,
				"Game/resources/quit.png",
				"ExitButton",
				{100.0f,420.0f},
				{200,70},
				{0.5,0.5,0.5,0.3}})));

	add(std::move(containerWidgets));
}
