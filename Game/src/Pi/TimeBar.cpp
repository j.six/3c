#include <Magnum/Trade/AbstractImporter.h>
#include <Magnum/Trade/ImageData.h>
#include <Magnum/ImageView.h>
#include <Magnum/GL/Texture.h>
#include <Magnum/GL/TextureFormat.h>
#include <Corrade/Containers/ArrayView.h>
#include <Corrade/Containers/Optional.h>
#include <Corrade/Containers/Reference.h>
#include <Corrade/PluginManager/Manager.h>
#include <Magnum/GL/Renderer.h>
#include <cmath>

#include <iostream>


#include <imgui.h>

#define IMGUI_DEFINE_MATH_OPERATORS

#include <imgui_internal.h>


#include "../../include/Pi/TimeBar.hpp"

constexpr const char* TimePath 		= "Game/resources/textures/Time.png";

namespace Pi
{
	#define ImGuiWindowFlags_No ImGuiWindowFlags_NoBackground|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoScrollbar|ImGuiWindowFlags_NoScrollWithMouse|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoSavedSettings|ImGuiWindowFlags_NoMove
	
	//========================= CONSTRUCTORS ===================================

	TimeBar::TimeBar	(float& time, const WidgetsArgs& Args, const ImVec4& textColor):
		Widgets		(Args),
		time_		{time},
		maxTime_	{time},
		textColor_	{textColor}
	{
		/* no texture is needed */
		if (Args.rs != nullptr )
		{
			/* taking the ressource in the RessourceManager */
			Resource<Texture> timeTex{Args.rs->get<Texture>(std::string(TimePath))};
			
			/* if there's none loading into the RessourceManager*/
			if (!timeTex)
			{
				GL::Texture2D timeData 		= std::move(loadTexture(TimePath));	
				Args.rs->set<Texture>		(timeTex.key(),std::move(timeData));	
				timeTex_ 					=  &(*(*Args.rs->get<Texture>(timeTex.key())));
			}
			else
			{	
				/* else taking it in*/
				timeTex_ 	= &(*(*timeTex));	
			}
		}
	}

	TimeBar::TimeBar(Widgets&& movedWidgets, float& time, GameResourceManager* rs, const ImVec4& textColor):
		Widgets		(std::move(movedWidgets)),
		time_		{time},
		maxTime_	{time},
		textColor_	{textColor}
	{
		/* no texture is needed */
		if (rs != nullptr )
		{
			/* taking the ressource in the RessourceManager */
			Resource<Texture> timeTex{rs->get<Texture>(std::string(TimePath))};
			
			/* if there's none loading into the RessourceManager*/
			if (!timeTex)
			{
				GL::Texture2D timeData 		= std::move(loadTexture(TimePath));	
				rs->set<Texture>			(timeTex.key(),std::move(timeData));	
				timeTex_ 					=  &(*(*rs->get<Texture>(timeTex.key())));
			}
			else
			{	
				/* else taking it in*/
				timeTex_ 	= &(*(*timeTex));	
			}
		}
	}

	TimeBar::TimeBar	(TimeBar&& movedTimeBar):
		Widgets		(std::move(movedTimeBar)),
		time_		{movedTimeBar.time_},
		maxTime_	{movedTimeBar.maxTime_},
		timeTex_	{movedTimeBar.timeTex_},
		textColor_	{std::move(movedTimeBar.textColor_)}
	{
	}
	
	TimeBar::~TimeBar	()
	{
	}
		
//========================= UPDATE	METHODS ===================================

	void	TimeBar::update	(const float& deltaTime)
	{
		switch (context_)
		{
			case Context::ANIM:
	
				/* no need to update if anim_ is finished */
				if (anim_ == 0)
					break;
				
				/* making anim_ continue */
				anim_ -= deltaTime * animSpeed_;
				
				/* puting anim_ to end */
				if (0 >= anim_)
					anim_ = 0;
				
				break;
			default:
				break;
		}

		/* update the children_'s Widgets */
		for (std::unique_ptr<Widgets>& i : children_)
		{
			i->update(deltaTime);
		}
	}

	//========================= DISPLAY METHODS ===================================



	void 	TimeBar::draw	(const ImVec2& parentPos)
	{
		static float oldTime = time_;
		static float newTime = time_;
		static float timeDisplay = oldTime;
	
		/* Guards */
		if (time_ < 0)
			time_ = 0;
		
		switch (context_)
		{
			case  Context::ANIM:
			
			/* time has changed */
			if (time_ != oldTime)
			{
				/* the time_ is different from the one 
					we were going for */
				if (newTime != time_)
				{
					/* the interpolation is between the new time
						and the time that we had*/
					newTime = time_;
					oldTime = timeDisplay;
					anim_ 	= 1.0f;
				}

				/* Interpolation */
				timeDisplay = Math::Implementation::lerp(time_,oldTime,anim_);
				
				/* Stopping animation */
				if (timeDisplay == time_)
				{
					oldTime 	= time_;

				}
				/* Get the anim_ ready for if the time change again*/
				reset();	
			}
			break;
			default:
				/* Just displaying the time */
				timeDisplay = time_;
			break;
		}

		ImGui::SetWindowPos(name_.c_str(),{parentPos.x + pos_.x, parentPos.y + pos_.y});
		ImGui::Begin(name_.c_str(), nullptr, ImGuiWindowFlags_No);
		ImGui::SetWindowSize(size_);
		ImGui::PushStyleColor(ImGuiCol_Text,textColor_);
		ImGui::PushStyleColor(ImGuiCol_WindowBg,color_);

		/*choose display between context_*/
		context_ == Context::ANIM ?
		ImGui::timeBar(*texture_,*timeTex_,name_.c_str(),timeDisplay/maxTime_,time_,
						size_,uv0_,uv1_):
		ImGui::timeBar(*texture_,*timeTex_,name_.c_str(),time_/maxTime_,time_,
						size_,uv0_,uv1_);

		ImGui::PopStyleColor(2);
	  	ImGui::End();

		/* display children */
		for (std::unique_ptr<Widgets>& i : children_)
		{
			i->draw({parentPos.x + pos_.x, parentPos.y + pos_.y});
		}
	}
}

//========================== INTERNAL FUNCTION ====================================

IMGUI_API void ImGui::timeBar(
Magnum::GL::Texture2D& texId, Magnum::GL::Texture2D& timeId,
const char* 	label, 
const float& 	timeCoeff,
const float& 	time,
const ImVec2& 	imageSize, 
const ImVec2& 	uv0, const ImVec2& uv1, 
float frame_padding,
const ImVec4& bg_col, 
const ImVec4& tint_col) 
{
	/* set the widgets display on the window*/
	ImGuiWindow* window = ImGui::GetCurrentWindow();
	
	/* pass if not wanted*/
	if (window->SkipItems)
		return;
	
	/*making sure we, at least, have the place to pint the text*/
	ImVec2 size = imageSize;
	if (size.x <= 0 && size.y <= 0) 
	{	
		size.x = size.y = ImGui::GetTextLineHeightWithSpacing();
	}
	else 
	{
		if (size.x <= 0)          
			size.x = size.y;
		else if (size.y <= 0)     
			size.y = size.x;
		
		size *= window->FontWindowScale * ImGui::GetIO().FontGlobalScale;
	}
	
	/* get the context */
	ImGuiContext& 	  g 		= *GImGui;
	const ImGuiStyle& style 	= g.Style;
	/* get the text info */
	const ImGuiID id 			= window->GetID(label);
	const ImVec2  textSize 		= ImGui::CalcTextSize(label,nullptr,true);
	const bool 	  hasText 		= textSize.x > 0;

	/* get the space between element*/
	const float innerSpacing 			= hasText ? ((frame_padding >= 0) 	? frame_padding : (style.ItemInnerSpacing.x)) : 0.f;
	const ImVec2 padding 				= (frame_padding >= 0) 				? ImVec2(frame_padding, frame_padding) : style.FramePadding;

	/* get the size of the whole */
	const ImVec2 totalSizeWithoutPadding(size.x + innerSpacing,size.y > textSize.y ? size.y : textSize.y);

	/* get the bound-box of the whole*/
	const ImRect bb(window->DC.CursorPos, window->DC.CursorPos + totalSizeWithoutPadding + padding * 2);
	
	/* get the pos of the text */
	ImVec2 start(0,0);
	start = window->DC.CursorPos + padding;
	
	/* get the pos of the text depending on coeff */
	if (size.y < textSize.y) 
		start.y += (textSize.y - size.y) * 0.5f;
	
	const ImRect image_bb(start, start + size);
	start 	= window->DC.CursorPos + padding;
	start.x += innerSpacing;

	if (size.y > textSize.y) 
		start.y += (size.y - textSize.y) * 0.5f;
	
	/* add the text to the thing to display*/
	ImGui::ItemSize(bb);
	if (!ImGui::ItemAdd(bb, id))
		return;
	
	/* Begin Render */
	const ImU32 col = ImGui::GetColorU32(ImGuiCol_WindowBg);
	ImGui::RenderFrame(bb.Min, bb.Max, col, true, ImClamp(ImMin(padding.x, padding.y), 0.0f, style.FrameRounding));

	/* get the bounding box of the time */
	ImRect barBb		= bb;

	barBb.Min.x 	+= padding.x * 2 + innerSpacing + textSize.x;
   	barBb.Min.y		+= 2 * padding.y;	
	barBb.Max.x 	-= 2 * padding.x + innerSpacing;
   	barBb.Max.y		-= 2 * padding.y;	

	ImRect timeBb 		= barBb;
	timeBb.Max.x		= (timeBb.Max.x - timeBb.Min.x) * timeCoeff + timeBb.Min.x;


	/* adding things to render into list */
	if (bg_col.w > 0.0f)
	{
		window->DrawList->AddRectFilled(image_bb.Min, image_bb.Max, ImGui::GetColorU32(bg_col));
		window->DrawList->AddRectFilled(timeBb.Min, timeBb.Max, ImGui::GetColorU32(bg_col));
	}

	/* Render texture */
	window->DrawList->AddImage(static_cast<ImTextureID>(&texId), image_bb.Min, image_bb.Max, uv0, uv1, ImGui::GetColorU32(tint_col));
	window->DrawList->AddImage(static_cast<ImTextureID>(&timeId), timeBb.Min, timeBb.Max, uv0, uv1, ImGui::GetColorU32(tint_col));

	/* Render Text*/
	if (textSize.x > 0) 
		ImGui::RenderText(start,label);

	ImVec2 timeStart = start;

	timeStart.x += ((size.x)/2.0f);

	ImGui::RenderText(timeStart,std::string(std::to_string(time)).c_str());	
}
