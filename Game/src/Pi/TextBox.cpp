#include <imgui.h>

#define IMGUI_DEFINE_MATH_OPERATORS

#include <imgui_internal.h>
#include "../../include/Pi/TextBox.hpp"

#include <iostream>

constexpr float wordSpeedCoeff = 50.0f;

//================================================== TEXTBUTTON ================================================

namespace Pi
{
	#define ImGuiWindowFlags_No ImGuiWindowFlags_NoBackground|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoScrollbar|ImGuiWindowFlags_NoScrollWithMouse|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoSavedSettings|ImGuiWindowFlags_NoMove
	
	//========================= CONSTRUCTORS ===================================
	
	TextBox::TextBox	(const WidgetsArgs& Args, const ImVec4& textCol):
		Widgets		(Args),
		message_	{},
		textCol_	{textCol}
	{
	}

	TextBox::TextBox	(const std::vector<std::string>& message, const ImVec4& textCol, const WidgetsArgs& Args):
		Widgets		(Args),
		message_	{message},
		textCol_	{textCol}
	{
	}

	TextBox::TextBox	(Widgets&& movedWidgets, const std::vector<std::string>& message, const ImVec4& textCol):
		Widgets	(std::move(movedWidgets)),
		message_{std::move(message)},
		textCol_{std::move(textCol)}
	{
	}

	TextBox::TextBox	(TextBox&& movedTextBox):
		Widgets	(std::move(movedTextBox)),
		message_{std::move(movedTextBox.message_)},
		textCol_{std::move(movedTextBox.textCol_)}

	{
	}
	
	TextBox::~TextBox	()
	{
	}

//========================= UPDATE	METHODS ===================================

	void	TextBox::update	(const float& deltaTime)
	{
		/* update the children_ */
		Widgets::update(deltaTime);

		if (anim_ == 0)
			return;

		/*  progress anim_ making sure the time is all the same for each word
			by multiplying by wordCoeff */
		float wordCoeff = 1.0f/static_cast<float>(message_[phrasePart_].size());
		anim_ 			-= deltaTime * animSpeed_ * wordCoeff * wordSpeedCoeff; 

		/* putting back to 0.0f*/
		if (0 >= anim_)
		{	
			anim_ = 0.0f;
		}
	}

	//========================= DISPLAY METHODS ===================================
	
	void 	TextBox::draw	(const ImVec2& parentPos)
	{
		/* Guards */
		if (message_.size() <= 0)
			return;

		static ImVec2 	drawSize 	= {size_.x, size_.y * (1 - anim_)};
	
		ImGui::SetNextWindowFocus();
		ImGui::SetWindowPos(name_.c_str(),{parentPos.x + pos_.x, parentPos.y + pos_.y});
		ImGui::Begin(name_.c_str(), nullptr, ImGuiWindowFlags_No);	
		ImGui::SetWindowSize(size_);
	
		ImGui::PushStyleColor(ImGuiCol_Text,textCol_);
		ImGui::PushStyleColor(ImGuiCol_WindowBg,color_);
	
		
		switch (context_)
		{
			case Context::ANIM:
				/* display opening window until the right size is reached */
				if (drawSize.y != size_.y)
				{			
					drawSize = {size_.x,size_.y * (1 - anim_)};
					ImGui::textBox(*texture_,"","",drawSize,uv0_,{uv1_.x,uv1_.y * (1 - anim_)});

					/* reset if animation is finished */
					reset();
				}
				else
				{
					/* then display letter by letter by interpoling the animation by the message's letter nb */
					unsigned int letterNb 	= Math::Implementation::lerp(0u,
											static_cast<unsigned int>(message_[phrasePart_].size()),(1 - anim_));			
					
					ImGui::textBox(*texture_,name_.c_str(),message_[phrasePart_].substr(0,letterNb).c_str(),drawSize,uv0_,uv1_);
				}
			break;
			case Context::CLOSING:
					/* decreasing the size of the window*/
					drawSize = 	{size_.x,size_.y - (size_.y * (1 - anim_))};
					if (drawSize.y <= 0)
					{
						next();
						break;
					}
					ImGui::textBox(*texture_,"","",drawSize,uv0_,{uv1_.x,uv1_.y - (uv1_.y * (1 - anim_))});
					break;
			case Context::DISP:
				/* just displaying the window */
				ImGui::textBox(*texture_,name_.c_str(),message_[phrasePart_].c_str(),{size_.x,size_.y},uv0_,uv1_);
				break;
			default:
			break;
		}

	
		ImGui::PopStyleColor(2);
	   	ImGui::End();

		/* dispay the children_ */
		for (std::unique_ptr<Widgets>& i : children_)
		{
			i->draw({parentPos.x + pos_.x, parentPos.y + pos_.y});
		}
	}
}

//===================	INTERNAL FUNCTION	=================================

IMGUI_API void ImGui::textBox(
Magnum::GL::Texture2D& texId,
const char*		owner,
const char* 	label, 
const ImVec2& 	imageSize, 
const ImVec2& 	uv0, const ImVec2& uv1, 
float frame_padding, 
const ImVec4& bg_col, 
const ImVec4& tint_col)
{
	/* set the widgets display on the window*/
	ImGuiWindow* window = ImGui::GetCurrentWindow();
	
	/* pass if not wanted*/
	if (window->SkipItems)
		return;
	
	/*making sure we, at least, have the place to pint the text*/
	ImVec2 size = imageSize;
	if (size.x <= 0 && size.y <= 0) 
	{	
		size.x = size.y = ImGui::GetTextLineHeightWithSpacing();
	}
	else 
	{
		if (size.x <= 0)          
			size.x = size.y;
		else if (size.y <= 0)     
			size.y = size.x;
		
		size *= window->FontWindowScale * ImGui::GetIO().FontGlobalScale;
	}
	
	/* get the context */
	ImGuiContext& 	  g 		= *GImGui;
	const ImGuiStyle& style 	= g.Style;
	/* get the text info */
	const ImGuiID 	id 			= window->GetID(label);
	ImVec2  		textSize 	= ImGui::CalcTextSize(label,nullptr,true);
	ImVec2			nameSize	= ImGui::CalcTextSize(owner,nullptr,true);
	const bool 	  	hasText 	= textSize.x > 0;

	/* get the space between element*/
	const float innerSpacing 			= hasText ? ((frame_padding >= 0) 	? frame_padding : (style.ItemInnerSpacing.x)) : 0.f;
	const ImVec2 padding 				= (frame_padding >= 0) 				? ImVec2(frame_padding, frame_padding) : style.FramePadding;

	/* get the size of the whole */
	const ImVec2 totalSizeWithoutPadding(size.x + innerSpacing,size.y > textSize.y ? size.y : textSize.y);

	const ImRect bb(window->DC.CursorPos, window->DC.CursorPos + totalSizeWithoutPadding + padding * 2);
	
	/* Message's position */
	ImVec2 start		= window->DC.CursorPos + padding;
	/* Name's position */
	ImVec2 nameStart 	= start; 

	nameStart.x += 250;
	nameStart.y += 16;

	
	/* get the right position of texts */
	if (size.y < textSize.y) 
		start.y += (textSize.y - size.y) * 0.5f;
	if (size.y < nameSize.y) 
		nameStart.y += (nameSize.y - size.y) * 0.1f;
	if (size.y > nameSize.y) 
		nameStart.y += (size.y - nameSize.y) * 0.1f;


	const ImRect image_bb	(start, start + size);
	start 					= window->DC.CursorPos + padding;
	start.x 				+= innerSpacing;

	start.x += 170;
	

	if (size.y > textSize.y) 
		start.y += (size.y - textSize.y) * 0.7f;
	
	/* add the texts to the thing to display*/
	ImGui::ItemSize(bb);
	if (!ImGui::ItemAdd(bb, id))
		return; 
	
	/* Begin Render */
	const ImU32 col 	= ImGui::GetColorU32(ImGuiCol_WindowBg);
	ImGui::RenderFrame	(bb.Min, bb.Max, col, true, ImClamp(ImMin(padding.x, padding.y), 0.0f, style.FrameRounding));
	
	if (bg_col.w > 0.0f)
		window->DrawList->AddRectFilled(image_bb.Min, image_bb.Max, ImGui::GetColorU32(bg_col));
	
	window->DrawList->AddImage(static_cast<ImTextureID>(&texId), image_bb.Min, image_bb.Max, uv0, uv1, ImGui::GetColorU32(tint_col));

	if (nameSize.x > 0)
		ImGui::RenderText(nameStart,owner);	

	std::string _label = label;
	
	/* if the text is too big wrap line */
	while (textSize.x > 0) 
	{
		int i = 0;
		if (textSize.x <  size.x - innerSpacing - padding.x - start.x)
		{
			ImGui::RenderText(start,_label.c_str());	
			return;
		}

		ImVec2 subTextSize = ImGui::CalcTextSize(_label.substr(0,_label.size() - i).c_str(),nullptr,true);

		while (subTextSize.x > size.x - innerSpacing - padding.x - start.x)
		{
			i++;
			subTextSize	= ImGui::CalcTextSize(_label.substr(0,_label.size() - i).c_str(),nullptr,true);
		}

		ImGui::RenderText(start,_label.substr(0,_label.size() - i).c_str());
		_label = _label.substr(_label.size() - i, _label.size());
		textSize = ImGui::CalcTextSize(_label.c_str(),nullptr,true);
		start.y += innerSpacing + padding.y * 2;
	}
}

