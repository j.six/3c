#include "../include/Enemy.hpp"

using namespace Magnum;

Enemy::Enemy(const Vector3& pos): position_(pos)
{}

void Enemy::engage_fight(Player* p1)
{
    Vector2 distenem(position_.x() - p1->position.x() , position_.z() - p1->position.z());
    float lenght = distenem.length();
    if(rayon >= lenght )
    {
            p1->fight = true;
            inFight_ = true;
    }
}

void Enemy::addMonster(E_FighterType type, float life, float attspeed, float damage, float mana, const std::string& pathname, const std::string& name, GameResourceManager& ressources, Object3D* parent)
{
    switch (type)
    {
    case E_FighterType::Mage :
        monsters_.emplace_back(std::make_unique<FighterMage>(life,attspeed,damage,mana,pathname, name, ressources, parent));
        break;

    case E_FighterType::Warrior :
        monsters_.emplace_back(std::make_unique<FighterWarrior>(life,attspeed,damage,mana,pathname, name, ressources, parent));
        break;

    case E_FighterType::Assassin :
        monsters_.emplace_back(std::make_unique<FighterAssassin>(life,attspeed,damage,mana,pathname, name, ressources, parent));
        break;

    default:
        break;
    }
}
