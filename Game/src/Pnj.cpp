#include "../include/Pnj.hpp"
#include "../include/modelLoader.hpp"

Pnj::Pnj (  const std::string&      name,  
            const Magnum::Vector3& pos,
            const Magnum::Vector3& rot,
            const Magnum::Vector3& scale,
            const std::string&                      pathModel,
            GameResourceManager&                    resources,
            Object3D*                               parent)
    :   position_   {pos},
        rotation_   {rot},
        scale_      {scale},
        models_     {},
        drawables_  {},
        dialogues_  {},
        name_       (name)
{
    addModel(pathModel, resources, parent);
}