#include "../include/modelLoader.hpp"
#include <Magnum/PixelFormat.h>
#include <Magnum/GL/TextureFormat.h>
#include <Magnum/Trade/ImageData.h>
#include <Magnum/Math/Functions.h>
#include <Magnum/MeshTools/Compile.h>
#include <Magnum/Shaders/Shaders.h>
#include <Magnum/Shaders/Phong.h>
#include <Magnum/Trade/MeshData3D.h>
#include <Magnum/Trade/ObjectData3D.h>
#include <Magnum/Trade/MeshObjectData3D.h>
#include <Corrade/Containers/Pointer.h>
#include <Magnum/GL/AbstractShaderProgram.h>
#include <Magnum/Mesh.h>
#include <Magnum/ImageView.h>
#include <Corrade/Utility/Debug.h>

#include "../include/ColoredDrawable.hpp"
#include "../include/TexturedDrawable.hpp"

using namespace Magnum;
using namespace Math::Literals;

std::unique_ptr<Object3D> ModelLoader::loadModel(const std::string&                     pathModel,
                                                GameResourceManager&                    resources,
                                                Magnum::SceneGraph::DrawableGroup3D&    group, 
                                                Object3D*                               parent)
{
    /* Load a scene importer plugin */
    ImporterPlugin manager;
    Importer importer = initLoaderAndGetImporter(manager);

    /* Load file */
    if(!importeDataFromfile(pathModel, importer))
        return nullptr;

    /* Load all textures. Textures that fail to load will be NullOpt. */
    Textures textures = loadTextures(resources, importer);     

    /* Load all materials. Materials that fail to load will be NullOpt. The
    data will be stored directly in objects later, so save them only
    temporarily. */
    Materials materials = loadMaterials(resources, importer);

    /* Load all meshes. Meshes that fail to load will be NullOpt. */
    Meshes meshes = loadMeshes(resources, importer);

    /* Load the scene */
    return std::move(loadScene(resources, textures, meshes, materials, importer, group, parent));

}

Importer ModelLoader::initLoaderAndGetImporter(ImporterPlugin& manager, const std::string& loaderId)
{
    Importer importer = manager.loadAndInstantiate(loaderId);

    if(!importer)
    {
        Warning {} << Debug::boldColor(Debug::Color::Yellow) <<
        "Cannot import pluging \"" << loaderId << "\". Try to load with AnySceneImporter";

        importer = manager.loadAndInstantiate("AnySceneImporter");
        if (!importer)
        {
            Fatal {} << Debug::boldColor(Debug::Color::Red) << 
            "Cannot import pluging \"AnySceneImporter\"";
        }
    }

    return importer;
}

bool ModelLoader::importeDataFromfile(const std::string& pathModel, Importer& importer)
{
    Debug{} << "Opening file" << pathModel;

    if(!importer->openFile(pathModel))
    {
        Error {} << Debug::boldColor(Debug::Color::Red) <<
        "Cannot open file \"" << pathModel << "\", skipping";
        return false;
    }
    return true;
}

Textures ModelLoader::loadTextures(GameResourceManager& resources, Importer& importer)
{
    Textures textureObj {importer->textureCount()};

    for(UnsignedInt i = 0; i != importer->textureCount(); i++)
    {
        std::string textureName = importer->object3DName(importer->defaultScene()) + "_Texture" + std::to_string(i);
        //Debug{} << "Importing texture" << i << textureName;

        Containers::Optional<Trade::TextureData> textureData = importer->texture(i);
        if(!textureData || textureData->type() != Trade::TextureData::Type::Texture2D)
        {
            Warning {} << Debug::boldColor(Debug::Color::Yellow) <<
            "Cannot load texture properties, skipping";
            continue;
        }

        //Debug{} << "Importing image" << textureData->image() << importer->image2DName(textureData->image());

        Containers::Optional<Trade::ImageData2D> imageData = importer->image2D(textureData->image());
        GL::TextureFormat format;
        if(imageData && imageData->format() == PixelFormat::RGB8Unorm)
            format = GL::TextureFormat::RGB8;
        else if(imageData && imageData->format() == PixelFormat::RGBA8Unorm)
            format = GL::TextureFormat::RGBA8;
        else {
            Warning {} << Debug::boldColor(Debug::Color::Yellow) <<
            "Cannot load texture image, skipping";
            continue;
        }

        /* Configure the texture */
        GL::Texture2D texture;
        texture
            .setMagnificationFilter(textureData->magnificationFilter())
            .setMinificationFilter(textureData->minificationFilter(), textureData->mipmapFilter())
            .setWrapping(SamplerWrapping::Repeat)
            .setStorage(Math::log2(imageData->size().max()) + 1, format, imageData->size())
            .setSubImage(0, {}, *imageData)
            .generateMipmap();

        Resource<Containers::Optional<GL::Texture2D>> textureRs;
        if(!(textureRs = resources.get<Containers::Optional<GL::Texture2D>>(textureName)))
        {
            resources.set<Containers::Optional<GL::Texture2D>>(textureRs.key(), std::move(texture));
        }
        else
        {
            //Warning {} << Debug::color(Debug::Color::Yellow) <<
            //"Texture name's \"" << textureName << "\" already exist and cannot be load\n";
        }
        textureObj[i] = resources.get<Containers::Optional<GL::Texture2D>>(textureRs.key());
    }

    return textureObj;
}

//TODO:Add resource manager with materials
Materials ModelLoader::loadMaterials(GameResourceManager& resources, Importer& importer)
{
    Materials materials{importer->materialCount()};

    for(UnsignedInt i = 0; i != importer->materialCount(); ++i)
    {
        //Debug{} << "Importing material" << i << importer->materialName(i);

        Containers::Pointer<Trade::AbstractMaterialData> materialData = importer->material(i);
        if(!materialData || materialData->type() != Trade::MaterialType::Phong)
        {
           // Warning {} << Debug::boldColor(Debug::Color::Yellow) <<
           // "Cannot load material, skipping";
            continue;
        }

        materials[i] = std::move(static_cast<Trade::PhongMaterialData&>(*materialData));
    }

    return materials;
}

Meshes ModelLoader::loadMeshes(GameResourceManager& resources, Importer& importer)
{
    Meshes meshObj {importer->mesh3DCount()};

    for(UnsignedInt i = 0; i != importer->mesh3DCount(); ++i)
    {
        std::string meshName = importer->object3DName(importer->defaultScene()) + "_Mesh" + std::to_string(i);
        //Debug{} << "Importing mesh" << i << meshName;

        Containers::Optional<Trade::MeshData3D> meshData = importer->mesh3D(i);

        if(!meshData || !meshData->hasNormals() || meshData->primitive() != MeshPrimitive::Triangles)
        {
           // Warning {} << Debug::boldColor(Debug::Color::Yellow) <<
           // "Cannot load the mesh, skipping";
            continue;
        }

        /* Compile the mesh */
        Resource<GL::Mesh> meshRs;
        if(!(meshRs = resources.get<GL::Mesh>(meshName)))
        {
            resources.set<GL::Mesh>(meshRs.key(), MeshTools::compile(*meshData));
        }
        else
        {
           // Warning {} << Debug::color(Debug::Color::Yellow) <<
           // "Mesh name's \"" << meshName << "\" already exist and cannot be load\n";
        }
        meshObj[i] = resources.get<GL::Mesh>(meshRs.key());
    }

    return meshObj;
}

std::unique_ptr<Object3D> ModelLoader::loadScene(   GameResourceManager&        resources,
                                                    Textures&                   textures,
                                                    Meshes&                     meshes,
                                                    Materials&                  materials,
                                                    Importer&                   importer,
                                                    Magnum::SceneGraph::DrawableGroup3D&    group, 
                                                    Object3D*                               parent)
{
    std::unique_ptr<Object3D> pManipulator = std::make_unique<Object3D>();

    if (parent != nullptr)
        pManipulator->setParent(parent);

    if(importer->defaultScene() != -1)
    {
        //Debug{} << Debug::boldColor(Debug::Color::Blue) << "Adding default scene" << importer->sceneName(importer->defaultScene());

        Containers::Optional<Trade::SceneData> sceneData = importer->scene(importer->defaultScene());
        if(!sceneData)
        {
            Error{} << Debug::boldColor(Debug::Color::Red) <<
            "Cannot load scene, exiting";
            return nullptr;
        }
        
        /* Recursively add all children */
        for(UnsignedInt objectId: sceneData->children3D())
        {
            addObject(resources, importer, textures, meshes, materials, group, *pManipulator.get(), objectId);
        }

    /* The format has no scene support, display just the first loaded mesh with
    a default material and be done with it */
    }
    else if(!meshes.empty())
    {
        Resource<Containers::Pointer<GL::AbstractShaderProgram>> shader;
        if(!(shader = resources.get<Containers::Pointer<GL::AbstractShaderProgram>>("ColoredShader")))
        {
            /*If colored shader dosn't exist, create it*/
            Containers::Pointer<Shaders::Phong> coloredShader = std::make_unique<Shaders::Phong>();
            coloredShader
                ->setAmbientColor(0x111111_rgbf)
                .setSpecularColor(0xffffff_rgbf)
                .setShininess(80.0f);

            resources.set<Magnum::Containers::Pointer<Magnum::GL::AbstractShaderProgram>>(shader.key(), std::move(coloredShader));
            shader = resources.get<Containers::Pointer<GL::AbstractShaderProgram>>(shader.key());
        }

        new ColoredDrawable{*pManipulator.get(), *static_cast<Shaders::Phong*>((*shader).get()), *meshes[0], 0xffffffff_rgbaf, group};
    }
    else
    {
        Warning {} << Debug::boldColor(Debug::Color::Yellow) <<
        "File not loaded\n";
        pManipulator = nullptr;
    }

    return std::move(pManipulator);
}

void ModelLoader::addObject(    GameResourceManager& ressourcesManager,
                                Importer& importer,
                                Textures& textures,
                                Meshes& meshes,
                                Materials& materials,
                                Magnum::SceneGraph::DrawableGroup3D&    group,
                                Object3D& parent,
                                Magnum::UnsignedInt idObject)
{
    //Debug{} << "Importing object" << idObject << importer->object3DName(idObject);

    Containers::Pointer<Trade::ObjectData3D> objectData = importer->object3D(idObject);
    GL::Mesh& mesh = *ressourcesManager.get<GL::Mesh>(meshes[objectData->instance()].key());

    if(!objectData) {
        Warning{} << Debug::boldColor(Debug::Color::Yellow) << "Cannot import object, skipping";
        return;
    }

    /* Add the object to the scene and set its transformation */
    auto* object = new Object3D{&parent};
    object->setTransformation(objectData->transformation());

    /*Get shader resource and create it if it dosn't exist*/
    Resource<Containers::Pointer<GL::AbstractShaderProgram>> coloredShaderRessource;
    if(!(coloredShaderRessource = ressourcesManager.get<Containers::Pointer<GL::AbstractShaderProgram>>("ColoredShader")))
    {
        /*If colored shader dosn't exist, create it*/
        Containers::Pointer<Shaders::Phong> coloredShader = std::make_unique<Shaders::Phong>();
        coloredShader
            ->setAmbientColor(0x111111_rgbf)
            .setSpecularColor(0xffffff_rgbf)
            .setShininess(80.0f);

        ressourcesManager.set<Containers::Pointer<GL::AbstractShaderProgram>>(coloredShaderRessource.key(), std::move(coloredShader));
        coloredShaderRessource = ressourcesManager.get<Containers::Pointer<GL::AbstractShaderProgram>>(coloredShaderRessource.key());
    }

    Resource<Containers::Pointer<GL::AbstractShaderProgram>> texturedShaderRessource;
    if(!(texturedShaderRessource = ressourcesManager.get<Containers::Pointer<GL::AbstractShaderProgram>>("TexturedShader")))
    {
        /*If colored shader dosn't exist, create it*/
        Containers::Pointer<Shaders::Phong> texturedShader = std::make_unique<Shaders::Phong>(Shaders::Phong::Flag::DiffuseTexture);
        texturedShader
            ->setAmbientColor(0x111111_rgbf)
            .setSpecularColor(0x111111_rgbf)
            .setShininess(80.0f);

        ressourcesManager.set<Containers::Pointer<GL::AbstractShaderProgram>>(texturedShaderRessource.key(), std::move(texturedShader));
        texturedShaderRessource = ressourcesManager.get<Containers::Pointer<GL::AbstractShaderProgram>>(texturedShaderRessource.key());
    }

    /* Add a drawable if the object has a mesh and the mesh is loaded */
    if(objectData->instanceType() == Trade::ObjectInstanceType3D::Mesh && objectData->instance() != -1 && (&mesh))
    {
        const Int materialId = static_cast<Trade::MeshObjectData3D*>(objectData.get())->material();

        /* Material not available / not loaded, use a default material */
        if(materialId == -1 || !materials[materialId])
        {
            Error{} << Debug::boldColor(Debug::Color::Red) <<
            "Cannot import material";
            
            new ColoredDrawable{*object, *static_cast<Shaders::Phong*>((*coloredShaderRessource).get()), mesh, 0xffffff_rgbf, group};

        /* Textured material. If the texture failed to load, again just use a
        default colored material. */
        }
        else if(materials[materialId]->flags() & Trade::PhongMaterialData::Flag::DiffuseTexture)
        {            
            Containers::Optional<GL::Texture2D>& texture = (*ressourcesManager.get<Containers::Optional<GL::Texture2D>>(textures[materials[materialId]->diffuseTexture()].key())); 

            if(texture)
                new TexturedDrawable{*object, *static_cast<Shaders::Phong*>((*texturedShaderRessource).get()), mesh, *texture, group};
            else
                new ColoredDrawable{*object, *static_cast<Shaders::Phong*>((*coloredShaderRessource).get()), mesh, 0xffffff_rgbf, group};

        /* Color-only material */
        } else {
            new ColoredDrawable{*object, *static_cast<Shaders::Phong*>((*coloredShaderRessource).get()), mesh, materials[materialId]->diffuseColor(), group};
        }
    }

    /* Recursively add children */
    for(std::size_t id: objectData->children())
        addObject(ressourcesManager, importer, textures, meshes, materials, group, parent, idObject);
}
