#include "../include/Chest.hpp"
#include <iostream>

void Chest::player_in_zone(Player& player,bool openit,GameResourceManager& ressources,Pi::PlayerInterface& textbox)
{
    Magnum::Vector2 distenem(position.x() - player.position.x() , position.z() - player.position.z());
    float lenght = distenem.length();
    if(rayon >= lenght )
    {
        std::vector<std::string> str;
        str.push_back ("Press 'e' to interact with chest."); 
        textbox.PnjTextBox(&ressources,"Chest",str);

        if(openit == true)
        {    

            textbox.get("Chest").close();

            if(moneyin == 0)
            {
                textbox.get("Chest ").close();
                std::vector<std::string> str;
                str.push_back ("This chest is empty !"); 
                textbox.PnjTextBox(&ressources,"Chest  ",str);
            
            }
            if(moneyin > 0)
            {
                std::vector<std::string> str;
                str.push_back ("You find bear recipe");
                textbox.PnjTextBox(&ressources,"Chest ",str);
                player.money += moneyin;
                moneyin -= moneyin;
            }
            
        }

        return;
        
    }

    textbox.stop("Chest");
    textbox.stop("Chest ");
    textbox.stop("Chest  ");
}
