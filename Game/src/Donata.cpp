#include "../include/Donata.hpp"
#include <iostream>
#include "../include/modelLoader.hpp"
#include "../include/World/City.hpp"
#include "../include/World/House.hpp"

using namespace Magnum;
using namespace Math::Literals;
using namespace Pi;


constexpr int 		WindowWidth  = 1440;
constexpr int 		WindowHeight = 900;

//========================= CONSTRUCTORS ===================================


Donata::Donata		(const Arguments& arguments)
	:
		/* Set the Window and the SDL */
       	Platform::Application	{arguments, NoCreate},
		PI_ 					{NoCreate},
		resourceManager_		(),
		deltaTime_				{NoCreate}
{
    {
        const Vector2 dpiScaling = this->dpiScaling({});
        Configuration conf;
        conf.setTitle("Donata")
            .setSize({WindowWidth, WindowHeight}, dpiScaling)
			.setWindowFlags(Magnum::Platform::Sdl2Application::Configuration::WindowFlag::Fullscreen);
        GLConfiguration glConf;
        glConf.setSampleCount(dpiScaling.max() < 2.0f ? 8 : 2);
        if(!tryCreate(conf, glConf))
            create(conf, glConf.setSampleCount(0));
    }

	/*setting configuration*/
	GL::Renderer::setClearColor(0xa5c9ea_rgbf);
    SDL_ShowCursor(false);
    SDL_SetRelativeMouseMode(SDL_TRUE);

	PI_ 		= std::move(PlayerInterface(windowSize(), framebufferSize(), dpiScaling())); 
	deltaTime_	= std::move(DeltaTime());

	PI_.StartMenu(&resourceManager_,"Start");
	
	// =========================================== //

    /* renderer setup */
    GL::Renderer::enable(GL::Renderer::Feature::DepthTest);
    GL::Renderer::enable(GL::Renderer::Feature::FaceCulling);

    /*initialize rand*/
    std::srand(std::time(nullptr));

    /* Player */ 
    {
		player.drawables.emplace_back();
        player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk1.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
		player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk2.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
        player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk3.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
        player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk4.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
        player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk5.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
		player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk6.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
        player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk7.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
		player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk8.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
        player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk9.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
		player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk10.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
		player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk11.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
        player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk12.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
		player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk13.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
        player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk14.obj", resourceManager_, player.drawables.back(), &_scene)));
		player.drawables.emplace_back();
		player.models.emplace_back(std::move(ModelLoader::loadModel("Game/resources/animateobj/newwalk/walk15.obj", resourceManager_, player.drawables.back(), &_scene)));
		
	}

    // =========================================== //

    /* Set up the camera */
    _cameraObject = new Object3D{&_scene};
    (*_cameraObject)
        .translate(Vector3::zAxis(5.0f))
        .rotateX(-15.0_degf)
        .rotateY(30.0_degf);
    _camera = new SceneGraph::Camera3D{*_cameraObject};
    _camera->setProjectionMatrix(Matrix4::perspectiveProjection(
        60.0_degf, Vector2{windowSize()}.aspectRatio(), 0.01f, 100.0f));

    // ===================== Hero of player ====================== //
    player.addHero(E_FighterType::Assassin, 750,1,10,2,"Game/resources/meshes/leonfight.obj", "Ansem", resourceManager_, &_scene);
    player.addHero(E_FighterType::Mage, 500, 1, 10, 20,"Game/resources/meshes/waitgrandma.obj", "Kairis", resourceManager_, &_scene);
    player.addHero(E_FighterType::Warrior, 1000,1,10,12,"Game/resources/meshes/leonfight.obj","Leon", resourceManager_, &_scene);

    currentWorld_ = std::make_unique<City>(resourceManager_, player, *_cameraObject, *_camera , PI_);
}

Donata::~Donata		()
{  
}

//========================== INPUT METHODS ====================================

void Donata::viewportEvent(ViewportEvent& event) 
{
	/* change window's size */
	GL::defaultFramebuffer.setViewport({{}, event.framebufferSize()});

	/* scale the UI's size by the same scale */
	PI_.relayout(Vector2{event.windowSize()}/event.dpiScaling(),
			event.windowSize(), event.framebufferSize());
}

void Donata::keyPressEvent(KeyEvent& event)
{
    if(currentWorld_ != nullptr && camerafree_ == false)
    {
        currentWorld_->keyPressEvent(event);
    }
    
    redraw();

    // ============= Camera =============//
    if(camerafps_ == false && camera3p_ == false)
    {
        if(event.key() == KeyEvent::Key::F5)
            camerafree_ = !camerafree_;
    }
    if(camerafree_ == false && camera3p_ == false)
    {
        if(event.key() == KeyEvent::Key::F6)
            camerafps_ = !camerafps_;
    }
    if(camerafree_ == false && camerafps_ == false)
    {
        if(event.key() == KeyEvent::Key::F7)
            camera3p_ = !camera3p_;
    }
    if( camerafree_ == true || camerafps_ == true || camera3p_ == true)
    {
        cam.camera_pressinput(event);
    }

    // ============= ======= =============//

    //if(player.fight == true)
    //{
    //    Debug{} << "bonjour tu fight";
    //    arena->input(event);
    //}
    
	
    /* User's control effect */
    if((player.fight == false && camerafree_ == false)) 
    {
	    switch (event.key())
	    {
        
		case KeyEvent::Key::W :
	        mouse[0] = true;
			break;
		case KeyEvent::Key::S :
	        mouse[1] = true;

			break;
		case KeyEvent::Key::A :
	        mouse[2] = true;
			break;
		case KeyEvent::Key::D :
	        mouse[3] = true;
			break;
		case KeyEvent::Key::Z :
			player.fight = false;
			player.position = {-5,0.5,-5};
			for (auto &&model : player.models)
            {
				model->resetTransformation();
				model->scale(player.scale_).translate(player.position);
				break;
			}
        
		default:
				break;
        }
	}

		event.setAccepted();
		redraw();
		/* Quit the Donata brutally */
    if(event.key() == KeyEvent::Key::Esc)
	{
        std::exit(0);
    }
    	
    if(PI_.handleKeyPressEvent(event)) 
		return;
}


void Donata::keyReleaseEvent(KeyEvent& event) 
{

    if(currentWorld_ != nullptr && camerafree_ == false )
    {
        currentWorld_->keyReleaseEvent(event);
    }
    
    if(player.fight == false && camerafree_ == false)
    {

		switch (event.key())
		{
			case KeyEvent::Key::W :
		        mouse[0] = false;
				break;
			case KeyEvent::Key::S :
		        mouse[1] = false;
				break;
			case KeyEvent::Key::A :
		        mouse[2] = false;
				break;
			case KeyEvent::Key::D :
				mouse[3] = false;
			break;
			default:
			break;

		}
    }

    // ============= Camera =============//

    if(camerafree_ == true || camerafps_ == true)
    {
        cam.camera_realeaseinput(event);
    }
    
    // ============= ====== =============//

	if(PI_.handleKeyReleaseEvent(event))
	   	return;

    event.setAccepted();
	redraw();
}

void Donata::mousePressEvent(MouseEvent& event)
{
    if(currentWorld_ != nullptr)
    {
        currentWorld_->mousePressEvent(event);
    }

	if(PI_.handleMousePressEvent(event))
	  	return;
	
}

void Donata::mouseReleaseEvent(MouseEvent& event)
{

    if(currentWorld_ != nullptr)
    {
        currentWorld_->mouseReleaseEvent(event);
    }
   
	PI_.next();

	if(PI_.handleMouseReleaseEvent(event)) 
		return;
	
}

void Donata::mouseMoveEvent(MouseMoveEvent& event) 
{
    if(camerafree_ == true || camerafps_ == true) 
    {   
        cam.mouse_camera(event);
    }

    if(currentWorld_ != nullptr)
    {
        currentWorld_->mouseMoveEvent(event);
    }
    

	if(PI_.handleMouseMoveEvent(event))
		return;
	
}

void Donata::mouseScrollEvent(MouseScrollEvent& event) 
{

    if(currentWorld_ != nullptr)
    {
        currentWorld_->mouseScrollEvent(event);
	}
           

	if(PI_.handleMouseScrollEvent(event)) 
	{
		/* Prevent scrolling the page */
		event.setAccepted();
		return;
	}
}

void Donata::textInputEvent(TextInputEvent& event)
{
	if(PI_.handleTextInputEvent(event))
		return;
}

void Donata::resetKey()
{
    for(int i = 0 ; i < 4 ; i++)
    {
        mouse[i] = false;
    }
}


//========================= DISPLAY METHODS ===================================

void 	Donata::setToDraw		()
{
	/* choose the Display Methods depending on the context */
	switch (context_)
	{
		case Context::START	:
			drawStart();
			break;
		case Context::RUNNING :
			if(currentWorld_ != nullptr)
            {
                GL::Renderer::enable(GL::Renderer::Feature::FaceCulling);
                GL::Renderer::setClearColor(0xa5c9ea_rgbf);
                currentWorld_->draw();
				PI_.draw(*this, *deltaTime_);
                SDL_SetRelativeMouseMode(SDL_TRUE);
            }
			break;
		default:
			break;
	}
}

void	Donata::drawStart		()
{
	PI_.draw(*this, *deltaTime_);

	if (PI_.get("PlayButton").active())
	{
		PI_.clear();

		context_ = Context::RUNNING;
	}
	if (PI_.get("ExitButton").active())
	{
		exit();
	}
	redraw();
}

// ============================= Tick Event ============================= //

void Donata::tickEvent()
{
	deltaTime_.update();
	if (context_ == Context::RUNNING)
	{
    	if(currentWorld_ != nullptr)
    	{
    	    currentWorld_->execut(deltaTime_.get());
    	}

    	// ============= Camera =============//

    	if(camerafree_ == true) // camera to debug : type freefly
    	{
    	    cam.camerafreefly();
    	    _cameraObject->resetTransformation();
    	    _cameraObject->rotateX(static_cast<Rad>(cam.pitch)).rotateY(static_cast<Rad>(-cam.yaw));
    	    _cameraObject->translate(cam.cameraPos);
    	}
    	if(camerafps_ == true)
    	{
    	    cam.camerafps();
    	    cam.cameraPos = player.position ;
    	    
			for (auto &&model : player.models)
            {
				model->resetTransformation();
				model->scale(player.scale_);
				model->rotateY(static_cast<Rad>(-cam.yaw + static_cast<float>(M_PI)));
				model->translate(cam.cameraPos);
			}

    	    cam.cameraPos.y() += 2.3f;
    	    _cameraObject->resetTransformation();
    	    _cameraObject->rotateX(static_cast<Rad>(cam.pitch)).rotateY(static_cast<Rad>(-cam.yaw));
    	    _cameraObject->translate(cam.cameraPos);
    	}
	}
    redraw();
}


// ============================= Draw Event ============================= //

void 	Donata::drawEvent		()
{
      /* clear the Buffer */ 
	GL::defaultFramebuffer.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

	/* Choose the thing to draw depending on the context */
	setToDraw();
    
	/* display */
	swapBuffers();
}


MAGNUM_APPLICATION_MAIN(Donata)
