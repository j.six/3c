#include "../include/Fighter.hpp"
#include "../include/tools.hpp"

using namespace Magnum;

Fighter::Fighter(float life, float attSpeed, float damage,float mana, const std::string& pathname, const std::string& name, GameResourceManager& ressources, Object3D* parent) 
{
	name_		= name;
    life_ 		= life;
    attSpeed_ 	= attSpeed;
    damage_ 	= damage;
    mana_ 		= mana;
    initiative_ = ranValueLimite(0, 100);
    pathname_ 	= pathname;
    moveSpeed_ = 1.f;

    modelfighter_ = std::move(ModelLoader::loadModel(pathname_.c_str(), ressources, drawable_, parent));
    modelfighter_->resetTransformation();
    modelfighter_->scale({0.01,0.01,0.01}).rotateY(static_cast<Magnum::Rad>(rotatey)).translate(position_);
}

void Fighter::attack_fighter(Fighter& fighter)
{
    fighter.life_ -= damage_;
}

/*
void Fighter::loadFighter(GameResourceManager& ressources, Scene3D& scene_)
{
    modelfighter_ = std::move(ModelLoader::loadModel(pathname_.c_str(), ressources, drawable_, &scene_));
    modelfighter_->resetTransformation();
    modelfighter_->scale({0.01,0.01,0.01}).rotateY(static_cast<Magnum::Rad>(rotatey)).translate(position_);
}*/

void Fighter::rotate_Y(float rad)
{
    rotatey = rad;
}

void Fighter::moveForward    (float detlaTime)
{
    position_ += direction_ * moveSpeed_* detlaTime;
    modelfighter_->translate(direction_ * moveSpeed_ * detlaTime);
}

void Fighter::moveBackward   (float detlaTime)
{
    position_ -= direction_ * moveSpeed_ * detlaTime;
    modelfighter_->translate(-direction_ * moveSpeed_ * detlaTime);
}

void Fighter::moveLeft       (float detlaTime)
{
    Vector3 dirOrtho(-direction_.z(), direction_.y(), direction_.x());
    position_ -= dirOrtho * moveSpeed_ * detlaTime;
    modelfighter_->translate(-dirOrtho * moveSpeed_ * detlaTime);
}

void Fighter::moveRight      (float detlaTime)
{
    Vector3 dirOrtho(-direction_.z(), direction_.y(),direction_.x());
    position_ += dirOrtho * moveSpeed_ * detlaTime;
    modelfighter_->translate(dirOrtho * moveSpeed_ * detlaTime);
}

void Fighter::turnLeft       (float deltaTime)
{
    rotateY(-static_cast<float>(M_PI) * deltaTime);
}

void Fighter::turnRight      (float deltaTime)
{
    rotateY(static_cast<float>(M_PI) * deltaTime);
}

// ====================================== Mage ====================================== //

FighterMage::FighterMage(float life, float attSpeed, float damage,float mana, const std::string& pathname, const std::string& name, GameResourceManager& ressources, Object3D* parent)
    : Fighter (life, attSpeed, damage,  mana, pathname, name, ressources, parent)
{
    capacities_.emplace_back(S_Capacity{"Basic attack", 0.5f, 2.5f, 0.f});
    capacities_.emplace_back(S_Capacity{"Give mana", 5.f, 1.f, 1.f});
    capacities_.emplace_back(S_Capacity{"Give health", 5.f, 1.f, 1.f});
    capacities_.emplace_back(S_Capacity{"Ultim Attack", 7.f, 1.5f, 1.f});

    moveSpeed_ = 2.f;
}

void FighterMage::attack1(Fighter& fighter)
{
    const int indexCapa = 0;

    if(mana_ < capacities_[indexCapa].manaCost)
    {
        Debug {} << "Not enought mana";
        return;
    }

    if((fighter.position_ - position_).length() > capacities_[indexCapa].range)
    {
        Debug {} << (fighter.position_ - position_).length() << " Cannot attack at this distance";
        return;
    }

    Debug {} << "Fighter use capacity \"" << capacities_[indexCapa].name << "\"";
    capacityUse     = indexCapa;
    delayAttack_    = capacities_[indexCapa].timeDelay;
    mana_           -= capacities_[indexCapa].manaCost;
    fighter.life_   -= damage_;
}

void FighterMage::attack2(Fighter& fighter)
{
    const int indexCapa = 1;

    if(mana_ < capacities_[indexCapa].manaCost)
    {
        Debug {} << "Not enought mana";
        return;
    }

    if((fighter.position_ - position_).length() > capacities_[indexCapa].range)
    {
        Debug {} << (fighter.position_ - position_).length() << " Cannot attack at this distance";
        return;
    }

    Debug {} << "Fighter use capacity \"" << capacities_[indexCapa].name << "\"";
    capacityUse     = indexCapa;
    delayAttack_    = capacities_[indexCapa].timeDelay;
    mana_           -= capacities_[indexCapa].manaCost;
    fighter.mana_   += 10.f;
}

void FighterMage::attack3(Fighter& fighter)
{
    const int indexCapa = 2;

    if(mana_ < capacities_[indexCapa].manaCost)
    {
        Debug {} << "Not enought mana";
        return;
    }

    if((fighter.position_ - position_).length() > capacities_[indexCapa].range)
    {
        Debug {} << (fighter.position_ - position_).length() << " Cannot attack at this distance";
        return;
    }

    Debug {} << "Fighter use capacity \"" << capacities_[indexCapa].name << "\"";
    capacityUse = indexCapa;
    delayAttack_= capacities_[indexCapa].timeDelay;
    mana_      -= capacities_[indexCapa].manaCost;
    fighter.life_ += 20;
}

void FighterMage::attack4(Fighter& fighter)
{
    const int indexCapa = 3;

    if(mana_ < capacities_[indexCapa].manaCost)
    {
        Debug {} << "Not enought mana";
        return;
    }
    
    if((fighter.position_ - position_).length() > capacities_[indexCapa].range)
    {
        Debug {} << (fighter.position_ - position_).length() << " Cannot attack at this distance";
        return;
    }

    Debug {} << "Fighter use capacity \"" << capacities_[indexCapa].name << "\"";
    capacityUse     = indexCapa;
    delayAttack_    = capacities_[indexCapa].timeDelay;
    mana_           = 0;
    fighter.life_   -= damage_ * 2;
}

// ====================================== Warrior ====================================== //

FighterWarrior::FighterWarrior(float life, float attSpeed, float damage,float mana, const std::string& pathname, const std::string& name, GameResourceManager& ressources, Object3D* parent)
    : Fighter (life, attSpeed, damage,  mana, pathname, name, ressources, parent)
{
    capacities_.emplace_back(S_Capacity{"Basic attack", 0.5f, 2.5f, 0.f});
    capacities_.emplace_back(S_Capacity{"Attack and grab enemy", 5.f, 3.f, 1.f});
    capacities_.emplace_back(S_Capacity{"Attack and move enemy", 2.f, 3.f, 1.f});
    capacities_.emplace_back(S_Capacity{"Ultim Attack", 2.f, 3.f, 3.f});

    moveSpeed_ = 0.5f;
}

void FighterWarrior::attack1(Fighter& fighter)
{
    const int indexCapa = 0;

    if(mana_ < capacities_[indexCapa].manaCost)
    {
        Debug {} << "Not enought mana";
        return;
    }

    if((fighter.position_ - position_).length() > capacities_[indexCapa].range)
    {
        Debug {} << (fighter.position_ - position_).length() << " Cannot attack at this distance";
        return;
    }

    Debug {} << "Fighter use capacity \"" << capacities_[indexCapa].name << "\"";
    capacityUse     = indexCapa;
    delayAttack_    = capacities_[indexCapa].timeDelay;
    mana_           -= capacities_[indexCapa].manaCost;
    fighter.life_   -= damage_;
}

void FighterWarrior::attack2(Fighter& fighter)
{
    const int indexCapa = 1;

    if(mana_ < capacities_[indexCapa].manaCost)
    {
        Debug {} << "Not enought mana";
        return;
    }

    if((fighter.position_ - position_).length() > capacities_[indexCapa].range)
    {
        Debug {} << (fighter.position_ - position_).length() << " Cannot attack at this distance";
        return;
    }

    Debug {} << "Fighter use capacity \"" << capacities_[indexCapa].name << "\"";
    capacityUse     = indexCapa;
    delayAttack_    = capacities_[indexCapa].timeDelay;
    mana_           -= capacities_[indexCapa].manaCost;
    fighter.life_   -= damage_;
}

void FighterWarrior::attack3(Fighter& fighter)
{
    const int indexCapa = 2;

    if(mana_ < capacities_[indexCapa].manaCost)
    {
        Debug {} << "Not enought mana";
        return;
    }

    if((fighter.position_ - position_).length() > capacities_[indexCapa].range)
    {
        Debug {} << (fighter.position_ - position_).length() << " Cannot attack at this distance";
        return;
    }

    Debug {} << "Fighter use capacity \"" << capacities_[indexCapa].name << "\"";
    capacityUse     = indexCapa;
    delayAttack_    = capacities_[indexCapa].timeDelay;
    mana_           -= capacities_[indexCapa].manaCost;
    fighter.life_   -= damage_ + 10.f;
    fighter.translate((position_ - fighter.position_).normalized() * 3.5f);
}

void FighterWarrior::attack4(Fighter& fighter)
{
    const int indexCapa = 3;

    if(mana_ < capacities_[indexCapa].manaCost)
    {
        Debug {} << "Not enought mana";
        return;
    }

    if((fighter.position_ - position_).length() > capacities_[indexCapa].range)
    {
        Debug {} << (fighter.position_ - position_).length() << " Cannot attack at this distance";
        return;
    }

    Debug {} << "Fighter use capacity \"" << capacities_[indexCapa].name << "\"";
    capacityUse     = indexCapa;
    delayAttack_    = capacities_[indexCapa].timeDelay;
    mana_           = 0.f;
    fighter.life_   -= damage_ * 2;
}

// ====================================== Assassin ====================================== //

FighterAssassin::FighterAssassin(float life, float attSpeed, float damage,float mana, const std::string& pathname, const std::string& name, GameResourceManager& ressources, Object3D* parent)
    : Fighter (life, attSpeed, damage,  mana, pathname, name, ressources, parent)
{
    capacities_.emplace_back(S_Capacity{"Basic attack", 0.5f, 2.5f, 0.f});
    capacities_.emplace_back(S_Capacity{"Multiple degas", 5.f, 2.5f, 2.f});
    capacities_.emplace_back(S_Capacity{"Teleport toward target", 2.f, 10.f, 2.f});
    capacities_.emplace_back(S_Capacity{"Ultim attack", 2.f, 3.f, 3.f});

    moveSpeed_ = 4.f;
}

void FighterAssassin::attack1(Fighter& fighter)
{
    const int indexCapa = 0;
 
    if(mana_ < capacities_[indexCapa].manaCost)
    {
        Debug {} << "Not enought mana";
        return;
    }

    if((fighter.position_ - position_).length() > capacities_[indexCapa].range)
    {
        Debug {} << (fighter.position_ - position_).length() << " Cannot attack at this distance";
        return;
    }

    Debug {} << "Fighter use capacity \"" << capacities_[indexCapa].name << "\"";
    capacityUse     = indexCapa;
    delayAttack_    = capacities_[indexCapa].timeDelay;
    mana_           -= capacities_[indexCapa].manaCost;
    fighter.life_   -= damage_;
}

void FighterAssassin::attack2(Fighter& fighter)
{
    const int indexCapa = 1;

    if(mana_ < capacities_[indexCapa].manaCost)
    {
        Debug {} << "Not enought mana";
        return;
    }

    if((fighter.position_ - position_).length() > capacities_[indexCapa].range)
    {
        Debug {} << (fighter.position_ - position_).length() << " Cannot attack at this distance";
        return;
    }

    Debug {} << "Fighter use capacity \"" << capacities_[indexCapa].name << "\"";
    capacityUse     = indexCapa;
    delayAttack_    = capacities_[indexCapa].timeDelay;
    mana_           -= capacities_[indexCapa].manaCost;
    fighter.life_   -= damage_ + 5;
}

void FighterAssassin::attack3(Fighter& fighter)
{
    const int indexCapa = 2;

    if(mana_ < capacities_[indexCapa].manaCost)
    {
        Debug {} << "Not enought mana";
        return;
    }

    if((fighter.position_ - position_).length() > capacities_[indexCapa].range)
    {
        Debug {} << (fighter.position_ - position_).length() << " Cannot attack at this distance";
        return;
    }

    Debug {} << "Fighter use capacity \"" << capacities_[indexCapa].name << "\"";
    capacityUse     = indexCapa;
    delayAttack_    = capacities_[indexCapa].timeDelay;
    mana_           -= capacities_[indexCapa].manaCost;
    translate((fighter.position_ - position_) * 0.9f);
}

void FighterAssassin::attack4(Fighter& fighter)
{
    const int indexCapa = 3;

    if(mana_ < capacities_[indexCapa].manaCost)
    {
        Debug {} << "Not enought mana";
        return;
    }

    if((fighter.position_ - position_).length() > capacities_[indexCapa].range)
    {
        Debug {} << (fighter.position_ - position_).length() << " Cannot attack at this distance";
        return;
    }

    Debug {} << "Fighter use capacity \"" << capacities_[indexCapa].name << "\"";
    capacityUse     = indexCapa;
    delayAttack_    = capacities_[indexCapa].timeDelay;
    mana_           = 0;
    fighter.life_ -= damage_ * 3;
}
