#include "../include/World/City.hpp"
#include "../include/modelLoader.hpp"

#include <Magnum/Math/Angle.h>
#include "../include/tools.hpp"
#include <algorithm>

using namespace Magnum;
using namespace Math::Literals;
using namespace Magnum::Platform;

#define 	TURN_DURATION 	10.0f
const char* TimeCounterName = "Time: ";

Arena::Arena (GameResourceManager& ressources, Player& player, Object3D& cameraObject, Magnum::SceneGraph::Camera3D& camera, 
				Pi::PlayerInterface& playerInterface)
    :   World           (player, cameraObject, camera, playerInterface, ressources),
        pMap_           {nullptr},
		figthZone_		{	Zone(	{-10, -10, 10},
									{20, 20, -20})},
		enemy_          				{nullptr},
		arenaContext_					{E_ArenaContext::arriveCinematic},
		rotationCameraCinematicArrive_	{0.f},
		posCameraCinematicArrive_		{18.f, 21.f, 21.f},
		lerpStepForZoomAnimation_		{0.f},
		ressources_						{ressources},
		fighterOrder_					{},
		fighterFocusByPlayer_			{nullptr},
		stepFighterFocusByPlayer_		{0},
		countTurn						{0}
{
    initializeMap(ressources);
}

void Arena::initializeMap(GameResourceManager& ressources)
{
    /*Initialize map model*/
    pMap_ = std::move(ModelLoader::loadModel("Game/resources/meshes/park.obj", ressources, drawables_, &scene_));
	pMap_->scale({0.1f,0.1f,0.1f});
}

void Arena::execut             (float deltaTime)
{
	if (!playerInterface_.has(TimeCounterName))
		playerInterface_.TimeDisp(&rs_,TimeCounterName,timeToattack,{0.25f,0.0f});	

	if(arenaContext_ == E_ArenaContext::arriveCinematic)
	{
		if(rotationCameraCinematicArrive_ < 270.f)
		{
			float exDirX = posCameraCinematicArrive_.x();
			float camAngle = 50.f * deltaTime;
			float cosR = cosf(camAngle * static_cast<float>(M_PI) / 180.f);
			float sinR = sinf(camAngle * static_cast<float>(M_PI) / 180.f);

			posCameraCinematicArrive_.x() = exDirX * cosR - posCameraCinematicArrive_.z() * sinR;
			posCameraCinematicArrive_.z() = exDirX * sinR + posCameraCinematicArrive_.z() * cosR;

			cameraObject_.setTransformation(Magnum::Matrix4::lookAt(posCameraCinematicArrive_, {0.f, 0.f, 0.f}, {0.f, 1.f, 0.f}));

			rotationCameraCinematicArrive_ += camAngle;
		} else
		{
			if(lerpStepForZoomAnimation_ < 1.0f)
			{
				cameraObject_.setTransformation(Magnum::Matrix4::lookAt(lerp(posCameraCinematicArrive_, fighterOrder_[0].fighter->position_ + Magnum::Vector3::yAxis(3.f), lerpStepForZoomAnimation_), {0.f, 0.f, 0.f}, {0.f, 1.f, 0.f}));
				lerpStepForZoomAnimation_ += 0.01f;	// Add deltaTime
			}
			else
			{
				rotationCameraCinematicArrive_ = 0.f;
				lerpStepForZoomAnimation_ = 0.f;
				arenaContext_ = E_ArenaContext::battle;
			}
		}
	}
	else if (arenaContext_ == E_ArenaContext::battle || arenaContext_ == E_ArenaContext::playerTurn || arenaContext_ == E_ArenaContext::enemyTurn)
	{
		for (auto &&fighter : fighterOrder_)
		{
			fighter.fighter->execut(deltaTime);
		}

		cameraObject_.setTransformation(Magnum::Matrix4::lookAt(fighterOrder_[0].fighter->position_ + Magnum::Vector3::yAxis(3.f), {0, 0.f, 0.f}, {0.f, 1.f, 0.f}));	

		S_Fighter& fighterEngaged = fighterOrder_[countTurn % fighterOrder_.size()];

		if(fighterEngaged.isEnemy) 
		{
			/*Monster attack*/
			turnEnemy(fighterEngaged, deltaTime);
			arenaContext_ = E_ArenaContext::enemyTurn;
		}
		else
		{
			/*Heros attack*/
			turnPlayer(fighterEngaged, deltaTime);
			arenaContext_ = E_ArenaContext::playerTurn;
		}
		//Debug{} << " Time to Attack :  " << timeToattack;
	}
}

void Arena::keyPressEvent		(Magnum::Platform::Application::KeyEvent& event)
{
	if(arenaContext_ == E_ArenaContext::playerTurn)
	{
		switch (event.key())
		{
			case Application::KeyEvent::Key::Q :
				fighterFocusByPlayer_ = fighterOrder_[stepFighterFocusByPlayer_].fighter;

				playerInterface_.get(fighterFocusByPlayer_->name_).clear();

				if(stepFighterFocusByPlayer_ == 0)
				{
					stepFighterFocusByPlayer_ = fighterOrder_.size() - 1;
				}
				else
				{
					stepFighterFocusByPlayer_--;
				}
				break;

			case Application::KeyEvent::Key::E :

				fighterFocusByPlayer_ = fighterOrder_[stepFighterFocusByPlayer_].fighter;

					playerInterface_.get(fighterFocusByPlayer_->name_).clear();

				if(stepFighterFocusByPlayer_ == fighterOrder_.size() - 1)
				{
					stepFighterFocusByPlayer_ = 0;
				}
				else
				{
					stepFighterFocusByPlayer_++;
				}
				break;

			case Application::KeyEvent::Key::One :
				player_.action.useCapa1  = true;
				break;

			case Application::KeyEvent::Key::Two :
				player_.action.useCapa2 = true;
				break;

			case Application::KeyEvent::Key::Three :
				player_.action.useCapa3     = true;
				break;

			case Application::KeyEvent::Key::Four :
				player_.action.useCapa4    = true;
			break;

			case Application::KeyEvent::Key::W :
				player_.action.moveForward  = true;
				cameraObject_.translate(Vector3::zAxis(0.1f));
				break;

			case Application::KeyEvent::Key::S :
				player_.action.moveBackward = true;
				cameraObject_.translate(Vector3::zAxis(-0.1f));
				break;

			case Application::KeyEvent::Key::A :
				player_.action.turnLeft     = true;
				break;

			case Application::KeyEvent::Key::D :
				player_.action.turnRight    = true;
			break;

			default:
			break;
		}
	}
}

void Arena::keyReleaseEvent	(Magnum::Platform::Application::KeyEvent& event)
{
	switch (event.key())
	{
		case Application::KeyEvent::Key::One :
			player_.action.useCapa1  = false;
			break;
		case Application::KeyEvent::Key::Two :
			player_.action.useCapa2 = false;
			break;
		case Application::KeyEvent::Key::Three :
			player_.action.useCapa3     = false;
			break;
		case Application::KeyEvent::Key::Four :
			player_.action.useCapa4    = false;
		break;
		case Application::KeyEvent::Key::W :
	        player_.action.moveForward  = false;
		break;
		case Application::KeyEvent::Key::S :
	        player_.action.moveBackward = false;
		break;
		case Application::KeyEvent::Key::A :
	        player_.action.turnLeft     = false;
		break;
		case Application::KeyEvent::Key::D :
	        player_.action.turnRight    = false;
		break;
		default:
		break;
	}
}

void Arena::mousePressEvent	(Magnum::Platform::Application::MouseEvent& event)
{
	if(event.button() == Application::MouseEvent::Button::Left)
	{
			player_.action.useCapa1  = true;

	}

	if(event.button() == Application::MouseEvent::Button::Right)
	{
			player_.action.useCapa4  = true;
	}
}

void Arena::mouseReleaseEvent	(Magnum::Platform::Application::MouseEvent& event)
{
	if(event.button() == Application::MouseEvent::Button::Left)
	{
			player_.action.useCapa1  = false;
	}

	if(event.button() == Application::MouseEvent::Button::Right)
	{
			player_.action.useCapa4  = false;
	}
}

void Arena::entranceInto (Enemy& enemy)
{
	cameraObject_.setParent(&scene_);
	player_.setParent(scene_);
	enemy.setParent(scene_);

	float xmore = -5;

	enemy_ = &enemy;

	for(auto&& hero: player_.heros_)
	{
		hero->position_ = {-5,0,xmore};
		hero->rotate_Y(M_PI_2);
		hero->modelfighter_->resetTransformation();
    	hero->modelfighter_->scale({0.5,0.5,0.5}).rotateY(static_cast<Magnum::Rad>(hero->rotatey)).translate(hero->position_);
		xmore += 5;
	}

	xmore = -5;

	for(auto& mons: enemy_->monsters_)
	{
		mons->position_ = {5,0,xmore};
		mons->rotate_Y(-M_PI_2);
		mons->modelfighter_->resetTransformation();
    	mons->modelfighter_->scale({0.7,0.7,0.7}).rotateY(static_cast<Magnum::Rad>(mons->rotatey)).translate(mons->position_);
		xmore += 5;
	}

	/*sort the fighter and stor it in function of there initiative. The first will start to figth*/
	fighterOrder_.reserve(player_.heros_.size() + enemy_->monsters_.size());

	for(unsigned int i = 0; i < player_.heros_.size(); i++)
	{
		fighterOrder_.push_back(S_Fighter{player_.heros_[i].get(), false});
	}

	for(unsigned int i = 0; i < enemy_->monsters_.size(); i++)
	{
		fighterOrder_.push_back(S_Fighter{enemy_->monsters_[i].get(), true});
	}

    std::sort(	fighterOrder_.begin(),
				fighterOrder_.end(),
				[](S_Fighter& a, S_Fighter& b)
				{
        			return a.fighter->initiative_ > b.fighter->initiative_;
   	 			});
}

void Arena::draw()
{
	for(auto&& fighter : fighterOrder_)
	{
		camera_.draw(fighter.fighter->getDrawable());
	}

	camera_.draw(drawables_);
}

void Arena::turnPlayer(S_Fighter& heroEngaged, float deltaTime)
{
	if(fighterFocusByPlayer_ == nullptr)
	{
		fighterFocusByPlayer_ = foundEnemyMostNear(heroEngaged);
	}

	/*define camera*/
	if(fighterFocusByPlayer_ != heroEngaged.fighter)
	{
		useThirdPersonnCameraToSeePoint(heroEngaged.fighter->position_, fighterFocusByPlayer_->position_, 178.f, 10.5f, 2.5f);
	}
	else
	{
		cameraObject_.setTransformation(Magnum::Matrix4::lookAt(heroEngaged.fighter->position_ + Magnum::Vector3::yAxis(5.f) + Magnum::Vector3::xAxis(1.f),
																heroEngaged.fighter->position_,
																{0.f, 1.f, 0.f}));
	}

	/*use capacity*/
	if(!heroEngaged.fighter->isUsingCapacity())
	{
		/*Check if player use capacity*/
		if(player_.action.useCapa1)
		{
			heroEngaged.fighter->attack1(*fighterFocusByPlayer_);
		}
		else if (player_.action.useCapa2)
		{
			heroEngaged.fighter->attack2(*fighterFocusByPlayer_);
		}
		else if (player_.action.useCapa3)
		{
			heroEngaged.fighter->attack3(*fighterFocusByPlayer_);			
		}
		else if (player_.action.useCapa4)
		{
			heroEngaged.fighter->attack4(*fighterFocusByPlayer_);	
		}
		else
		{
			/*update orientation*/
			if(player_.action.moveForward)          heroEngaged.fighter->moveForward(deltaTime);
			else if(player_.action.moveBackward)    heroEngaged.fighter->moveBackward(deltaTime);

			if(player_.action.turnLeft)             heroEngaged.fighter->turnLeft(deltaTime);
			else if(player_.action.turnRight)       heroEngaged.fighter->turnRight(deltaTime);

			figthZone_.keepPointInsideZone(heroEngaged.fighter->position_);

			heroEngaged.fighter->modelfighter_-> resetTransformation()
				.rotateY     (Magnum::Rad(heroEngaged.fighter->getRotRad()))
				.scale       ({0.5f,0.5f,0.5f})
				.translate   (heroEngaged.fighter->position_);
		}
		
		/*Cehck if target is dead*/
		if(fighterFocusByPlayer_->isDead())
		{
			playerInterface_.get(fighterFocusByPlayer_->name_).clear();
			for (size_t i = 0; i < fighterOrder_.size(); i++)
			{
				if(fighterOrder_[i].fighter == fighterFocusByPlayer_)
				{
					fighterOrder_.erase(fighterOrder_.begin() + i);

					/*Get the next enemy in life*/
					for (auto &&i : fighterOrder_)
					{
						if (i.isEnemy)
						{
							fighterFocusByPlayer_ = i.fighter;
							break;
						}
					}
					
					break;
				}
			}
		}
	}

	if (!playerInterface_.has(heroEngaged.fighter->name_))
	{
		playerInterface_.EntityInfo(&rs_,heroEngaged.fighter->name_,
										 heroEngaged.fighter->life_,
										 heroEngaged.fighter->mana_,
										 {0.0f,0.8f});	
	}

	if (!playerInterface_.has(fighterFocusByPlayer_->name_))
	{
		playerInterface_.EntityInfo(&rs_,fighterFocusByPlayer_->name_,
										 fighterFocusByPlayer_->life_,
										 fighterFocusByPlayer_->mana_,
										 {0.7f,0.8f});
	}

	timeToattack -= deltaTime;

	if (timeToattack <= 0.f)
	{
		countTurn++;
	//	Debug{} << " Turn " <<  countTurn  << " finish, next turn." ;
		timeToattack = TURN_DURATION;
		playerInterface_.get(heroEngaged.fighter->name_).clear();
		return;
	}
}

void Arena::turnEnemy(S_Fighter& monsterEngaged, float deltaTime)
{
	Fighter* enemyAttacked = foundEnemyMostNear(monsterEngaged);
	useThirdPersonnCameraToSeePoint(monsterEngaged.fighter->position_, enemyAttacked->position_, 178.f, 2.5f, -0.5f);

	timeToattack -= deltaTime;

	if (!playerInterface_.has(monsterEngaged.fighter->name_))
	{
		playerInterface_.EntityInfo(&rs_,monsterEngaged.fighter->name_,
										 monsterEngaged.fighter->life_,
										 monsterEngaged.fighter->mana_,
										 {0.0f,0.8f});	
	}

	if (!playerInterface_.has(enemyAttacked->name_))
	{
		playerInterface_.EntityInfo(&rs_,enemyAttacked->name_,
										 enemyAttacked->life_,
										 enemyAttacked->mana_,
										 {0.7f,0.8f});
	}

	/*Monster atttack if it's possible*/
	if(!monsterEngaged.fighter->isUsingCapacity())
	{
		/*Monster will walk toward his enemy*/
		if((enemyAttacked->position_ - monsterEngaged.fighter->position_).length() > 2.f)
		{
			monsterEngaged.fighter->moveToward(enemyAttacked->position_, deltaTime);
			figthZone_.keepPointInsideZone(enemyAttacked->position_);
		}
		else
		{
			monsterEngaged.fighter->attack1(*enemyAttacked); /*Always use attack standard*/
			
			/*Check if target is dead*/
			if(enemyAttacked->isDead())
			{
				playerInterface_.get(enemyAttacked->name_).clear();
				for (size_t i = 0; i < fighterOrder_.size(); i++)
				{
					if(fighterOrder_[i].fighter == enemyAttacked)
					{
						fighterOrder_.erase(fighterOrder_.begin() + i);
						break;
					}
				}
			}
		}
	}

    if (timeToattack <= 0.f)
	{
		countTurn++;
		Debug{} << " Turn " <<  countTurn  << " finish, next turn." ;
		timeToattack = TURN_DURATION;
		playerInterface_.get(monsterEngaged.fighter->name_).clear();
		return;
	}
}

bool Arena::battleIsEnd(bool& playerWin)
{
	bool allEnemyIsDead = true;
	bool allPlayerIsDead = true;

	for (auto &&i : fighterOrder_)
	{
		if(i.isEnemy)
		{
			allEnemyIsDead = false;
		}
		else
		{
			allPlayerIsDead = false;
		}
	}

	if(allEnemyIsDead || allPlayerIsDead)
	{
		playerWin = allEnemyIsDead;

		/*reset arena*/
		arenaContext_ = E_ArenaContext::arriveCinematic;
		rotationCameraCinematicArrive_  = 0.f;
		posCameraCinematicArrive_		= {18.f, 21.f, 21.f};
		lerpStepForZoomAnimation_		= 0.f;
		fighterOrder_					.clear();
		fighterFocusByPlayer_			= nullptr;
		stepFighterFocusByPlayer_		= 0;
		countTurn						= 0;
		timeToattack 					= TURN_DURATION;

		playerInterface_.clear();

		return true;
	}

	playerWin = false;
	return false;        
}