#include "../include/World/House.hpp"
#include "../include/modelLoader.hpp"

#include <Magnum/Math/Angle.h>
#include "../include/tools.hpp"

using namespace Magnum;
using namespace Math::Literals;
using namespace Magnum::Platform;

House::House (GameResourceManager& ressources, Player& player, Object3D& cameraObject, Magnum::SceneGraph::Camera3D& camera, Pi::PlayerInterface& playerInterface)
    :   World           (player, cameraObject, camera, playerInterface, ressources),
        pMap_           {nullptr},
		exitZone_		{{-2.9f, -2.f, 2.3f}, {-1, 4, 2}},
		houseLimit_		{{3.f, -2.f, -5.2f}, {-6, 4, 10}}
{
    settingPlayer();
    initializeMap(ressources);
	initializePNJ(ressources);
}

void House::settingPlayer()
{
	for (auto &&model : player_.models)
	{
		model-> resetTransformation()
						.rotateY     (270.0_degf)
						.scale       (player_.scale_)
						.translate   ({-2.6f, 0.f, 2.7f});
	}

    player_.position = {-2.6f, 0.f, 3.6f};
	player_.resetOrientation();
	player_.rotateY(M_PI + M_PI_2);
}

void House::initializeMap(GameResourceManager& ressources)
{
    /*Initialize map model*/
    pMap_ = std::move(ModelLoader::loadModel("Game/resources/meshes/house.obj", ressources, drawables_, &scene_));
    pMap_   ->scale({0.05f, 0.05f, 0.05f})
            .translate({0.f, 0.f, 0.f});

    /*Initialize map light*/

    /*Set up map camera*/
}

void House::initializePNJ(GameResourceManager& ressources)
{
	/*add house owner*/
	pnjs_.emplace_back(	"Wakka",
						Vector3{1.5f, 0.f, 2.5f},
						Vector3{0.f, 260.f, 0.f},
						Vector3{0.5f,0.5f,0.5f},
						"Game/resources/meshes/wakkaball.obj", ressources, &scene_);

	pnjs_.back().addModel("Game/resources/meshes/wakkahey.obj", ressources, &scene_);

	pnjs_.back().addDialog("Hi traveler !");
	pnjs_.back().addDialog("I have some problem with my beer recipe... I lost it in the street next to my house, can you help me ?");
}

void House::entranceInto       () 
{
	World::entranceInto();
	settingPlayer();
	cam_.yaw = static_cast<float>(M_PI_2);
}

void House::execut             (float deltatime)
{
    player_.update(deltatime);

	houseLimit_.keepPointInsideZone(player_.position);

	cam_.camerafps();
	cam_.cameraPos = player_.position;
	cam_.cameraPos.y() += 2.1f;
	cameraObject_.resetTransformation();
	cameraObject_.rotateX(static_cast<Rad>(cam_.pitch)).rotateY(static_cast<Rad>(-cam_.yaw));
	cameraObject_.translate(cam_.cameraPos);

	if (pnjs_.back().playerHitPnj(player_.position))
	{
		if (!playerInterface_.has(pnjs_.back().getName()))
		{
			playerInterface_.PnjTextBox(&rs_,pnjs_.back().getName(),pnjs_.back().getDialogues());
		}
	}			
	else
	{
		playerInterface_.get(pnjs_.back().getName()).close();
		if (playerInterface_.closed(pnjs_.back().getName()))
			playerInterface_.stop(pnjs_.back().getName());
	}

	for (auto &&model : player_.models)
	{
		model-> resetTransformation()
			.rotateY     (Magnum::Rad(player_.getRotRad()))
			.scale       (player_.scale_)
			.translate   (player_.position);
	}


	for (auto &&pnj : pnjs_)
	{
		if (pnj.playerHitPnj(player_.position))
		{
			pnj.changSetAnimation(1);
			//std::cout << "You meet \"" << pnj.getName() << "\""<< std::endl;
			pnj.orientTowardPoint(player_.position);

			/*third person camera*/
			Magnum::Vector3 vectorFromPlayerToPnj(pnj.getPosition() - player_.position);
			vectorFromPlayerToPnj.y() = 0.f;
			float exDirX = vectorFromPlayerToPnj.x();
			static float angle = 170.f;
			float camAngleRad = angle * static_cast<float>(M_PI) / 180.f;
			float cosR = cosf(camAngleRad);
			float sinR = sinf(camAngleRad);

			vectorFromPlayerToPnj.x() = exDirX * cosR - vectorFromPlayerToPnj.z() * sinR;
			vectorFromPlayerToPnj.z() = exDirX * sinR + vectorFromPlayerToPnj.z() * cosR;

			Magnum::Vector3 vectorFromPlayerToCam ((vectorFromPlayerToPnj.normalized() * 3.f) + pnj.getPosition());
			vectorFromPlayerToCam.y() += 3.f;
			cameraObject_.setTransformation(Magnum::Matrix4::lookAt(vectorFromPlayerToCam, pnj.getPosition() + Magnum::Vector3::yAxis(1.f), {0.f, 1.f, 0.f}));

			break; /*player can only interract with on pnj at time*/
		}
		else
		{
			pnj.changSetAnimation(0);
		}
		
	}
}

void House::draw()
{
	bool pnjLookAt = false;
	for (auto &&pnj : pnjs_)
	{
		if (pnj.playerHitPnj(player_.position))
		{
			pnjLookAt = true;
			break;
		}
	}

	if(pnjLookAt)
	{
		World::draw();
	}
	else
	{
		camera_.draw(drawables_);
	}
	
	for (auto &&pnj : pnjs_)
	{
		pnj.draw(camera_);
	}
}

void House::keyPressEvent		(Magnum::Platform::Application::KeyEvent& event)
{
	switch (event.key())
	{
		case Application::KeyEvent::Key::W :
	        player_.action.moveForward  = true;
            cameraObject_.translate(Vector3::zAxis(0.1f));
			break;
		case Application::KeyEvent::Key::S :
	        player_.action.moveBackward = true;
            cameraObject_.translate(Vector3::zAxis(-0.1f));
			break;
		case Application::KeyEvent::Key::A :
	        player_.action.moveLeft     = true;
            cameraObject_.translate(Vector3::xAxis(0.1f));
			break;
		case Application::KeyEvent::Key::D :
	        player_.action.moveRight    = true;
            cameraObject_.translate(Vector3::xAxis(-0.1f));
		break;
		default:
		break;
	}
}

void House::keyReleaseEvent	(Magnum::Platform::Application::KeyEvent& event)
{
	switch (event.key())
	{
		case Application::KeyEvent::Key::W :
	        player_.action.moveForward  = false;
		break;
		case Application::KeyEvent::Key::S :
	        player_.action.moveBackward = false;
		break;
		case Application::KeyEvent::Key::A :
	        player_.action.moveLeft     = false;
		break;
		case Application::KeyEvent::Key::D :
	        player_.action.moveRight    = false;
		break;
		default:
		break;
	}
}

void House::mouseMoveEvent(Magnum::Platform::Application::MouseMoveEvent& event) 
{
	player_.rotateY(event.relativePosition().x() * 0.001f);
    cam_.mouse_camera(event);
}
