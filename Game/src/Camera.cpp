#include "../include/Camera.hpp"

using namespace Magnum;

Camera::Camera(/* args */)
{}

Camera::~Camera()
{
}
// ========================= Camera Input ========================= //

void Camera::mouse_camera(Magnum::Platform::Sdl2Application::MouseMoveEvent& event)
{
    float xdelta_pos = event.relativePosition().x();
    float ydelta_pos = event.relativePosition().y();

    float sensitivity_mouse = 0.1f;

    xdelta_pos *= sensitivity_mouse * 0.01f;
    ydelta_pos *= sensitivity_mouse * 0.01f;

    yaw += xdelta_pos;
    pitch += -ydelta_pos;

    if (pitch >= static_cast<float>(M_PI_2))
        pitch = static_cast<float>(M_PI_2) ;

    else if (pitch <= static_cast<float>(-M_PI_2))
        pitch = static_cast<float>(-M_PI_2);
}

void Camera::camera_pressinput(Platform::Sdl2Application::KeyEvent& event)
{
    float Speed = 4.f;
    float FrameSpeed = Speed * 0.002f;

    switch (event.key())
    {
        case Platform::Sdl2Application::KeyEvent::Key::Q :
                FrameSpeed *= 3.f;
            break;
        case Platform::Sdl2Application::KeyEvent::Key::W :
            freefly_[0] = true;
            break;
        case Platform::Sdl2Application::KeyEvent::Key::S :
            freefly_[1] = true;
            break;

        case Platform::Sdl2Application::KeyEvent::Key::A :
            freefly_[2] = true;
            break;
        case Platform::Sdl2Application::KeyEvent::Key::D :
            freefly_[3] = true;
            break;
        case Platform::Sdl2Application::KeyEvent::Key::Space :
            freefly_[4] = true;
            break;
        case Platform::Sdl2Application::KeyEvent::Key::LeftShift :
            freefly_[5] = true;
            break;
        case Platform::Sdl2Application::KeyEvent::Key::F8 :
            Debug{} << "pos x :" << cameraPos.x() << " | pos y :" << cameraPos.y() 
            << " | pos z :" << cameraPos.z();
            break;
        default:
				break;
    }
}

void Camera::camera_realeaseinput(Platform::Sdl2Application::KeyEvent& event)
{
    switch (event.key())
    {
        case Platform::Sdl2Application::KeyEvent::Key::W :
            freefly_[0] = false;
            break;
        case Platform::Sdl2Application::KeyEvent::Key::S :
            freefly_[1] = false;
            break;

        case Platform::Sdl2Application::KeyEvent::Key::A :
            freefly_[2] = false;
            break;
        case Platform::Sdl2Application::KeyEvent::Key::D :
            freefly_[3] = false;
            break;
        case Platform::Sdl2Application::KeyEvent::Key::Space :
            freefly_[4] = false;
            break;
        case Platform::Sdl2Application::KeyEvent::Key::LeftShift :
            freefly_[5] = false;
            break;
        default:
            break;
    }
}

// ========================= Camera Free Fly ========================= //

void Camera::camerafreefly()
{
    ForwardVelocity = 0;
    StrafeVelocity = 0;

    if(freefly_[0] == true && freefly_[1] == false) // w : forward
    {
        ForwardVelocity = -0.3f; 
    }
    if(freefly_[1] == true &&  freefly_[0] == false) // s : back
    {
        ForwardVelocity = 0.3f; 
    }
    if(freefly_[2] == true && freefly_[3] == false) // a : left side 
    {
        StrafeVelocity = -0.3f; 
    }
    if(freefly_[3] == true && freefly_[2] == false)  // d : right side 
    {
        StrafeVelocity = 0.3f; 
    }
    if(freefly_[4] == true) // Space
    {
        cameraPos.y() += 0.3f;
    }
    if(freefly_[5] == true) // left shift
    {
        cameraPos.y() -= 0.3f;
    }

    position_camerafreefly();
    
}

void Camera::position_camerafreefly()
{
    cameraPos.z() += cos(yaw) * cos(pitch) * ForwardVelocity;
    cameraPos.x() += sin(-yaw) * cos(-pitch) * ForwardVelocity;
    cameraPos.y() -= sin(pitch) * ForwardVelocity;

    cameraPos.z() += sin(yaw)  * StrafeVelocity;
    cameraPos.x() += cos(yaw)  * StrafeVelocity;
}

// ========================= Camera FPS ========================= //

void Camera::camerafps()
{
    ForwardVelocity = 0;
    StrafeVelocity = 0;

    if(freefly_[0] == true && freefly_[1] == false) // w : forward
    {
        ForwardVelocity = -0.3f; 
    }
    if(freefly_[1] == true &&  freefly_[0] == false) // s : back
    {
        ForwardVelocity = 0.3f; 
    }
    if(freefly_[2] == true && freefly_[3] == false) // a : left side 
    {
        StrafeVelocity = -0.3f; 
    }
    if(freefly_[3] == true && freefly_[2] == false)  // d : right side 
    {
        StrafeVelocity = 0.3f; 
    }
     if(freefly_[4] == true) // Space
    {
        cameraPos.y() += 0.3f;
    }
    if(freefly_[5] == true) // left shift
    {
        cameraPos.y() -= 0.3f;
    }
    
    position_camerafps();
}

void Camera::position_camerafps()
{
    cameraPos.z() += cos(yaw) * cos(pitch) * ForwardVelocity;
    cameraPos.x() += sin(-yaw) * cos(-pitch) * ForwardVelocity;

    cameraPos.z() += sin(yaw)  * StrafeVelocity;
    cameraPos.x() += cos(yaw)  * StrafeVelocity;
}