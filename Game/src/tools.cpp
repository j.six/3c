#include "../include/tools.hpp"
#include <type_traits>
#include <iostream>

int ranValueLimite(int min, int max)
{
	if (min == 0 && max == 0)
		return 0;
	max++;
	max -= min;
	return rand() % max + min; 
}

bool ranPercentProba(int percent)//return if purcent value is respect in randome case
{ 
	return ranValueLimite(0, 100) <= percent;
}

template<typename T, T Min, T Max>
auto isBetween(T value) -> std::enable_if_t<Min <= Max, bool>
{
	return value >= Min && value <= Max;
}

std::string to_strPrecision (const float & numb)
{
	std::stringstream stream;
	stream << std::fixed << std::setprecision(2) << numb;
	return stream.str();
}
