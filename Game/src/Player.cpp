#include "../include/Player.hpp"
#include <Corrade/Utility/Debug.h>

using namespace Magnum;

void Player::update (float deltaTime)
{
    if(action.moveForward)          moveForward(deltaTime);
    else if(action.moveBackward)    moveBackward(deltaTime);
    else
    {
        walkAnim_ = 0.f;
        stepAnim_ = 0;
    }

    if(action.moveLeft)             moveLeft(deltaTime);
    else if(action.moveRight)       moveRight(deltaTime);

    if(action.turnLeft)             turnLeft(deltaTime);
    else if(action.turnRight)       turnRight(deltaTime);
}

void Player::rotateY (float rad)
{
    float exDirX = direction_.x();
    float cosR = cosf(rad);
    float sinR = sinf(rad);

    direction_.x() = exDirX * cosR - direction_.z() * sinR;
    direction_.z() = exDirX * sinR + direction_.z() * cosR;

    rot_ -= rad;

    if (rot_ > static_cast<float>(M_PI) * 2.f)
    {
        rot_ -= static_cast<float>(M_PI) * 2.f;
    }
    else if (rot_ < 0.f)
    {
        rot_ += static_cast<float>(M_PI) * 2.f;
    }
}

void Player::moveForward    (float deltaTime)
{
    walkAnim_ += deltaTime;

    if(walkAnim_ >= walkTime_)
    {
        walkAnim_ = 0.f;
        stepAnim_++;
        if(stepAnim_ == drawables.size())
        {
            stepAnim_ = 0;
        }
    }

    position += direction_ * moveSpeed_ * deltaTime;
    for (auto &&model : models)
    {
        model->translate(direction_ * moveSpeed_ * deltaTime);
    }
}

void Player::moveBackward   (float deltaTime)
{
    walkAnim_ += deltaTime;

    if(walkAnim_ >= walkTime_)
    {
        walkAnim_ = 0.f;
        stepAnim_++;
        if(stepAnim_ == drawables.size())
        {
            stepAnim_ = 0;
        }
    }

    position -= direction_ * moveSpeed_ * deltaTime;
    for (auto &&model : models)
    {
        model->translate(-direction_ * moveSpeed_* deltaTime);
    }
}

void Player::moveLeft       (float deltaTime)
{
    Vector3 dirOrtho(-direction_.z(), direction_.y(), direction_.x());
    position -= dirOrtho * moveSpeed_ * deltaTime;
    for (auto &&model : models)
    {
        model->translate(-dirOrtho * moveSpeed_ * deltaTime);
    }
}

void Player::moveRight      (float deltaTime)
{
    Vector3 dirOrtho(-direction_.z(), direction_.y(),direction_.x());
    position += dirOrtho * moveSpeed_ * deltaTime;
    for (auto &&model : models)
    {
        model->translate(dirOrtho * moveSpeed_ * deltaTime);
    }
}

void Player::turnLeft       (float deltaTime)
{
    rotateY(-turnSpeed_ * deltaTime);
}

void Player::turnRight      (float deltaTime)
{
    rotateY(turnSpeed_ * deltaTime);
}

void Player::addHero(E_FighterType type, float life, float attspeed, float damage, float mana,const std::string& pathname, const std::string& name, GameResourceManager& ressources, Object3D* parent)
{
    switch (type)
    {
    case E_FighterType::Mage :
        heros_.emplace_back(std::make_unique<FighterMage>(life,attspeed,damage,mana,pathname, name, ressources, parent));
        break;

    case E_FighterType::Warrior :
        heros_.emplace_back(std::make_unique<FighterWarrior>(life,attspeed,damage,mana,pathname, name, ressources, parent));
        break;

    case E_FighterType::Assassin :
        heros_.emplace_back(std::make_unique<FighterAssassin>(life,attspeed,damage,mana,pathname, name, ressources, parent));
        break;

    default:
        break;
    }
}

void Player::resetAction ()
{
    action.moveForward        = false;
    action.moveBackward       = false;
    action.moveLeft           = false;
    action.moveRight          = false;
    action.turnLeft           = false;
    action.turnRight          = false;
    action.useCapa1           = false;
    action.useCapa2           = false;
    action.useCapa3           = false;
    action.useCapa4           = false;
}
