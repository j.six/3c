# 3C (character, control, camera)
> Ritouni Theo
> Bleuse Quentin
> Six Jonathan.
> February 2020

## Summary
- [Description](##Description "Goto description part")
- [Command](##Command "Goto command part")
- [How to compil](##How-to-compil "Goto compil part")
- [How to launch](##How-to-launch "Goto launch part")
- [Bug](##Bug "Goto bug part")
- [Technical aspect](##Technical-aspect "Goto technical part")

## Description

### Step of our work
> 02/04 : Test to undestand who magnum load obj and multiple texturing. Load tower for example
![Screen shoot 1](Game/resources/screenShoot/Screenshot2020-02-04-18-26-33.png "Screen shoot example 1")

> 03/04 : Create plan with different camera zone. Test to understand complexity of 3C
![Screen shoot 3](Game/resources/screenShoot/Screenshot2020-02-10-12-23-41.png "Screen shoot example 3")

>10/02 : Load complicat obj
![Screen shoot 2](Game/resources/screenShoot/Screenshot2020-02-10-12-20-13.png "Screen shoot example 2")


> 11/02 : Add 3C to the project and ccreate zone with multiple camera insid city. n obj with complex angle
![Screen shoot 4](Game/resources/screenShoot/Screenshot2020-02-11-19-18-47.png "Screen shoot example 4")

> 14/02 : Create arena with fight system. Interger UI system 
![Screen shoot 5](Game/resources/screenShoot/Screenshot2020-02-17-13-39-57.png "Screen shoot example 5")

> 17/02 : Add diferent quest with different type of camera.
![Screen shoot 5](Game/resources/screenShoot/Screenshot2020-02-17-13-47-15.png "Screen shoot example 5")


## Function implemented


### **Files**


## Command

### Keyboard :
In exploration
Command 		| Action
------------- 	| -------------
Esc         	| Exit
W               | Move toward
S               | Move backward
A               | turn left
D               | turn right

In fight 
Command 		| Action
------------- 	| -------------
Esc         	| Exit
W               | Move toward
S               | Move backward
A               | turn left
D               | turn right
Q               | target other fighter left
E               | target other fighter rigth
1 and left clic | use capacity 1 (standart attack)
2               | use capacity 2
3               | use capacity 3
4 and right clic| use capacity 4 (massiv attack)


## How to compil
Open terminal in source of project and execut this command

```
make
```

## How to launch

```
make run
or
./bin/exe
```

## Bug

## Technical aspect

This project is depending to :
- SDL 2.0
- Magnum engin