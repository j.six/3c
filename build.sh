#!/bin/bash

INSTALL_PREFIX=$PWD/magnum-library
IMGUI_DIR=$PWD/build/imgui

export PATH=$INSTALL_PREFIX/bin:$PATH

source project_config.sh
mkdir $PROJ_NAME-build -p
pushd $PROJ_NAME-build
cmake ../$PROJ_NAME -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
    -DCMAKE_IGNORE_PATH=/usr/lib \
    -DCORRADE_INCLUDE_DIR=$INSTALL_PREFIX/include \
    -DCORRADE_RC_EXECUTABLE=$INSTALL_PREFIX/bin/corrade-rc \
    -DIMGUI_DIR=$IMGUI_DIR
make -j 
popd
