#!/bin/bash

set -x
INSTALL_PREFIX=$PWD/magnum-library
IMGUI_DIR=$PWD/build/imgui

export PATH=$INSTALL_PREFIX/bin:$PATH

mkdir -p build
pushd build

[ ! -d 'corrade' ] && git clone https://github.com/mosra/corrade.git
mkdir -p build-corrade
pushd build-corrade
cmake ../corrade -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX
make -j
make install
popd

[ ! -d 'magnum' ] && git clone https://github.com/mosra/magnum.git
mkdir -p build-magnum
pushd build-magnum
cmake ../magnum -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
    -DWITH_SDL2APPLICATION=ON \
    -DWITH_ANYIMAGEIMPORTER=ON \
    -DWITH_ANYAUDIOIMPORTER=ON \
    -DWITH_ANYIMAGECONVERTER=ON \
    -DWITH_ANYSCENEIMPORTER=ON \
    -DWITH_WAVAUDIOIMPORTER=ON \
    -DWITH_MAGNUMFONT=ON \
    -DWITH_MAGNUMFONTCONVERTER=ON \
    -DWITH_OBJIMPORTER=ON \
    -DWITH_AUDIO=ON
make -j
make install
popd

[ ! -d 'imgui' ] && git clone https://github.com/ocornut/imgui.git

[ ! -d 'magnum-integration' ] && git clone https://github.com/mosra/magnum-integration.git
mkdir -p build-magnum-integration
pushd build-magnum-integration
cmake ../magnum-integration -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
    -DWITH_IMGUI=ON \
    -DIMGUI_DIR=$IMGUI_DIR
make -j
make install
popd

[ ! -d 'magnum-plugins' ] && git clone https://github.com/mosra/magnum-plugins.git
mkdir -p build-magnum-plugins
pushd build-magnum-plugins
cmake ../magnum-plugins -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
    -DWITH_ASSIMPIMPORTER=ON \
    -DWITH_BASISIMAGECONVERTER=OFF \
    -DWITH_BASISIMPORTER=OFF \
    -DWITH_DDSIMPORTER=ON \
    -DWITH_DEVILIMAGEIMPORTER=OFF \
    -DWITH_DRFLACAUDIOIMPORTER=OFF \
    -DWITH_DRMP3AUDIOIMPORTER=OFF \
    -DWITH_DRWAVAUDIOIMPORTER=OFF \
    -DWITH_FAAD2AUDIOIMPORTER=OFF \
    -DWITH_JPEGIMAGECONVERTER=OFF \
    -DWITH_JPEGIMPORTER=OFF \
    -DWITH_MINIEXRIMAGECONVERTER=OFF \
    -DWITH_OPENGEXIMPORTER=OFF \
    -DWITH_PNGIMAGECONVERTER=OFF \
    -DWITH_PNGIMPORTER=OFF \
    -DWITH_STANFORDIMPORTER=OFF \
    -DWITH_STBIMAGECONVERTER=OFF \
    -DWITH_STBIMAGEIMPORTER=ON \
    -DWITH_STBTRUETYPEFONT=ON \
    -DWITH_STBVORBISAUDIOIMPORTER=OFF \
    -DWITH_TINYGLTFIMPORTER=OFF
make -j
make install
popd

[ ! -d 'magnum-extras' ] && git clone https://github.com/mosra/magnum-extras.git
mkdir -p build-magnum-extras
pushd build-magnum-extras
cmake ../magnum-extras -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
    -DWITH_UI=ON
make -j
make install
popd

[ ! -d 'magnum-examples' ] && git clone https://github.com/mosra/magnum-examples.git
mkdir -p build-magnum-examples
pushd build-magnum-examples
cmake ../magnum-examples -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
    -DIMGUI_DIR=$IMGUI_DIR \
    -DWITH_AREALIGHTS_EXAMPLE=ON \
    -DWITH_AUDIO_EXAMPLE=ON \
    -DWITH_BOX2D_EXAMPLE=OFF \
    -DWITH_BULLET_EXAMPLE=OFF \
    -DWITH_CUBEMAP_EXAMPLE=ON \
    -DWITH_DART_EXAMPLE=OFF \
    -DWITH_FLUIDSIMULATION2D_EXAMPLE=ON \
    -DWITH_FLUIDSIMULATION3D_EXAMPLE=ON \
    -DWITH_IMGUI_EXAMPLE=ON \
    -DWITH_LEAPMOTION_EXAMPLE=OFF \
    -DWITH_MOUSEINTERACTION_EXAMPLE=ON \
    -DWITH_MOTIONBLUR_EXAMPLE=ON \
    -DWITH_OVR_EXAMPLE=OFF \
    -DWITH_PICKING_EXAMPLE=ON \
    -DWITH_PRIMITIVES_EXAMPLE=ON \
    -DWITH_SHADOWS_EXAMPLE=ON \
    -DWITH_TEXT_EXAMPLE=ON \
    -DWITH_TEXTUREDTRIANGLE_EXAMPLE=ON \
    -DWITH_TRIANGLE_EXAMPLE=ON \
    -DWITH_VIEWER_EXAMPLE=ON
make -j
make install
popd

popd


